import torch


class Action:
    """
    Generic action interface. It's unlikely you'll use this directly, see
    extending classes below.
    """

    def __init__(self, config=None):
        """
        Args:
            joint_pos (ndarray): Joint position to set initially
        """
        self.config = config
        if config is not None:
            self.set_joint_position(config.default_joint_pos)
        else:
            self.set_joint_position(None)

    def set_joint_position(self, joint_pos):
        """
        Set joint position action.

        Args:
            joint_pos (ndarray): Joint position action to set
        """
        if joint_pos is not None and not torch.is_tensor(joint_pos):
            joint_pos = torch.tensor(joint_pos, dtype=torch.float)
        self.joint_pos = joint_pos

    def get_joint_position(self):
        """
        Returns current set joint position
        """
        if self.joint_pos is None:
            return None
        return self.joint_pos.clone()

    def update_action_joint_pos(self, env_state):
        pass


class ArmAction(Action):
    """
    Arm action interface.

    TODO currently nothing arm-specific that needs done, but can add e.g.
    checking joint limits.
    """

    ...


class EndEffectorAction(Action):
    """
    End-effector action interface. This handles different action modes,
    currently supported:
        - Joint position: directly set raw joint position of fingers
        - Discrete: set discrete actions. Currently supported:
            - open: Open fingers to joint angle specified in EE config
            - close: Close fingers to joint angle specified in EE config
        - Same-angle: Command all fingers to same joint position

    When action modes besides joint position is used, the function
    self.update_action_joint_pos is used to compute the joint position
    in terms of the other action mode being used and the EE config.

    TODO: can support other low-level command interfaces to Isaac Gym such
          as velocity or torque, but we're currently only utilizing position.
    TODO: can add other discrete actions and other action modes entirely
          as needed.
    """

    def __init__(self, config=None, discrete=None, same_angle=None):
        """
        Args:
            joint_pos (ndarray): Joint position to initialize command
            discrete (str): String name of discrete action to initialize with
            same_angle (float): Joint angle to initialize same-angle command to
        """
        super().__init__(config)
        self.set_discrete(discrete)
        self.set_same_angle(same_angle)
        self.interpolate_list = []
        self.interpolate_gap = config.interpolate_gap

    def set_discrete(self, discrete):
        self.discrete = discrete

    def set_same_angle(self, angle):
        self.same_angle = angle

    def get_discrete(self):
        return self.discrete

    def get_same_angle(self):
        return self.same_angle

    def has_discrete(self):
        return self.discrete is not None

    def has_same_angle(self):
        return self.same_angle is not None

    def clear_discrete(self):
        self.set_discrete(None)
        self.interpolate_list = []

    def update_action_joint_pos(self, env_state):
        """
        Computes low-level joint position command to send to Isaac Gym in terms of
        other action modes that are set. Currently supports discrete actions
        (e.g. open/close fingers) and same-angle (i.e. set all fingers to the same
        angle).

        The action modes are mutually exclusive, so you can't for example use both
        same-angle and discrete modes. If the other action modes are not being used,
        this will be a pass-through function that does not modify the joint position
        command already set.

        Note the joint position updates are performed in-place.
        """
        if self.has_discrete() and self.has_same_angle():
            raise RuntimeError("Cannot set both discrete and same-angle actions")

        idxs = self.config.grip_finger_indices
        open_pos = self.config.open_finger_joint_pos
        close_pos = self.config.close_finger_joint_pos

        joint_pos = None
        if self.has_discrete():
            if self.discrete == "close":
                diff = torch.tensor(close_pos) - env_state.joint_position[idxs, 0]
                change = torch.zeros_like(diff)
                change[diff.abs() >= self.interpolate_gap] += self.interpolate_gap
                change[diff < 0] *= -1
                change[diff.abs() < self.interpolate_gap] = diff[diff.abs() < self.interpolate_gap]
                joint_pos = env_state.joint_position[idxs, 0] + change
            elif self.discrete == "open":
                diff = torch.tensor(open_pos) - env_state.joint_position[idxs, 0]
                change = torch.zeros_like(diff)
                change[diff.abs() >= self.interpolate_gap] += self.interpolate_gap
                change[diff < 0] *= -1
                change[diff.abs() < self.interpolate_gap] = diff[diff.abs() < self.interpolate_gap]
                joint_pos = env_state.joint_position[idxs, 0] + change
            else:
                raise ValueError(f"Unknown discrete action for EE: {self.discrete}")
        elif self.has_same_angle():
            joint_pos = [self.same_angle] * len(idxs)

        if joint_pos is not None:
            try:
                self.joint_pos[idxs] = joint_pos.to(self.joint_pos.device)
            except Exception as e:
                print("error:", e)
                import pdb

                pdb.set_trace()

            # if not torch.is_tensor(self.joint_pos):
            #     self.joint_pos[idxs] = torch.tensor(joint_pos).to(self.joint_pos.device)


class RobotAction:
    """
    Robot action interface. This provides a single interface to engage
    with both the arm and EE action interfaces.
    """

    def __init__(self, armConfig=None, eeConfig=None):
        self.arm = ArmAction(armConfig)
        self.end_effector = EndEffectorAction(eeConfig)

    def set_joint_position(self, arm_joint_pos=None, ee_joint_pos=None):
        self.set_arm_joint_position(arm_joint_pos)
        self.set_ee_joint_position(ee_joint_pos)

    def set_arm_joint_position(self, joint_pos):
        self.arm.set_joint_position(joint_pos)

    def set_ee_joint_position(self, joint_pos):
        self.end_effector.set_joint_position(joint_pos)

    def set_ee_discrete(self, discrete):
        self.end_effector.set_discrete(discrete)

    def set_ee_same_angle(self, angle):
        self.end_effector.set_same_angle(angle)

    def get_joint_position(self):
        """
        Returns the combined arm and joint positions, useful for computing the
        final joint position commands that will be sent to Isaac Gym.

        If there is no end-effector action then only the arm joints will be returned.
        """
        arm_joints = self.get_arm_joint_position()
        ee_joints = self.get_ee_joint_position()

        joint_pos = arm_joints
        if ee_joints is not None:
            joint_pos = torch.cat([arm_joints, ee_joints])
        return joint_pos

    def get_arm_joint_position(self):
        return self.arm.get_joint_position()

    def get_ee_joint_position(self):
        return self.end_effector.get_joint_position()

    def get_ee_discrete(self):
        return self.end_effector.get_discrete()

    def get_ee_same_angle(self):
        return self.end_effector.get_same_angle()

    def has_ee_discrete(self):
        return self.end_effector.has_discrete()

    def has_ee_same_angle(self):
        return self.end_effector.has_same_angle()

    def get_state_tensor(self):
        """
        Returns tensor of arm joint angles combined with discrete EE action.
        """
        joint = self.get_arm_joint_position()
        tensor = torch.zeros((joint.shape[0] + 1))
        tensor[:-1] += joint
        if self.has_ee_discrete() and self.end_effector.discrete == "close":
            tensor[-1] = 1
        return tensor

    def update_action_joint_pos(self, env_state):
        """
        Computes full joint position action (arm + EE) given actions for both
        the arm and the EE. This allows for different action parameterizations
        for each of the arm and the EE, e.g. discrete open/close for the EE and
        continuous values for the arm.
        """
        # Currently there isn't any processing on the arm actions
        # (pass-through func) but allowing the option anyways
        self.arm.update_action_joint_pos(env_state)
        self.end_effector.update_action_joint_pos(env_state)

    def clear_ee_discrete(self):
        self.end_effector.clear_discrete()

    def clear_ee_same_angle(self):
        self.set_ee_same_angle(None)
