import sys
import rospy
from tqdm import tqdm

from ll4ma_isaacgym.behaviors import Behavior
from ll4ma_isaacgym.core import Simulator
from ll4ma_util import ui_util, file_util


class Session:
    """
    Manages simulator execution, including managing behaviors, data collection,
    stepping the simulator, etc.
    """

    def __init__(self, session_config):
        self.config = session_config
        self.simulator = Simulator(self.config)
        self._step_idx = 0
        self._extra_step_idxs = [0] * self.config.n_envs
        self._is_last_step = [False] * self.config.n_envs
        self._behavior_params = {}
        self._reset_behaviors()

    def run(self):
        """
        Main entry point for running session.
        """
        if self.config.run_forever:
            while not rospy.is_shutdown():
                self.step()
        elif self.config.test_reset > 0:
            rate = rospy.Rate(100)
            while not rospy.is_shutdown():
                self.reset()
                for _ in range(int(100 * self.config.test_reset)):
                    if rospy.is_shutdown():
                        break
                    self.step(False)
                    rate.sleep()
        elif not self.config.data_root:
            for _ in range(self.config.n_demos):
                if rospy.is_shutdown():
                    break
                self._run()
                self.reset()
        else:
            pbar = tqdm(total=self.config.n_demos - self.get_n_demos(), file=sys.stdout)
            while not rospy.is_shutdown() and self.get_n_demos() < self.config.n_demos:
                self._run()
                if not rospy.is_shutdown():
                    # Log behavior params to save in datasets
                    self.simulator.log_env_attr("behavior_params", self._behavior_params)

                    if self.config.log_only_success:
                        save_env = [b.is_complete_success() for b in self.behaviors]
                        self.simulator.save_data(pbar, save_env)
                    else:
                        self.simulator.save_data(pbar)
                    self.reset()
            if not rospy.is_shutdown():
                ui_util.print_happy("\n\nData collection complete\n")
                print(f"  {self.config.data_root} has {self.get_n_demos()} demos\n")

    def _run(self):
        while not rospy.is_shutdown() and not self.is_complete():
            self.step()

    def step(self, pre_physics=True, post_physics=True, increment_step_idx=True):
        """
        Perform one step in the task including applying actions, stepping physics,
        and computing observations.
        """
        # Apply actions
        if pre_physics:
            self._pre_physics_step()

        # Account for extra steps that are being recorded after task completion
        if self.config.task.extra_steps > 0:
            for env_idx, behavior in enumerate(self.behaviors):
                if behavior.is_complete():
                    self._extra_step_idxs[env_idx] += 1

        if not self.simulator.collect_data:
            self.simulator.step(post_physics)
        else:
            # We don't need to further log any data for envs that are finished
            for env_idx, behavior in enumerate(self.behaviors):
                if behavior.is_complete():
                    if self.config.task.extra_steps > 0:
                        current = self._extra_step_idxs[env_idx]
                        target = self.config.task.extra_steps
                        if self.config.log_only_behavior_intervals:
                            self.simulator.should_log[env_idx] = current == target
                            self._is_last_step[env_idx] = current == target
                        else:
                            self.simulator.should_log[env_idx] = current < target
                    else:
                        self.simulator.should_log[env_idx] = False

            # Determine if data should be logged for each env
            orig_should_log = self.simulator.should_log.copy()
            if self.config.log_only_behavior_intervals:
                # Log data if data logging is still active for the env and
                # a sub-behavior transition is occurring
                for env_idx, behavior in enumerate(self.behaviors):
                    should_log = orig_should_log[env_idx]
                    is_transition = behavior.is_subbehavior_transition()
                    is_last_step = self._is_last_step[env_idx]
                    env_should_log = should_log and (is_transition or is_last_step)
                    self.simulator.should_log[env_idx] = env_should_log
            else:
                # If step is not at the log interval, don't want to cache data for any env
                if self._step_idx % self.config.log_interval != 0:
                    self.simulator.should_log = [False] * self.config.n_envs

            # Update the behavior params that will be logged when execution is complete
            self._behavior_params = [b.get_behavior_params()["behaviors"] for b in self.behaviors]
            # Save hierarchical behavior string so we know behavior active at every timestep
            auxiliary_data = {"behavior": [b.get_behavior_string() for b in self.behaviors]}

            self.simulator.step(post_physics, auxiliary_data)
            self.simulator.should_log = orig_should_log

        if increment_step_idx:
            self._step_idx += 1

    def _pre_physics_step(self):
        """
        Perform operations prior to a simulator step. Primarily computes actions from behaviors.
        """
        for env_idx in range(self.config.n_envs):
            if self.behaviors[env_idx].is_complete():
                continue
            env_state = self.simulator.get_env_state(env_idx)
            action = self.behaviors[env_idx].get_action(env_state)
            if action is not None:
                self.simulator.apply_action(
                    action,
                    env_idx,
                    adapt_for_objects=self.behaviors[env_idx].adapt_action_for_objects(),
                )

    def is_complete(self):
        """
        Determines when this task is complete.
        """
        if self.config.n_steps > 0:
            is_complete = self._step_idx >= self.config.n_steps
        else:
            behaviors_complete = all([b.is_complete() for b in self.behaviors])
            extra_steps_complete = all(
                [s >= self.config.task.extra_steps for s in self._extra_step_idxs]
            )
            is_complete = behaviors_complete and extra_steps_complete
        return is_complete

    def get_n_demos(self):
        return len(file_util.list_dir(self.config.data_root, ".pickle"))

    def reset(self):
        """
        Resets the environments, behaviors, and data caches.
        """
        self.simulator.reset()
        self._reset_behaviors()
        self._step_idx = 0
        self._extra_step_idxs = [0] * self.config.n_envs
        self._is_last_step = [False] * self.config.n_envs
        self._behavior_params = {}

    def _reset_behaviors(self):
        """
        Resets the behaviors for each env.
        """
        self.behaviors = [
            Behavior(
                self.config.task.behavior,
                self.simulator.robot,
                self.config.env,
                self.simulator,
                self.config.device,
                self.config.open_loop,
            )
            for _ in range(self.config.n_envs)
        ]
