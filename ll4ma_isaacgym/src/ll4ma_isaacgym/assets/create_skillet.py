#!/usr/bin/env python
"""
This script is a bit of a hack to get around the abysmal mesh for the YCB
skillet, they left in all sorts of bumps and artifacts that was proving
difficult to smooth out, and they scanned it with the lid on it.

This script uses trimesh to construct the skillet from primitive shapes,
and then takes the handle from the actual mesh and attaches it.
"""

import trimesh


if __name__ == "__main__":

    n_sections = 100

    wall_thickness = 0.003  # Measured with caliper
    wall_diameter = 10.5 * 0.0254  # 10.5in skillet converted to m
    wall_outer_r = wall_diameter / 2.0
    wall_inner_r = wall_outer_r - wall_thickness
    wall_height = 0.06625

    base_r = wall_outer_r
    base_height = wall_thickness

    wall = trimesh.creation.annulus(wall_inner_r, wall_outer_r, wall_height, n_sections)
    base = trimesh.creation.cylinder(base_r, base_height, n_sections)
    base.vertices[:, 2] -= wall_height / 2.0
    base.vertices[:, 2] += wall_thickness / 2.0  # Make base flush to bottom of wall

    core = trimesh.util.concatenate([base, wall])
    # core.show()

    # Get the handle from the YCB mesh
    ycb = trimesh.load("skillet_ycb.stl")
    handle = trimesh.intersections.slice_mesh_plane(ycb, (0, -1, 0), (0, -0.043, 0), cap=True)
    handle.vertices[:, 1] -= 0.086  # Translate to edge of wall
    handle.vertices[:, 2] += 0.030  # Translate up to meet weld point
    # handle.show()

    skillet = trimesh.util.concatenate([core, handle])
    for facet in skillet.facets:
        skillet.visual.face_colors = (130, 110, 91, 255)
        skillet.visual.vertex_colors = (130, 110, 91, 255)

    skillet.show()
    skillet.export("skillet.stl")
