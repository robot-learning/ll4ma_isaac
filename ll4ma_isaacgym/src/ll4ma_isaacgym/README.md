
Steps if you need to add a new task:
1. add python file with the new task class in it to `behaviors/`
2. add the class to the `behaviors/__init__.py`
3. add the corresponding config dataclass to `core/config.py`
4. add the config class to `core/__init__.py`
5. add the task to the VALID_TASKS list at the top of `core/config.py` (line 17)
