#!/usr/bin/env python
import os
import rospy
from visualization_msgs.msg import MarkerArray

from ll4ma_util import file_util, ros_util, ui_util


COLORS = ["dodgerblue", "mediumseagreen"]
TEXT_SIZE = 0.05

marker_idx = 0
color_idx = 0


def add_marker(marker_list, marker):
    global marker_idx
    marker.id = marker_idx
    marker_idx += 1
    marker_list.append(marker)


def get_behavior_markers(params, data, attrs):
    global color_idx

    markers = []

    ui_util.print_dict(params)

    for name, behavior_params in params.items():
        color = COLORS[color_idx % len(COLORS)]
        color_idx += 1

        if behavior_params["type"] in ["PickObject", "PlaceObject"]:
            obj_name = behavior_params["target_object"]
            obj_config = attrs["objects"][obj_name]
            if obj_config["asset_filename"]:
                # TODO this assumes assets are all in isaac gym asset dir
                asset_dir = "package://ll4ma_isaacgym/src/ll4ma_isaacgym/assets"
                urdf_path = f"{asset_dir}/{obj_config['asset_filename']}"
                mesh_path = ros_util.get_mesh_filename_from_urdf(urdf_path)
                mesh_filename = mesh_path.split("/")[-1]
                mesh_resource = f"{asset_dir}/{mesh_filename}"

                idxs = []
                for i, behavior_string in enumerate(data["behavior"]):
                    subs = behavior_string.split("__")
                    for sub in subs:
                        if name == sub:
                            idxs.append(i)
                if len(idxs) < 2:
                    raise ValueError(f"Not sure what to do without more timesteps for {name}")
                start_idx = idxs[0]
                end_idx = idxs[-1]

                obj_start_pos = data["objects"][obj_name]["position"][start_idx]
                obj_start_quat = data["objects"][obj_name]["orientation"][start_idx]
                obj_end_pos = data["objects"][obj_name]["position"][end_idx]
                obj_end_quat = data["objects"][obj_name]["orientation"][end_idx]

                start_mesh = ros_util.get_marker_msg(
                    obj_start_pos,
                    obj_start_quat,
                    shape="mesh",
                    mesh_resource=mesh_resource,
                    marker_id=marker_idx,
                    alpha=0.3,
                    color=color,
                )
                start_text = ros_util.get_marker_msg(
                    obj_start_pos,
                    obj_start_quat,
                    shape="text",
                    text=f"{name} start",
                    color="white",
                    scale=[TEXT_SIZE] * 3,
                )
                end_mesh = ros_util.get_marker_msg(
                    obj_end_pos,
                    obj_end_quat,
                    shape="mesh",
                    mesh_resource=mesh_resource,
                    alpha=0.9,
                    marker_id=marker_idx,
                    color=color,
                )
                end_text = ros_util.get_marker_msg(
                    obj_end_pos,
                    obj_end_quat,
                    shape="text",
                    text=f"{name} end",
                    color="white",
                    scale=[TEXT_SIZE] * 3,
                )

                add_marker(markers, start_mesh)
                add_marker(markers, start_text)
                add_marker(markers, end_mesh)
                add_marker(markers, end_text)
        elif "behaviors" in behavior_params and behavior_params["behaviors"]:
            markers += get_behavior_markers(behavior_params["behaviors"], data, attrs)
        else:
            raise ValueError("Not sure how to handle params")

    return markers


if __name__ == "__main__":
    rospy.init_node("visualize_behavior_params")
    filename = rospy.get_param("~filename")

    file_util.check_path_exists(filename, "Data file")

    data, attrs = file_util.load_pickle(filename)

    data_dir = os.path.dirname(filename)
    metadata_filename = os.path.join(data_dir, "metadata.yaml")
    file_util.check_path_exists(metadata_filename, "Data collection metadata file")
    metadata = file_util.load_yaml(metadata_filename)

    env = MarkerArray()
    behaviors = MarkerArray()
    behaviors.markers = get_behavior_markers(attrs["behavior_params"], data, attrs)
    behaviors_pub = rospy.Publisher("/behavior_markers", MarkerArray, queue_size=1)
    env_pub = rospy.Publisher("/env", MarkerArray, queue_size=1)

    table_config = metadata["env"]["objects"]["table"]
    env.markers.append(
        ros_util.get_marker_msg(
            table_config["position"],
            table_config["orientation"],
            shape="cube",
            scale=table_config["extents"],
            color=table_config["rgb_color"],
        )
    )

    rate = rospy.Rate(1)
    rospy.loginfo("Visualizing behavior params...")
    while not rospy.is_shutdown():
        behaviors_pub.publish(behaviors)
        env_pub.publish(env)
        rate.sleep()
