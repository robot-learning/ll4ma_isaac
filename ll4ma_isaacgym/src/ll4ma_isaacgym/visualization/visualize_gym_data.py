#!/usr/bin/env python
import rospy
import pickle

from sensor_msgs.msg import JointState, Image
from tf2_msgs.msg import TFMessage
from visualization_msgs.msg import Marker, MarkerArray

from ll4ma_util import file_util, vis_util, ros_util


def get_marker_msg(objects, tf, idx):
    marker_msg = MarkerArray()
    for i, (obj_id, obj_data) in enumerate(objects.items()):
        marker = Marker()
        marker.id = i
        marker.type = Marker.CUBE
        marker.action = Marker.MODIFY
        marker.pose.position.x = tf[obj_id]["position"][idx][0]
        marker.pose.position.y = tf[obj_id]["position"][idx][1]
        marker.pose.position.z = tf[obj_id]["position"][idx][2]
        marker.pose.orientation.x = tf[obj_id]["orientation"][idx][0]
        marker.pose.orientation.y = tf[obj_id]["orientation"][idx][1]
        marker.pose.orientation.z = tf[obj_id]["orientation"][idx][2]
        marker.pose.orientation.w = tf[obj_id]["orientation"][idx][3]
        marker.header.frame_id = tf[obj_id]["parent_frame"]
        marker.scale.x = obj_data["x_extent"]
        marker.scale.y = obj_data["y_extent"]
        marker.scale.z = obj_data["z_extent"]
        marker.color.a = 1.0
        marker.color.r = obj_data["rgb_color"][0]
        marker.color.g = obj_data["rgb_color"][1]
        marker.color.b = obj_data["rgb_color"][2]
        marker_msg.markers.append(marker)
    return marker_msg


def visualize_data(filename, rate_val=60):
    if filename.endswith(".pickle"):
        rospy.loginfo("Loading pickle data...")
        with open(filename, "rb") as f:
            data, attrs = pickle.load(f)
        rgb = data["rgb"]
        depth = data["depth"]
        seg = data["segmentation"]
        seg_ids = attrs["segmentation_ids"]
        joint_pos = data["joint_position"]
        joint_tau = data["joint_torque"]
        joint_names = attrs["joint_names"]
        tf = data["objects"]
        for obj_name, obj_data in tf.items():
            tf[obj_name]["parent_frame"] = attrs["objects"][obj_name]["parent_frame"]
        objects = attrs["objects"]
        rospy.loginfo("Pickle data loaded.")
    else:
        raise ValueError("Unknown file extension: {}".format(filename))

    rospy.loginfo("Creating ROS messages...")

    n_timesteps = len(rgb)

    colors = [
        "windows blue",
        "amber",
        "faded green",
        "dusty purple",
        "light red",
        "deep blue",
        "blue green",
        "pumpkin",
        "periwinkle blue",
        "lemon yellow",
        "mocha",
        "greeny yellow",
        "grey/blue",
        "dark fuchsia",
        "greyish teal",
        "eggplant purple",
        "strong blue",
    ]

    seg_colors = vis_util.random_colors(len(seg_ids), xkcd_colors=colors)

    joint_state_msgs = []
    rgb_msgs = []
    depth_msgs = []
    seg_msgs = []
    # flow_msgs = []
    tf_msgs = []
    marker_msgs = []

    for i in range(n_timesteps):
        joint_state_msgs.append(
            ros_util.get_joint_state_msg(
                joint_pos[i], joint_tau=joint_tau[i], joint_names=joint_names
            )
        )
        rgb_msgs.append(ros_util.rgb_to_msg(rgb[i]))
        depth_msgs.append(ros_util.depth_to_msg(depth[i]))
        seg_msgs.append(ros_util.seg_to_msg(seg[i], seg_ids, seg_colors))
        # flow_msgs.append(get_flow_msg(flow[i]))
        tf_msgs.append(ros_util.get_tf_msg(tf, i))
        marker_msgs.append(get_marker_msg(objects, tf, i))
    rospy.loginfo("ROS messages created.")

    joint_state_pub = rospy.Publisher("/panda/joint_states", JointState, queue_size=1)
    rgb_pub = rospy.Publisher("/rgb", Image, queue_size=1)
    depth_pub = rospy.Publisher("/depth", Image, queue_size=1)
    seg_pub = rospy.Publisher("/segmentation", Image, queue_size=1)
    # flow_pub = rospy.Publisher('/optical_flow', Image, queue_size=1)
    tf_pub = rospy.Publisher("/tf", TFMessage, queue_size=1)
    marker_pub = rospy.Publisher("/markers", MarkerArray, queue_size=1)

    rospy.loginfo("Publishing messages...")
    rate = rospy.Rate(rate_val)
    while not rospy.is_shutdown():
        for i in range(n_timesteps):
            ros_util.publish_msg(joint_state_msgs[i], joint_state_pub)
            ros_util.publish_msg(rgb_msgs[i], rgb_pub)
            ros_util.publish_msg(depth_msgs[i], depth_pub)
            ros_util.publish_msg(seg_msgs[i], seg_pub)
            # publish_msg(flow_msgs[i], flow_pub)
            ros_util.publish_tf_msg(tf_msgs[i], tf_pub)
            ros_util.publish_marker_msg(marker_msgs[i], marker_pub)
            rate.sleep()
        rospy.sleep(3)


if __name__ == "__main__":
    rospy.init_node("visualize_gym_data")

    filename = rospy.get_param("~filename")
    rate = rospy.get_param("~rate", 60)
    file_util.check_path_exists(filename, "Data file")

    visualize_data(filename, rate)
