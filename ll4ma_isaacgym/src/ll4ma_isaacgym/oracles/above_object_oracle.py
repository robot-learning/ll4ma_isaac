#!/usr/bin/env python
import numpy as np

from ll4ma_isaacgym.oracles import Oracle


class AboveObjectOracle(Oracle):
    """
    Oracle determines if the robot's end-effector is considered 'above' an object.
    """

    def __init__(self, min_z=0.01, max_z=0.4, xy_tolerance=0.04):
        """
        All units are in meters.

        Args:
            min_z: Minimum z-separation allowed between object centroid and EE
            max_z: Maximum z-separation allowed between object centroid and EE
            xy_tolerance: Absolute value of separation allowed between object centroid and EE
        """
        self.min_z = min_z
        self.max_z = max_z
        self.xy_tolerance = xy_tolerance

    def generate_labels(self, data, target_obj):
        """
        Generates labels for EE being 'above' an object. Computed as being
        within the defined position tolerances.
        """
        self._validate_data(data, target_obj)
        obj_pos = data["objects"][target_obj]["position"]
        ee_pos = data["ee_position"]
        x_diff = ee_pos[:, 0] - obj_pos[:, 0]
        y_diff = ee_pos[:, 1] - obj_pos[:, 1]
        z_diff = ee_pos[:, 2] - obj_pos[:, 2]
        z_satisfied = np.logical_and(z_diff < self.max_z, z_diff > self.min_z)
        xy_satisfied = np.logical_and(
            np.abs(x_diff) < self.xy_tolerance, np.abs(y_diff) < self.xy_tolerance
        )
        pos_satisfied = np.logical_and(z_satisfied, xy_satisfied)
        return pos_satisfied

    def get_data_where_satisfied(self, data, target_obj):
        return super().get_data_where_satisfied(data, target_obj)

    def get_first_where_satisfied(self, data, target_obj):
        return super().get_first_where_satisfied(data, target_obj)

    def _validate_data(self, data, target_obj):
        for k in ["objects", "ee_position"]:
            if k not in data:
                raise ValueError(f"No entry for '{k}' in data")
        if target_obj not in data["objects"]:
            raise ValueError(f"Target object '{target_obj}' not found in data")
        if "position" not in data["objects"][target_obj]:
            raise ValueError(f"Position not found for '{target_obj}'")


if __name__ == "__main__":
    # Test to see what it's labeling by watching RGB video with display of label
    import matplotlib.pyplot as plt
    import argparse
    from ll4ma_isaac.util import file_util

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--pickle", type=str, required=True, help="Absolute path to pickle data file"
    )
    args = parser.parse_args()
    file_util.check_path_exists(args.pickle, "Data file")

    data, attrs = file_util.load_pickle(args.pickle)
    oracle = AboveObjectOracle()
    above = oracle.generate_labels(data, "box")

    rgb = data["rgb"]

    fig, ax = plt.subplots(1, 1)
    img = ax.imshow(rgb[0])
    for i in range(len(rgb)):
        if above[i]:
            fig.suptitle("ABOVE", fontsize=30)
        else:
            fig.suptitle("")
        img.set_data(rgb[i])
        fig.canvas.draw_idle()
        plt.pause(0.02)
