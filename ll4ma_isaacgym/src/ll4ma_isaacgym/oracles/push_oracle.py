#!/usr/bin/env python
import numpy as np

from ll4ma_isaacgym.oracles import Oracle


class PushOracle(Oracle):
    """
    Oracle determines if the robot is in a pose suitable for pushing an object
    (i.e. essentially a pre-condition on executing a push action)
    """

    def __init__(self, min_z=0.01, max_z=0.05, x_tolerance=0.07, y_tolerance=0.01):
        """
        All units are in meters.

        Args:

        TODO this is hacked pretty hard for the iiwa push block, need to account for object
        size in the thresholds that are set since they are relative to obj centroid

        """
        self.min_z = min_z
        self.max_z = max_z
        self.x_tolerance = x_tolerance
        self.y_tolerance = y_tolerance

    def generate_labels(self, data, target_obj):
        """
        Generates labels for EE being 'above' an object. Computed as being
        within the defined position tolerances.
        """
        self._validate_data(data, target_obj)
        obj_pos = data["objects"][target_obj]["position"]
        ee_pos = data["ee_position"]
        x_diff = ee_pos[:, 0] - obj_pos[:, 0]

        obj_x_diff_from_start = obj_pos[:, 0] - obj_pos[0, 0]

        x_satisfied = np.abs(x_diff) < self.x_tolerance

        # Once object has moved, it's already been pushed so we don't want to
        # consider timesteps after that for pre-push
        obj_moved_x = np.abs(obj_x_diff_from_start) > 0.05
        push = np.logical_and(x_satisfied, obj_moved_x)

        return push

    def get_data_where_satisfied(self, data, target_obj):
        return super().get_data_where_satisfied(data, target_obj)

    def get_first_where_satisfied(self, data, target_obj):
        return super().get_first_where_satisfied(data, target_obj)

    def get_name(self):
        return "push"

    def _validate_data(self, data, target_obj):
        for k in ["objects", "ee_position"]:
            if k not in data:
                raise ValueError(f"No entry for '{k}' in data")
        if target_obj not in data["objects"]:
            raise ValueError(f"Target object '{target_obj}' not found in data")
        if "position" not in data["objects"][target_obj]:
            raise ValueError(f"Position not found for '{target_obj}'")


if __name__ == "__main__":
    # Test to see what it's labeling
    import matplotlib.pyplot as plt
    import argparse
    from ll4ma_util import file_util

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-f", "--filename", type=str, required=True, help="Absolute path to pickle data file"
    )
    parser.add_argument(
        "-t", "--target_obj", type=str, required=True, help="Name of target object being pushed"
    )
    parser.add_argument("-p", "--pause", type=float, default=0.02)
    parser.add_argument("--show_rgb", action="store_true")
    args = parser.parse_args()
    file_util.check_path_exists(args.filename, "Data file")

    data, attrs = file_util.load_pickle(args.filename)
    oracle = PushOracle()
    pushed = oracle.generate_labels(data, args.target_obj)
    print(pushed)

    if args.show_rgb:
        rgb = data["rgb"]
        fig, ax = plt.subplots(1, 1)
        fig.set_size_inches(6, 6)
        img = ax.imshow(rgb[0])
        for i in range(len(rgb)):
            if pushed[i]:
                fig.suptitle(f"ts={i}: PUSHED", fontsize=30)
            else:
                fig.suptitle(f"ts={i}")
            img.set_data(rgb[i])
            fig.canvas.draw_idle()
            plt.pause(args.pause)
