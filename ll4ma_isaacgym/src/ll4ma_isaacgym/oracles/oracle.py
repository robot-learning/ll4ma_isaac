import numpy as np


class Oracle:
    """
    Base clase for oracles, which operate on streams of data recorded from the robot
    doing a task, and it will label each timestep as satisfying a property or not.
    """

    def generate_labels(self, data, *args, **kwargs):
        """
        Generates labels for the data at each timestep by determining if
        it satisfies the oracle's defined conditions at each timestep.
        """
        raise NotImplementedError()

    def get_data_where_satisfied(self, data, *args, **kwargs):
        labels = self.generate_labels(data, *args, **kwargs)
        return self._get_data_where_satisfied(data, labels)

    def get_first_where_satisfied(self, data, *args, **kwargs):
        sat_data = self.get_data_where_satisfied(data, *args, **kwargs)
        return self._get_first(sat_data)

    def get_name(self):
        """
        This is for getting a useful key for the oracle, e.g. for putting in a filename
        """
        raise NotImplementedError("get_name not implemented")

    def _get_data_where_satisfied(self, data, labels):
        if isinstance(data, np.ndarray):
            return data[labels]
        elif isinstance(data, dict):
            sat_data = {}
            for k, v in data.items():
                sat_data[k] = self._get_data_where_satisfied(v, labels)
            return sat_data
        else:
            raise ValueError(f"Unknown data type: {type(data)}")

    def _get_first(self, data):
        if isinstance(data, np.ndarray):
            return data[0] if len(data) > 0 else None
        elif isinstance(data, dict):
            first_data = {}
            for k, v in data.items():
                first_data[k] = self._get_first(v)
            return first_data
        else:
            raise ValueError(f"Unknown data type: {type(data)}")
