#!/usr/bin/env python
import rospy
import torch
import numpy as np
from sensor_msgs.msg import JointState
from ll4ma_util import ros_util
from ll4ma_isaacgym.srv import GetForwardKinematics, GetForwardKinematicsRequest


if __name__ == "__main__":
    """
    Simple test to see if batch FK works (written for iiwa+reflex, 12 joints)
    """
    rospy.init_node("test_fk_service")

    tensor = torch.from_numpy(
        np.array(
            [
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, np.pi / 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, -np.pi / 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            ]
        )
    )

    req = GetForwardKinematicsRequest()
    req.joint_states = [JointState() for _ in range(3)]
    req.joint_states[0].position = tensor[0, :].numpy().tolist()
    req.joint_states[1].position = tensor[1, :].numpy().tolist()
    req.joint_states[2].position = tensor[2, :].numpy().tolist()

    req.tensor_joint_positions = tensor.flatten().numpy().tolist()
    req.n_samples = 3
    req.n_joints = tensor.size(-1)

    resp, _ = ros_util.call_service(req, "/isaacgym/get_forward_kinematics", GetForwardKinematics)
    poses = torch.from_numpy(np.array(resp.tensor_poses)).view(-1, 7)

    # These should both show the same values, one in Pose msgs the other in a tensor
    for pose in resp.poses:
        print("\n", pose)
    print("\nTENSOR POSES\n", poses)
