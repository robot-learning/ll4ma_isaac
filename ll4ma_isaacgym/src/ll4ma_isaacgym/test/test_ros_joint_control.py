#!/usr/bin/env python
import rospy
from sensor_msgs.msg import JointState

from ll4ma_isaacgym.msg import IsaacGymState


joint_state = None


def state_cb(msg):
    global joint_state
    joint_state = msg.joint_state[0]


if __name__ == "__main__":
    rospy.init_node("test_ros_joint_control")

    pub = rospy.Publisher("/panda/joint_cmd", JointState, queue_size=1)
    rospy.Subscriber("/isaacgym_state", IsaacGymState, state_cb)
    rate = rospy.Rate(60)

    rospy.loginfo("Waiting for Isaac Gym state...")
    while joint_state is None and not rospy.is_shutdown():
        rate.sleep()
    rospy.loginfo("State received!")

    increment = 0.002

    rospy.loginfo("Commanding robot")
    joint_cmd = joint_state
    while not rospy.is_shutdown():
        for _ in range(200):
            joint_cmd.position = [j + increment for j in joint_cmd.position]
            pub.publish(joint_cmd)
            rate.sleep()
        for _ in range(200):
            joint_cmd.position = [j - increment for j in joint_cmd.position]
            pub.publish(joint_cmd)
            rate.sleep()
