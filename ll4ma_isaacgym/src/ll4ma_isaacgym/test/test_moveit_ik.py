#!/usr/bin/env python
from isaacgym import gymapi  # Just to avoid gym complaining about torch imports

import os.path as osp
import rospy
import numpy as np
from copy import deepcopy

from ll4ma_isaacgym.core import SessionConfig, Simulator, DEFAULT_TASK_CONFIG_DIR

from moveit_interface import util as moveit_util

gymapi.Version  # Appease flake8


def set_joints(sim, joints):
    reflex_joints = np.zeros(5)
    sim.robot.set_init_joint_position(np.concatenate([joints, reflex_joints]))
    sim.reset()


def get_joints_and_ee_pose(sim):
    state = deepcopy(sim.get_env_state(0))  # Copy needed otherwise it overwrites old values
    joints = state.joint_position.squeeze().cpu().numpy()
    ee_pose = state.ee_state[:7].squeeze().cpu().numpy()
    return joints, ee_pose


if __name__ == "__main__":
    rospy.init_node("test_moveit_ik")

    cfg_fn = osp.join(DEFAULT_TASK_CONFIG_DIR, "iiwa_push_cleaner.yaml")
    cfg = SessionConfig(config_filename=cfg_fn)
    sim = Simulator(cfg)

    np.set_printoptions(precision=4)

    for _ in range(100):
        rand_joints = np.random.uniform(-1.0, 1.0, 7)
        set_joints(sim, rand_joints)

        start_joints, start_ee_pose = get_joints_and_ee_pose(sim)
        rospy.sleep(1)  # Sleep to display in viewer

        ik_resp, _ = moveit_util.get_ik([start_ee_pose], "reflex_palm_link")
        soln_joints = np.array(ik_resp.solutions[0].position)
        set_joints(sim, soln_joints)

        new_joints, new_ee_pose = get_joints_and_ee_pose(sim)
        print("\nSTART JOINTS", start_joints[:7])
        print("NEW JOINTS  ", new_joints[:7])
        print("START EE POSE", start_ee_pose)
        print("NEW EE POSE  ", new_ee_pose)
        rospy.sleep(1)
