import numpy as np
from copy import deepcopy


class Arm:
    """
    Arm interface to simplify interactions in the Simulator.
    """

    def __init__(self, arm_config):
        """
        Args:
            arm_config (ArmConfig): Likely an extending class of ArmConfig that
            defines robot-specific configuration
        """
        self.config = arm_config

        # Maintain init joint pos that can change (e.g. when setting env to be the
        # same as from a previously recorded session). This then keeps the robot's
        # configured default joint position the same
        self.init_joint_pos = deepcopy(self.config.default_joint_pos)

    def get_name(self):
        return self.config.name

    def get_init_joint_position(self, randomize=False):
        joint_pos = deepcopy(self.init_joint_pos)
        if randomize:
            # TODO might need to make this more intelligent if there are non-symmetric axes
            # TODO only randomizing arm for the moment
            max_eps = self.config.joint_pos_sample_range
            joint_pos += np.random.uniform(-max_eps, max_eps, self.num_joints())
        return joint_pos

    def set_init_joint_position(self, joint_pos):
        self.init_joint_pos = joint_pos

    def get_default_joint_position(self):
        return self.config.default_joint_pos

    def num_joints(self):
        return self.config.n_joints

    def update_action_joint_pos(self, action, env_state):
        """
        Compute low-level joint position command to execute in simulator.
        Currently this is a pass-through function but functionality can
        be added if discrete action modes or higher-level action interfaces
        are used that need to be computed down to low-level commands based
        on information from the robot's configuration.

        See EndEffector class for an example, that has e.g. discrete
        open/close commands for which joint commands are computed.
        """
        pass
