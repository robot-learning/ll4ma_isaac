from ll4ma_isaacgym.core import util as gym_util


class Robot:
    """
    Robot interface for easily interfacing with both the arm and the EE.
    """

    def __init__(self, robot_config, n_envs=1):
        """
        Args:
            robot_config (RobotConfig): Likely an extending class of RobotConfig that
            defines robot-specific configuration (including arm and end-effector)
        """
        self.config = robot_config
        self.n_envs = n_envs
        self.arm = gym_util.get_arm(robot_config.arm)
        self.end_effector = gym_util.get_end_effector(robot_config.end_effector, n_envs)
        self.rb_indices = {}

    def set_init_joint_position(self, joint_pos):
        self.init_joint_pos = joint_pos
        self.arm.set_init_joint_position(joint_pos[: self.arm.num_joints()])
        self.end_effector.set_init_joint_position(joint_pos[self.arm.num_joints() :])

    def has_end_effector(self):
        return self.end_effector is not None

    def has_rb_index(self, link_name):
        return link_name in self.rb_indices

    def get_name(self):
        return self.config.name

    def reset_rb_indices(self, link_name=None):
        if link_name is not None:
            self.rb_indices[link_name] = []
        else:
            self.rb_indices = {}

    def add_rb_index(self, link_name, rb_idx):
        if link_name not in self.rb_indices:
            self.rb_indices[link_name] = []
        self.rb_indices[link_name].append(rb_idx)

    def num_joints(self):
        return self.arm.num_joints() + self.end_effector.num_joints()
