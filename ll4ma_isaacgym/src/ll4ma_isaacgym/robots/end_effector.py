import numpy as np
from copy import deepcopy

import torch


class EndEffector:
    """
    End-effector interface to simplify interactions in the Simulator.
    """

    def __init__(self, ee_config, n_envs=1):
        """
        Args:
            ee_config (EndEffectorConfig): EE-specific configuration object, this
            will likely be an extending dataclass of EndEffectorConfig.
        """
        self.config = ee_config
        self.n_envs = n_envs
        self.rb_indices = []

        self.init_joint_pos = deepcopy(self.config.default_joint_pos)

        self.reset()

    def get_default_joint_position(self):
        return self.config.default_joint_pos

    def get_rb_index(self, env_idx):
        return self.rb_indices[env_idx]

    def get_link(self):
        return self.config.link

    def get_touch_links(self):
        """
        Returns touch links for this end-effector. These are used when attaching
        objects to the end-effector for planning in MoveIt while accounting for
        a grasped object. Touch links are links (e.g. fingers) that you want to
        ignore contacts for w.r.t. the attached object.
        """
        return self.config.touch_links

    def get_init_joint_position(self, randomize=False):
        joint_pos = deepcopy(self.init_joint_pos)
        if randomize:
            # TODO need to make this more intelligent for non-symmetric axes,
            # more common for EE fingers
            max_eps = self.config.joint_pos_sample_range
            joint_pos += np.random.uniform(-max_eps, max_eps, self.num_joints())
        return joint_pos

    def set_init_joint_position(self, joint_pos):
        self.init_joint_pos = joint_pos

    def add_rb_index(self, rb_idx):
        self.rb_indices.append(rb_idx)

    def update_action_joint_pos(
        self, ee_action, ee_joint_pos, env_idx=None, adapt_for_objects=[], sim=None
    ):
        """
        Compute joint position action. This accounts for different action
        parameterizations that can all in the end compute down to low-level
        joint angles that can be directly commanded to Isaac Gym.

        If you need more ee-specific functionality than is being offered by
        EndEffectorAction class, you should extend this class for your
        end-effector, and override this function.

        TODO currently this is only designed for gripping an object, can
        make more general by adding more discrete action types and doing
        switches here to handle each case.
        """
        n_envs = ee_action.size(0)
        assert ee_joint_pos.size(0) == n_envs
        update_env_idx = env_idx

        # Only compute action for joints involved in gripping the object
        grip_idxs = torch.tensor(self.config.grip_finger_indices, device=ee_action.device)
        open_pos = torch.tensor(self.config.open_finger_joint_pos, device=ee_action.device)
        close_pos = torch.tensor(self.config.close_finger_joint_pos, device=ee_action.device)

        env_idxs = torch.arange(n_envs)
        close_idxs = env_idxs[ee_action[:, 0] == self.config.discretes.close]
        open_idxs = env_idxs[ee_action[:, 0] == self.config.discretes.open]

        updated_pos = ee_joint_pos.clone()
        current_pos = ee_joint_pos[:, grip_idxs]
        # Compute diff between current joints and open/close configurations
        diff = torch.zeros_like(current_pos)
        if len(close_idxs) > 0:
            diff[close_idxs] = close_pos - current_pos[close_idxs]
        if len(open_idxs) > 0:
            diff[open_idxs] = open_pos - current_pos[open_idxs]

        # Compute the instantaneous change to move in direction of open/close configuration
        max_change = torch.full_like(diff, self.config.interpolate_gap)
        min_change = torch.full_like(diff, -self.config.interpolate_gap)
        change = torch.max(torch.min(diff, max_change), min_change)

        if adapt_for_objects:
            if sim is None:
                raise ValueError("Must pass in sim when adapting for contact with objects")
            for env_idx in range(n_envs):
                # TODO this is a little wonky, but if we're updating a specific env then
                # the input data only has 1 row, but we need to know its associated
                # global env idx for the sake of tracking things
                if update_env_idx is None:
                    update_env_idx = env_idx

                fingers_stopped = self.fingers_stopped[update_env_idx]
                for finger_idx in range(self.num_fingers()):
                    if not fingers_stopped[finger_idx]:
                        contacts = sim.get_env_contacts(
                            update_env_idx,
                            self.config.finger_contact_links[finger_idx] + adapt_for_objects,
                        )
                        if len(contacts) > 0:
                            change[env_idx, finger_idx] = 0.0
                            self.fingers_stopped[update_env_idx][finger_idx] = True
                if self.all_fingers_stopped(update_env_idx) and not self.grasped[update_env_idx]:
                    change[env_idx, :] += self.config.tighten_amount
                    self.grasped_joints[update_env_idx] = current_pos[env_idx] + change[env_idx]
                    self.grasped[update_env_idx] = True

                if self.grasped[update_env_idx]:
                    # Zero out the change, we don't want to do anything else with the fingers
                    updated_pos[env_idx, grip_idxs] = self.grasped_joints[update_env_idx]
                else:
                    updated_pos[env_idx, grip_idxs] = current_pos[env_idx] + change[env_idx]

                # TODO having to reset this so it works for multi-env, this is not a great
                # way to do this so might need to re-write this logic more cleanly
                update_env_idx = None
        else:
            updated_pos[:, grip_idxs] = current_pos + change

        return updated_pos

    def num_joints(self):
        return self.config.n_joints

    def num_fingers(self):
        return self.config.n_fingers

    def all_fingers_stopped(self, env_idx):
        return all(self.fingers_stopped[env_idx])

    def reset(self):
        self.fingers_stopped = [[False] * self.num_fingers() for _ in range(self.n_envs)]
        self.grasped = [False] * self.n_envs
        self.grasped_joints = [None] * self.n_envs

    def reset_env(self, env_idx):
        self.fingers_stopped[env_idx] = [False] * self.num_fingers()
        self.grasped[env_idx] = False
        self.grasped_joints[env_idx] = None
