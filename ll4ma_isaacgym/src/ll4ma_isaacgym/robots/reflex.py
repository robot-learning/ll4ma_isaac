from ll4ma_isaacgym.robots import EndEffector


class Reflex(EndEffector):
    def __init__(self, ee_config, n_envs):
        super().__init__(ee_config, n_envs)
