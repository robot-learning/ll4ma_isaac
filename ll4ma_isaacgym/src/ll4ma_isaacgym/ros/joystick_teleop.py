#!/usr/bin/env python
import sys
import rospy

from ll4ma_isaacgym.msg import IsaacGymState
from ll4ma_teleop.msg import TeleopCommand
from sensor_msgs.msg import Joy


# Axes
LEFT_JOY_LR = 0
LEFT_JOY_UD = 1
RIGHT_JOY_LR = 3
RIGHT_JOY_UD = 4


joy_state = None
gym_state = None


def joy_cb(msg):
    global joy_state
    joy_state = msg


def gym_cb(msg):
    global gym_state
    gym_state = msg


if __name__ == "__main__":
    rospy.init_node("joystick_teleop")

    # TODO can pass topic names as params
    rospy.Subscriber("/isaacgym_state", IsaacGymState, gym_cb)
    rospy.Subscriber("/joy", Joy, joy_cb)
    teleop_cmd_pub = rospy.Publisher("/iiwa/teleop_cmd", TeleopCommand, queue_size=1)

    rate = rospy.Rate(100)
    x_scale = -0.01
    y_scale = -0.01
    z_scale = 0.01

    rospy.loginfo("Waiting for Isaac Gym state and joystick state...")
    while not rospy.is_shutdown() and (gym_state is None or joy_state is None):
        rate.sleep()
    rospy.loginfo("States received!")

    if len(gym_state.ee_pose) != 1:
        rospy.logerr(
            f"Must have exactly 1 gym environment running, you have {len(gym_state.ee_pose)}"
        )
        sys.exit(1)

    teleop_cmd = TeleopCommand()
    # TODO for now just activating position, can add rot later
    teleop_cmd.factor = [1, 1, 1, 0, 0, 0]
    teleop_cmd.pose = gym_state.ee_pose[0]

    rospy.loginfo("Commanding relative pose...")
    while not rospy.is_shutdown():
        teleop_cmd.pose.position.x += x_scale * joy_state.axes[LEFT_JOY_UD]
        teleop_cmd.pose.position.y += y_scale * joy_state.axes[LEFT_JOY_LR]
        teleop_cmd.pose.position.z += z_scale * joy_state.axes[RIGHT_JOY_UD]
        teleop_cmd_pub.publish(teleop_cmd)
        rate.sleep()
