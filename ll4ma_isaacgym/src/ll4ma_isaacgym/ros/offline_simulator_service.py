#!/usr/bin/env python
import os
import rospy
import numpy as np

from geometry_msgs.msg import Pose

from ll4ma_util import file_util
from ll4ma_isaacgym.core import DEFAULT_TASK_CONFIG_DIR, SessionConfig, Simulator
from ll4ma_isaacgym.srv import GetForwardKinematics, GetForwardKinematicsResponse

import torch


class OfflineSimulatorService:
    """
    Offers ROS services for leveraging an Isaac Gym simulator separate from
    the main simulation, e.g. computing forward kinematics in batch.

    TODO maybe there's a nice way to integrate the OfflineSimulator from core here.
    That one is meant for multiprocessing but will do similar functionality
    """

    def __init__(self, session_config, rate=100):
        self.config = session_config
        self.sim = Simulator(self.config)
        self.rate = rate

        # Services offered by this node
        rospy.Service(
            "isaacgym/get_forward_kinematics", GetForwardKinematics, self.forward_kinematics
        )

    def run(self):
        rate = rospy.Rate(self.rate)
        rospy.loginfo("Isaac Gym services for offline simulator are ready")
        while not rospy.is_shutdown():
            rate.sleep()

    def forward_kinematics(self, req):
        """
        Forward kinematics service for computing FK in batch.
        """
        resp = GetForwardKinematicsResponse()
        dof_state = self.sim.get_dof_state()  # (n_envs, n_dofs, 2), last dim pos/vel
        n_batch = 0

        # Process the list of JointState msgs version if populated
        if len(req.joint_states) > 0:
            # Iterate over request JointState msgs to populate tensor buffer for batch FK calls
            for req_idx, joint_state in enumerate(req.joint_states):
                if len(joint_state.position) != dof_state.size(1):
                    rospy.logerr(
                        f"Joint state in FK request of size {len(joint_state.position)} "
                        f"but expected size {dof_state.size(1)}"
                    )
                    resp.success = False
                    return resp
                if n_batch == self.config.n_envs:
                    # Buffer is full, need to compute FK and reset buffer
                    resp.poses += self._forward_kinematics(dof_state, n_batch, True)
                    n_batch = 0
                dof_state[n_batch, :, 0] = torch.from_numpy(np.array(joint_state.position))
                n_batch += 1

            # Handle any remaining FK calls in the buffer
            if n_batch > 0:
                resp.poses += self._forward_kinematics(dof_state, n_batch, True)

        # Process the tensor version if populated
        if len(req.tensor_joint_positions) > 0:
            if req.n_joints != dof_state.size(1):
                rospy.logerr(
                    f"Joint state in FK request of size {req.n_joints} "
                    f"but expected size {dof_state.size(1)}"
                )
                resp.success = False
                return resp
            joint_pos = torch.from_numpy(np.array(req.tensor_joint_positions)).to(dof_state.device)
            joint_pos = joint_pos.view(req.n_samples, req.n_joints)
            batches = torch.split(joint_pos, self.config.n_envs)
            poses = []
            for batch in batches:
                n_batch = batch.size(0)
                dof_state[:n_batch, :, 0] = batch
                poses.append(self._forward_kinematics(dof_state, n_batch))
            poses = torch.cat(poses, dim=0)
            resp.tensor_poses = poses.flatten().numpy().tolist()

        resp.success = True
        return resp

    def _forward_kinematics(self, dof_state, n_batch=0, return_msgs=False):
        """
        Helper function to compute FK in batch.

        Args:
            dof_state (Tensor): Tensor of joint states (pos/vel), shape (n_envs, 2*n_dofs)
            max_env_idx (int): Highest index of env to read pose info out for. Defaults to reading
                               out all envs. This allows ignoring envs for which joint state was
                               not actually populated if n_envs exceeds the number of FK requests.
        Returns:
            poses (List[Pose]): List of Pose messages resulting from FK computation.
        """
        if n_batch <= 0:
            return []

        self.sim.set_dof_state(dof_state)
        self.sim.refresh_rigid_body_state()
        ee_state = self.sim.get_ee_state()  # (n_envs, 13), last dim pos(3)/quat(4)/vel(6)

        if return_msgs:
            poses = []
            for i in range(n_batch):
                pose = Pose()
                pose.position.x = ee_state[i, 0]
                pose.position.y = ee_state[i, 1]
                pose.position.z = ee_state[i, 2]
                pose.orientation.x = ee_state[i, 3]
                pose.orientation.y = ee_state[i, 4]
                pose.orientation.z = ee_state[i, 5]
                pose.orientation.w = ee_state[i, 6]
                poses.append(pose)
        else:
            poses = ee_state[:n_batch, :7]
        return poses


if __name__ == "__main__":
    rospy.init_node("isaacgym_offline_sim", anonymous=True)

    config_name = rospy.get_param("~config")
    config_dir = rospy.get_param("~config_dir", DEFAULT_TASK_CONFIG_DIR)
    rate = rospy.get_param("~rate", 100)
    n_envs = rospy.get_param("~n_envs", 1)

    config_filename = os.path.join(config_dir, config_name)
    file_util.check_path_exists(config_filename)
    config = SessionConfig(config_filename=config_filename)
    config.sim.render_graphics = False
    config.task.include_rgb_in_state = False
    config.task_include_depth_in_state = False
    config.n_envs = n_envs

    offline_sim = OfflineSimulatorService(config, rate)
    offline_sim.run()
