#!/usr/bin/env python
import sys
import rospy
from pynput import keyboard

from ll4ma_isaacgym.msg import IsaacGymState
from ll4ma_teleop.msg import TeleopCommand


pressed_key = None
state = None


def on_press(key):
    global pressed_key
    pressed_key = key


def on_release(key):
    global pressed_key
    pressed_key = None


def state_cb(msg):
    global state
    state = msg


if __name__ == "__main__":
    rospy.init_node("keyboard_teleop")

    listener = keyboard.Listener(on_press=on_press, on_release=on_release)
    listener.start()

    # TODO can pass topic names as params
    rospy.Subscriber("/isaacgym_state", IsaacGymState, state_cb)
    teleop_cmd_pub = rospy.Publisher("/panda/teleop_cmd", TeleopCommand, queue_size=1)

    rate = rospy.Rate(100)
    pos_increment = 0.01

    rospy.loginfo("Waiting for Isaac Gym state...")
    while not rospy.is_shutdown() and state is None:
        rate.sleep()
    rospy.loginfo("State received!")

    if len(state.ee_pose) != 1:
        rospy.logerr(f"Must have exactly 1 gym environment running, you have {len(state.ee_pose)}")
        sys.exit(1)

    teleop_cmd = TeleopCommand()
    # TODO for now just activating position, can add rot later and/or switch from GUI
    teleop_cmd.factor = [1, 1, 1, 0, 0, 0]
    teleop_cmd.pose = state.ee_pose[0]

    rospy.loginfo("Commanding relative pose...")
    while not rospy.is_shutdown():
        if pressed_key == keyboard.Key.up:
            teleop_cmd.pose.position.x -= pos_increment
        elif pressed_key == keyboard.Key.down:
            teleop_cmd.pose.position.x += pos_increment
        elif pressed_key == keyboard.Key.left:
            teleop_cmd.pose.position.y -= pos_increment
        elif pressed_key == keyboard.Key.right:
            teleop_cmd.pose.position.y += pos_increment
        else:
            try:
                letter = pressed_key.char
            except AttributeError:
                continue
            if letter == "w":
                teleop_cmd.pose.position.z += pos_increment
            elif letter == "s":
                teleop_cmd.pose.position.z -= pos_increment

        teleop_cmd_pub.publish(teleop_cmd)
        rate.sleep()
