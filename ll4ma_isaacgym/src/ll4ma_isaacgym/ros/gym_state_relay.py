#!/usr/bin/env python
import rospy
from sensor_msgs.msg import JointState, Image
from ll4ma_isaacgym.msg import IsaacGymState


gym_state = None


def state_cb(msg):
    global gym_state
    gym_state = msg


if __name__ == "__main__":
    """
    Helper node that will breakout individual state components from Isaac Gym state and
    relay to their own topics. Useful if you want to view a particular env in ROS.
    """
    rospy.init_node("gym_state_relay")

    # TODO can pass topic names in as params
    rospy.Subscriber("/isaacgym_state", IsaacGymState, state_cb)
    joint_state_pub = rospy.Publisher("/iiwa/joint_states", JointState, queue_size=1)
    rgb_pub = rospy.Publisher("/rgb", Image, queue_size=1)
    depth_pub = rospy.Publisher("/depth", Image, queue_size=1)

    rate = rospy.Rate(100)

    rospy.loginfo("Waiting for Isaac Gym state...")
    while not rospy.is_shutdown() and gym_state is None:
        rate.sleep()
    rospy.loginfo("State received!")

    rospy.loginfo("Relaying Gym state to individual topics...")
    while not rospy.is_shutdown():
        # TODO can make the index a param so you can select which env to relay if there are multiple
        env_idx = 0

        joint_state = gym_state.joint_state[env_idx]
        joint_state.header.stamp = rospy.Time.now()
        joint_state_pub.publish(joint_state)

        rgb = gym_state.rgb[env_idx]
        rgb_pub.publish(rgb)

        depth = gym_state.depth[env_idx]
        depth_pub.publish(depth)

        rate.sleep()
