#!/usr/bin/env python
import os
import pickle
import argparse
import numpy as np
import matplotlib.pyplot as plt
import open3d as o3d


def print_data(data, indent=""):
    if not isinstance(data, dict):
        raise ValueError("Something is wrong, was expecting dictionary as input")
    for k, v in data.items():
        if isinstance(v, np.ndarray):
            print(indent + k, v.shape)
        elif isinstance(v, dict):
            print(indent + k)
            print_data(v, indent + "    ")
        else:
            raise ValueError(f"Unknown datatype: {type(v)}")


def print_attrs(attrs, indent=""):
    if not isinstance(attrs, dict):
        raise ValueError("Something is wrong, was expecting dictionary as input")
    for k, v in attrs.items():
        if isinstance(v, dict):
            print(indent + k)
            print_attrs(v, indent + "    ")
        else:
            print(indent + k, v)


def show_rgb_video(data, step_to_visualize=0):
    if "rgb" not in data:
        raise ValueError("The key 'rgb' should be in the data, something is wrong")

    plt.axis("off")
    img = None
    rgb = data["rgb"]
    if True:
        if img is None:
            img = plt.imshow(rgb[step_to_visualize])
        else:
            img.set_data(rgb[step_to_visualize])
        plt.pause(3)
        plt.draw()


if __name__ == "__main__":
    """
    Simple script to change rgbd image to point cloud and voxel at a specific time step.
    Leverage open3d to visualize it. Save and reload the point cloud.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--filename", type=str, required=True)
    parser.add_argument(
        "--sample_time_step",
        type=int,
        default=50,
        help="transfer the rgbd to point cloud and voxel at a spacific time step",
    )
    args = parser.parse_args()

    if not os.path.exists(args.filename):
        print(f"Pickle file does not exist: {args.filename}")

    with open(args.filename, "rb") as f:
        data, attrs = pickle.load(f)

    print("=================== DATA ===================")
    print_data(data)

    print("=================== ATTR ===================")
    print_attrs(attrs)

    sample_time_step = args.sample_time_step
    rgb = data["rgb"][sample_time_step]
    print(rgb.shape)
    rgb_o3d = o3d.geometry.Image(rgb)
    depth = data["depth"][sample_time_step]
    depth = depth.reshape(depth.shape[0], depth.shape[1], 1)
    segmentation = data["segmentation"][sample_time_step]
    projection_matrix = data["projection_matrix"][sample_time_step]
    view_matrix = data["view_matrix"][sample_time_step]
    print(depth.shape)
    print(segmentation.shape)
    print(np.max(segmentation))
    # for i in range(segmentation.shape[0]):
    #     for j in range(segmentation.shape[1]):
    #         print(segmentation[i,j])
    depth_o3d = o3d.geometry.Image(depth)
    # rgbd = o3d.geometry.create_rgbd_image_from_color_and_depth(
    rgbd = o3d.geometry.RGBDImage.create_from_color_and_depth(
        rgb_o3d, depth_o3d, convert_rgb_to_intensity=False
    )

    # fx = 1
    # cx = 1
    # fy = 1
    # cy = 1
    # print(rgbd)
    # fx = projection_matrix[0,0]
    # fy = projection_matrix[1,1]
    points = []
    points1 = []
    color = []
    cam_width = 512
    cam_height = 512
    # pcd = o3d.geometry.create_point_cloud_from_rgbd_image(
    #     rgbd,
    #     o3d.camera.PinholeCameraIntrinsic(
    #         cam_width,
    #         cam_height,
    #         fx,
    #         fy,
    #         cam_width/2.0 ,
    #         -cam_height/2.0
    #     ),
    #     view_matrix
    # )
    # pcd = o3d.geometry.create_point_cloud_from_depth_image(
    #     depth_o3d,
    #     o3d.camera.PinholeCameraIntrinsic(
    #         512, 512, fx, fy, 256.0, 256.0
    #     )
    # )
    color_map = np.array([[0, 1, 0], [0, 0, 1], [1, 0, 0], [0, 1, 1], [1, 0, 1]])

    # Retrieve depth and segmentation buffer
    depth_buffer = depth
    seg_buffer = segmentation

    # Get camera view matrix and invert it to transform points from camera to world space
    vinv = np.linalg.inv(np.matrix(view_matrix))

    # Get camera projection matrix and necessary scaling coefficients for deprojection
    proj = projection_matrix
    fu = 2 / proj[0, 0]
    fv = 2 / proj[1, 1]

    # Ignore any points which originate from ground plane or empty space
    depth_buffer[seg_buffer == 0] = -10001

    centerU = cam_width / 2
    centerV = cam_height / 2
    for i in range(cam_width):
        for j in range(cam_height):
            if depth_buffer[j, i] < -10000:
                continue
            # This will take all segmentation IDs. Can look at specific objects by
            # setting equal to a specific segmentation ID, e.g. seg_buffer[j, i] == 2
            if seg_buffer[j, i] > 0:
                u = -(i - centerU) / (cam_width)  # image-space coordinate
                v = (j - centerV) / (cam_height)  # image-space coordinate
                d = depth_buffer[j, i]  # depth buffer value
                X2 = [d * fu * u, d * fv * v, d, 1]  # deprojection vector
                p2 = X2 * vinv  # Inverse camera view to get world coordinates
                points.append([p2[0, 2], p2[0, 0], p2[0, 1]])
                points1.append([p2[0, 2][0][0], p2[0, 0][0][0], p2[0, 1][0][0]])
                color.append(0)

    print("Point Cloud Complete")
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(np.array(points1))
    o3d.io.write_point_cloud("/tmp/test_pointcloud.ply", pcd)

    # Load saved point cloud and visualize it
    pcd_load = o3d.io.read_point_cloud("/tmp/test_pointcloud.ply")
    o3d.visualization.draw_geometries([pcd_load])
    print("POINT CLOUD EMPTY?", pcd.is_empty())
    print("POINT CLOUD POINTS", np.asarray(pcd.points))
    # voxel = o3d.geometry.create_surface_voxel_grid_from_point_cloud(pcd, 0.05)
    voxel = o3d.geometry.VoxelGrid.create_from_point_cloud(pcd, 0.05)
    o3d.visualization.draw_geometries([voxel])

    show_rgb_video(data, args.sample_time_step)
