#!/usr/bin/env python
# This import is not used in this file, but they make you import this
# before any torch import, and it's simpler to just enforce this to
# be the first thing imported.
from isaacgym import gymapi

import os
import sys
import argparse
import rospy

from ll4ma_isaacgym.core import (
    SessionConfig,
    Session,
    DEFAULT_TASK_CONFIG_DIR,
    DEFAULT_ASSET_ROOT,
    VALID_TASKS,
)
from ll4ma_util import file_util, ui_util

# This line is just so flake8 doesn't complain about our bogus import above
gymapi.Version


if __name__ == "__main__":
    """
    This script is the main entry point for running engineered behaviors in Isaac Gym.

    Usage for running default task config:

        $ python run_isaacgym.py

    You can specify other task configs (default configs are in the
    ll4ma_issacgym/config directory):

        $ python run_isaacgym.py --config iiwa_2stack.yaml

    If you want to collect data:

        $ python run_isaacgym.py --data_root /path/to/save/directory

    If you want to run multiple instances sequentially:

        $ python run_isaacgym.py --n_demos 10

    If you want to run multiple envs simultaneously (each performing the same task
    but independently, can speedup data collection):

        $ python run_isaacgym.py --n_envs 8

    There are other options described in help:

        $ python run_isaacgym.py -h

    """
    rospy.init_node("run_isaacgym", anonymous=True)

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--headless", action="store_true", help="Run headless without creating a viewer window"
    )
    parser.add_argument(
        "--render_graphics",
        action="store_true",
        help=(
            "Render camera sensors if true (still needed in headless mode "
            "if you want to collect camera data)"
        ),
    )
    parser.add_argument("--no_render_graphics", action="store_false", dest="render_graphics")
    parser.add_argument(
        "-c",
        "--config",
        type=str,
        default="iiwa_pick_place_cleaner.yaml",
        help="Filename of YAML config for simulation (relative to --config_dir)",
    )
    parser.add_argument(
        "--config_dir",
        type=str,
        default=DEFAULT_TASK_CONFIG_DIR,
        help="Absolute path to root directory where sim YAML configs are",
    )
    parser.add_argument(
        "--config_str", type=str, help="String version of config (you probably don't need this)"
    )
    parser.add_argument(
        "-l",
        "--list_configs",
        action="store_true",
        help="List available environment configuration files",
    )
    parser.add_argument(
        "--n_envs",
        type=int,
        default=1,
        help="Number of envs (i.e. number of independent sims to run at once)",
    )
    parser.add_argument(
        "--n_demos",
        type=int,
        default=1,
        help="Number of data instances to collect (only relevant if collecting data)",
    )
    parser.add_argument(
        "--n_steps",
        type=int,
        default=-1,
        help=(
            "Number of timesteps to run simulation (if <0, runs until task is "
            "complete or run forever if --run_forever is set)"
        ),
    )
    parser.add_argument(
        "--data_root",
        type=str,
        help="Absolute path to directory to save data in (required if you want to collect data",
    )
    parser.add_argument(
        "--data_prefix",
        type=str,
        default="demo",
        help="Prefix to add to collected data instance filenames",
    )
    parser.add_argument(
        "--log_only_success",
        action="store_true",
        help="Save only successful task runs when saving data",
    )
    parser.add_argument(
        "--log_interval",
        type=int,
        default=1,
        help="Step interval at which to log data for data collection",
    )
    parser.add_argument(
        "--log_only_behavior_intervals",
        action="store_true",
        help="Log data for data collection only before and aftersub-behaviors complete",
    )
    parser.add_argument(
        "--demo",
        type=str,
        help=(
            "Absolute path to a pre-recorded data file (for initializing the "
            "simulator scene to the same as in recorded demo"
        ),
    )
    parser.add_argument(
        "--asset_root",
        type=str,
        default=DEFAULT_ASSET_ROOT,
        help="Absolute path to root directory of asset (e.g. resolve mesh paths)",
    )
    parser.add_argument(
        "--run_forever", action="store_true", help="Run simulation forever until process is killed"
    )
    parser.add_argument(
        "--open_loop",
        action="store_true",
        help="Run behaviors open loop (not updated based on current state)",
    )
    parser.add_argument(
        "--randomize_robot", action="store_true", help="Randomize initial robot joint configuration"
    )
    parser.add_argument(
        "--no_randomize_robot",
        dest="randomize_robot",
        action="store_false",
        help="Disable randomization of initial robot joint configuration",
    )
    parser.add_argument("--publish_ros", action="store_true", help="Publish sim data to ROS topics")
    parser.add_argument("--physx_gpu", action="store_true", help="Use PhysX GPU for physics")
    parser.add_argument(
        "--test_reset",
        type=float,
        default=0.0,
        help="Reset environment indefinitely pausing for this many secs (test env init)",
    )
    parser.set_defaults(randomize_robot=True, render_graphics=True)
    args = parser.parse_args(rospy.myargv(argv=sys.argv)[1:])

    if args.list_configs:
        ui_util.print_info("\n  Available configuration files (to pass to -c/--config option):\n\n")
        config_filenames = [f for f in os.listdir(args.config_dir) if f.endswith(".yaml")]
        for f in config_filenames:
            print(f"    {f}")
        print("")
        sys.exit()

    if args.config_str:
        session_config = SessionConfig(
            config_dict=file_util.parse_dict_string(args.config_str)
        )
    elif args.config:
        config_filename = os.path.join(args.config_dir, args.config)
        file_util.check_path_exists(config_filename, "Config file")
        session_config = SessionConfig(config_filename=config_filename)
    else:
        ui_util.print_error_exit(
            "\n  Must provide one of the following:\n"
            "    - YAML config file to --config\n"
            "    - String representation of config dictionary to --config_str\n"
        )

    if args.data_root:
        os.makedirs(args.data_root, exist_ok=True)
        n_demos = len(file_util.list_dir(args.data_root, ".pickle"))
        if n_demos >= args.n_demos:
            ui_util.print_warning_exit(
                f"\n  Requested {args.n_demos} demos but {n_demos} have "
                "already been collected. Exiting.\n"
            )
        elif n_demos == 0:
            ui_util.print_info(f"\nCollecting {args.n_demos} demos")
        else:
            ui_util.print_info(
                f"\nCollecting {args.n_demos - n_demos} more demos ({n_demos} already collected)"
            )

    session_config.env.sim.use_gpu = args.physx_gpu
    device = "cuda" if args.physx_gpu else "cpu"

    # Populate args onto session config
    session_config.data_root = args.data_root
    session_config.data_prefix = args.data_prefix
    session_config.n_envs = args.n_envs
    session_config.n_demos = args.n_demos
    session_config.n_steps = args.n_steps
    session_config.open_loop = args.open_loop
    session_config.randomize_robot = args.randomize_robot
    session_config.run_forever = args.run_forever
    session_config.test_reset = args.test_reset
    session_config.publish_ros = args.publish_ros
    session_config.log_only_success = args.log_only_success
    session_config.log_interval = args.log_interval
    session_config.log_only_behavior_intervals = args.log_only_behavior_intervals
    session_config.demo = args.demo
    session_config.device = device
    session_config.env.sim.device = device
    session_config.env.sim.asset_root = args.asset_root
    session_config.env.sim.headless = args.headless
    session_config.env.sim.render_graphics = args.render_graphics

    if session_config.task.task_type in VALID_TASKS:
        session = Session(session_config)
        session.run()
    else:
        ui_util.print_error_exit(f"\n  Unknown task type: {session_config.task.task_type}\n")
