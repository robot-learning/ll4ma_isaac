#!/usr/bin/env python
import os
import sys
import argparse
from tqdm import tqdm

from ll4ma_util import file_util, ui_util


def pick_place_failure(data, attrs, lift_threshold=0.01):
    """
    Defines conditions of pick-place failure. Can add whatever conditions are
    necessary to get the desired data. Currently it checks if the object was
    actually lifted upwards from its original pose.
    """
    # TODO in future this will be saved in pick_place behavior
    target_obj = attrs["behavior_params"]["pick_place"]["behaviors"]["pick"]["target_object"]

    idx = data["behavior"].tolist().index("pick_place__pick")

    start_pos = data["objects"][target_obj]["position"][0]
    picked_pos = data["objects"][target_obj]["position"][idx]

    lifted = picked_pos[-1] - start_pos[-1] > lift_threshold

    return not lifted


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-d",
        "--directory",
        type=str,
        required=True,
        help="Absolute path to directory to be cleaned",
    )
    parser.add_argument(
        "--dry_run",
        action="store_true",
        help="Show what will be modified without taking any action",
    )
    args = parser.parse_args()

    if not args.dry_run:
        proceed = ui_util.query_yes_no(
            f"\n  This script will delete some files in {args.directory} "
            "if they are failures, proceed?"
        )
        if not proceed:
            ui_util.print_info("\n  No files were modified. Exiting.\n\n")
            sys.exit()

    file_util.check_path_exists(args.directory, "Data directory")
    filenames = file_util.list_dir(args.directory, ".pickle")

    print("\nProcessing files...")
    failures = []
    for filename in tqdm(filenames):
        data, attrs = file_util.load_pickle(filename)
        if pick_place_failure(data, attrs):
            failures.append(filename)

    if args.dry_run:
        if len(failures) == 0:
            ui_util.print_happy_exit(
                "\n  There are no failures, running this script won't do anything\n"
            )
        else:
            print(f"\n  There are {len(failures)} failures:")
            for failure in sorted(failures):
                print(f"    {failure}")
            print()
            sys.exit()

    if len(failures) == 0:
        ui_util.print_happy_exit("\n  There are no failures, no action taken!\n")

    print(f"\nRemoving {len(failures)} files...")
    for failure in tqdm(failures):
        file_util.remove(failure)

    # Re-index the files
    tmp_dir = os.path.join(args.directory, "temp")
    file_util.safe_create_dir(tmp_dir)
    filenames = file_util.list_dir(args.directory, ".pickle")
    for i, filename in enumerate(filenames):
        tmp_filename = os.path.join(tmp_dir, f"demo_{i+1:06d}.pickle")
        file_util.move(filename, tmp_filename)
    for filename in file_util.list_dir(tmp_dir, ".pickle"):
        save_filename = os.path.join(args.directory, os.path.basename(filename))
        file_util.move(filename, save_filename)
    file_util.remove(tmp_dir)

    ui_util.print_happy("\nFiles processed successfully.\n")
