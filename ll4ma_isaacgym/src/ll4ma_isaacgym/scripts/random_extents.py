import subprocess as subp


total_round = 500
for i in range(total_round):
    subp.check_call(
        [
            "python",
            "run_isaacgym.py",
            "--config",
            "iiwa_push_blocks_disturbance.yaml",
            "--n_envs",
            str(8),
            "--n_demos",
            str((i + 1) * 20),
            "--data_root",
            "/media/yixuan/LL4MA-D2/isaacgym_data/test_random_extent",
            "--log_only_success",
            "--log_only_behavior_intervals",
        ]
    )
