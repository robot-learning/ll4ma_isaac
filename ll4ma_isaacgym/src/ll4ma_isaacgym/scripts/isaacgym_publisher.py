#!/usr/bin/env python
"""
This spins up an instance of Isaacgym and publishes that as the environment
state prediction. This is useful for if you want to run a motion plan without
using vision to get a state prediction. It will allow rviz to show where the
isaacgym objects are and you can align them to match. See behavior_runner_iiwa.py
for an example of a script that uses the environment state prediction to create
a plan and executes it on the real iiwa robot.
"""
from isaacgym import gymtorch, gymapi  # noqa: F401
import sys
import argparse
import rospy
import torch

from std_srvs.srv import Trigger, TriggerResponse
from std_msgs.msg import String
from sensor_msgs.msg import JointState
from visualization_msgs.msg import Marker
from reflex_msgs.msg import Command, PoseCommand

from ll4ma_util import file_util

from ll4ma_isaacgym.core import Simulator, SessionConfig
from ll4ma_isaacgym.msg import IsaacGymState


class IsaacgymPublisher:
    """docstring for IsaacgymPublisher."""

    def __init__(self, config, complete_fake=False):
        self.config = config
        self.complete_fake = complete_fake

        self.simulator = Simulator(self.config)
        self.simulator.step()
        simstate = self.simulator.get_env_state(0)

        rospy.init_node("isaacgym_publisher")

        self.state_pub = rospy.Publisher("/iiwa/state_est/full", IsaacGymState, queue_size=10)

        self.object_markers = []
        self.rviz_pubs = []
        for oi, (obj_name, obj_config) in enumerate(self.simulator.config.env.objects.items()):
            pub = rospy.Publisher("/iiwa/state_est/{}".format(obj_name), Marker, queue_size=10)
            self.rviz_pubs.append(pub)

            marker = Marker()
            marker.header.frame_id = "world"
            marker.header.stamp = rospy.Time.now()
            marker.id = oi
            marker.action = Marker.ADD
            if obj_config.object_type == "box":
                marker.type = Marker.CUBE
                marker.scale.x = obj_config.extents[0]
                marker.scale.y = obj_config.extents[1]
                marker.scale.z = obj_config.extents[2]
            elif obj_config.object_type == "urdf":
                marker.type = Marker.MESH_RESOURCE
                marker.scale.x = 1
                marker.scale.y = 1
                marker.scale.z = 1
                marker.type = 10
                marker.mesh_resource = (
                    obj_config.asset_root + "/" + obj_config.asset_filename.replace(".urdf", ".stl")
                )
                marker.mesh_use_embedded_materials = True

            if obj_config.set_color:
                marker.color.r = simstate.object_colors[obj_name].r
                marker.color.g = simstate.object_colors[obj_name].g
                marker.color.b = simstate.object_colors[obj_name].b
                marker.color.a = 0.75

            marker.pose.position.x = simstate.object_states[obj_name][0]
            marker.pose.position.y = simstate.object_states[obj_name][1]
            marker.pose.position.z = simstate.object_states[obj_name][2]
            marker.pose.orientation.x = simstate.object_states[obj_name][3]
            marker.pose.orientation.y = simstate.object_states[obj_name][4]
            marker.pose.orientation.z = simstate.object_states[obj_name][5]
            marker.pose.orientation.w = simstate.object_states[obj_name][6]

            self.object_markers.append(marker)
            pub.publish(marker)

        if self.complete_fake:
            print("complete fake")
            self.joint_state = JointState()
            # self.joint_state.name = ["iiwa_joint_{}".format(i) for i in range(1,8)]
            self.joint_state.name = self.simulator.robot.joint_names[
                : self.simulator.robot.arm.num_joints()
            ]
            self.joint_state.header.frame_id = "world"
            self.joint_pub = rospy.Publisher("/iiwa/joint_states", JointState, queue_size=10)

            self.joint_cmd = None
            rospy.Subscriber("/iiwa/joint_cmd", JointState, self.joint_cmd_cb)

            from reflex_msgs2.msg import Hand

            self.hand_state = Hand()
            self.hand_pub = rospy.Publisher("/reflex_takktile2/hand_state", Hand, queue_size=10)

            self.hand_cmd = None
            self.hand_sub = rospy.Subscriber("/reflex_takktile2/command", Command, self.hand_cmd_cb)
            self.hand_pos = None
            self.hand_pos_sub = rospy.Subscriber(
                "/reflex_takktile2/command_position", PoseCommand, self.hand_pos_cb
            )
            self.hand_action = None
            self.hand_action_sub = rospy.Subscriber(
                "/reflex_takktile2/action", String, self.hand_action_cb
            )

            self.reset_srv = rospy.Service("/isaacgym/reset_env", Trigger, self.handle_reset_env)

        self.rate = rospy.Rate(100)  # 100hz

        self.state = IsaacGymState()

        self.action = simstate.joint_position.clone()

        self.update_state()

    def spin(self):
        while not rospy.is_shutdown():
            self.update_state()
            # publish the environment state
            self.state_pub.publish(self.state)
            for pi, pub in enumerate(self.rviz_pubs):
                pub.publish(self.object_markers[pi])
            if self.complete_fake:
                print("complete fake")
                self.joint_pub.publish(self.joint_state)
                self.hand_pub.publish(self.hand_state)

            self.rate.sleep()

    def handle_reset_env(self, req):
        self.reset_env()
        return TriggerResponse()

    def reset_env(self):
        rospy.loginfo("resetting...")
        self.simulator.reset()
        self.simulator.step()
        simstate = self.simulator.get_env_state(0)
        self.action = simstate.joint_position.clone()
        self.joint_cmd = None
        self.hand_action = None
        rospy.loginfo("reset")

    def update_state(self):

        arm_joints = self.simulator.robot.arm.num_joints()
        # hand_joints = self.simulator.robot.end_effector.num_joints()
        finger_idx = self.simulator.robot.end_effector.config.grip_finger_indices

        if self.complete_fake:
            simstate = self.simulator.get_env_state(0)
            self.action[:arm_joints] = simstate.joint_position[:arm_joints]
            if self.joint_cmd is not None:
                self.action[:arm_joints] = torch.tensor(
                    list(self.joint_cmd.position[:arm_joints])
                ).unsqueeze(-1)
            self.simulator.apply_actions(self.action.unsqueeze(0))
            self.simulator.step()
            if self.hand_action is not None:
                if self.hand_action.data == "close":
                    self.action[finger_idx] = 2
                elif self.hand_action.data == "open":
                    self.action[finger_idx] = 0
                self.simulator.apply_actions(self.action.unsqueeze(0))
                for _ in range(self.simulator.robot.end_effector.config.close_for_steps):
                    self.simulator.step()
                self.hand_action = None

        # update the IsaacGymState
        self.state = self.simulator._publish_ros_data(False)

        simstate = self.simulator.get_env_state(0)
        for oi, obj_name in enumerate(self.simulator.config.env.objects):
            marker = self.object_markers[oi]
            marker.header.stamp = rospy.Time.now()
            marker.pose.position.x = simstate.object_states[obj_name][0]
            marker.pose.position.y = simstate.object_states[obj_name][1]
            marker.pose.position.z = simstate.object_states[obj_name][2]
            marker.pose.orientation.x = simstate.object_states[obj_name][3]
            marker.pose.orientation.y = simstate.object_states[obj_name][4]
            marker.pose.orientation.z = simstate.object_states[obj_name][5]
            marker.pose.orientation.w = simstate.object_states[obj_name][6]

        if self.complete_fake:
            self.joint_state.header.stamp = rospy.Time.now()
            self.joint_state.position = simstate.joint_position.squeeze()[:arm_joints].tolist()
            self.joint_state.velocity = simstate.joint_velocity.squeeze()[:arm_joints].tolist()
            self.joint_state.effort = simstate.joint_torque.squeeze()[:arm_joints].tolist()

            # Hold out your right hand palm up, with
            # pointer finger, middle finger and thumb extended
            # Pointer = finger[0], Middle = finger[1], Thumb = finger[2]
            self.hand_state.finger[0].proximal = simstate.joint_position.squeeze()[
                finger_idx[0]
            ].item()
            # if (self.hand_cmd is not None and self.hand_cmd.pose.f1 > 0.1) or
            #     (self.hand_pos is not None and self.hand_pos.f1 > 0.1):
            #     print(self.hand_state.finger[0].proximal_velocity)
            #     print(simstate.joint_velocity.squeeze())
            #     import pdb; pdb.set_trace()
            self.hand_state.finger[0].proximal_velocity = simstate.joint_velocity.squeeze()[
                finger_idx[0]
            ].item()
            self.hand_state.finger[0].distal_approx = simstate.joint_position.squeeze()[
                finger_idx[0]
            ].item()

            self.hand_state.finger[1].proximal = simstate.joint_position.squeeze()[
                finger_idx[1]
            ].item()
            self.hand_state.finger[1].proximal_velocity = simstate.joint_velocity.squeeze()[
                finger_idx[1]
            ].item()
            self.hand_state.finger[1].distal_approx = simstate.joint_position.squeeze()[
                finger_idx[1]
            ].item()

            self.hand_state.finger[2].proximal = simstate.joint_position.squeeze()[
                finger_idx[2]
            ].item()
            self.hand_state.finger[2].proximal_velocity = simstate.joint_velocity.squeeze()[
                finger_idx[2]
            ].item()
            self.hand_state.finger[2].distal_approx = simstate.joint_position.squeeze()[
                finger_idx[2]
            ].item()
            # float32 proximal
            # radians, measured from all open = 0, to pi = closed
            # float32 distal_approx
            # radians, measured from all open = 0, to roughly pi = against proximal pad,
            # relative to prox link
            # bool[14] contact
            # binary, 0 = proximal, 8 = fingertip
            # float32[14] pressure
            # scalar, dimensionless units, 0 = proximal, 8 = fingertip (can go negative)
            # Imu imu

            self.hand_state.motor[0].joint_angle = simstate.joint_position.squeeze()[
                finger_idx[0]
            ].item()
            self.hand_state.motor[0].raw_angle = simstate.joint_position.squeeze()[
                finger_idx[0]
            ].item()
            self.hand_state.motor[0].velocity = simstate.joint_velocity.squeeze()[
                finger_idx[0]
            ].item()
            self.hand_state.motor[1].joint_angle = simstate.joint_position.squeeze()[
                finger_idx[1]
            ].item()
            self.hand_state.motor[1].raw_angle = simstate.joint_position.squeeze()[
                finger_idx[1]
            ].item()
            self.hand_state.motor[1].velocity = simstate.joint_velocity.squeeze()[
                finger_idx[1]
            ].item()
            self.hand_state.motor[2].joint_angle = simstate.joint_position.squeeze()[
                finger_idx[2]
            ].item()
            self.hand_state.motor[2].raw_angle = simstate.joint_position.squeeze()[
                finger_idx[2]
            ].item()
            self.hand_state.motor[2].velocity = simstate.joint_velocity.squeeze()[
                finger_idx[2]
            ].item()
            # self.hand_state.motor =
            # float64 joint_angle
            # float64 raw_angle
            # float64 velocity
            # float64 load
            # float64 voltage
            # int32 temperature
            # string error_state

            # self.hand_state.palmImu
            # # quaternion reading from IMU (w, x, y, z)
            # float32[4] quat
            # float32[3] euler_angles
            #
            # # these are defined in reflex_hand.h driver
            # uint8 calibration_status
            # uint16[11] calibration_data

    def joint_state_cb(self, msg):
        self.joint_state = msg

    def joint_cmd_cb(self, msg):
        self.joint_cmd = msg

    def hand_cmd_cb(self, msg):
        self.hand_cmd = msg
        self.hand_pos = None

    def hand_pos_cb(self, msg):
        self.hand_pos = msg
        self.hand_cmd = None

    def hand_action_cb(self, msg):
        self.hand_action = msg

    def wait_for_joint_state(self, timeout=30):
        if self.joint_state is None:
            rospy.loginfo("Waiting for joint state...")
            start = rospy.get_time()
            while (
                not rospy.is_shutdown()
                and rospy.get_time() - start < timeout
                and self.joint_state is None
            ):
                self.rate.sleep()
            if self.joint_state is not None:
                rospy.loginfo("Joint state received")
            else:
                rospy.logerr("Joint state unknown")
                return False
        return True


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c",
        "--config",
        type=str,
        default="config/iiwa_2block.yaml",
        help="Filename of YAML config for simulation (relative to current directory)",
    )
    parser.add_argument("--fake", action="store_true")
    parser.add_argument("--device", type=str, default="cpu")
    parser.set_defaults(fake=False)
    args = parser.parse_args(rospy.myargv(argv=sys.argv)[1:])

    file_util.check_path_exists(args.config, "Config file")
    session_config = SessionConfig(config_filename=args.config)
    session_config.device = args.device
    session_config.n_envs = 1
    publisher = IsaacgymPublisher(session_config, complete_fake=args.fake)

    publisher.spin()
