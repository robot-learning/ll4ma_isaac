#!/usr/bin/env python
import os
import pickle
import argparse
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm

from ll4ma_util import file_util


def print_data(data, indent=""):
    if not isinstance(data, dict):
        raise ValueError("Something is wrong, was expecting dictionary as input")
    for k, v in data.items():
        if isinstance(v, np.ndarray):
            print(indent + k, v.shape)
        elif isinstance(v, dict):
            print(indent + k)
            print_data(v, indent + "    ")
        elif isinstance(v, list):
            print(indent + k, len(v))
        else:
            raise ValueError(f"Unknown datatype for key '{k}': {type(v)}")


def print_attrs(attrs, indent=""):
    if not isinstance(attrs, dict):
        raise ValueError("Something is wrong, was expecting dictionary as input")
    for k, v in attrs.items():
        if isinstance(v, dict):
            print(indent + k)
            print_attrs(v, indent + "    ")
        else:
            print(indent + k, v)


def show_rgb_video(data, pause=0.01):
    if "rgb" not in data:
        raise ValueError("The key 'rgb' should be in the data, something is wrong")

    fig, ax = plt.subplots(figsize=(10, 10))
    plt.axis("off")
    plt.tight_layout()
    img = None
    rgb = data["rgb"]
    for i in range(len(rgb)):
        if img is None:
            img = plt.imshow(rgb[i])
        else:
            img.set_data(rgb[i])
        if "behavior" in data:
            plt.title(data["behavior"][i])
        plt.pause(pause)
        plt.draw()


def save_rgb_images(data):
    if "rgb" not in data:
        raise ValueError("The key 'rgb' should be in the data, something is wrong")

    save_dir = "/tmp/rgb_images"
    file_util.safe_create_dir(save_dir)
    for i, img in tqdm(enumerate(data["rgb"])):
        plt.imsave(os.path.join(save_dir, f"image_{i+1:04d}.png"), img)
    print(f"\n  Images saved here: {save_dir}\n")


if __name__ == "__main__":
    """
    Simple script to display the contents of a pickle file containing data recorded
    from Isaac Gym. Will print out the shape of recorded data, attributes saved,
    and display an RGB video of the recorded data (note you can disable RGB display
    using --no_show_rgb).
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--filename", type=str, required=True)
    parser.add_argument("-r", "--rgb_pause", type=float, default=0.01)
    parser.add_argument("--show_rgb", action="store_true")
    parser.add_argument("--no_show_rgb", dest="show_rgb", action="store_false")
    parser.add_argument("--save_rgb", action="store_true")
    parser.set_defaults(show_rgb=True)
    args = parser.parse_args()

    if not os.path.exists(args.filename):
        print(f"Pickle file does not exist: {args.filename}")

    with open(args.filename, "rb") as f:
        data, attrs = pickle.load(f)

    print("=================== DATA ===================")
    print_data(data)

    print("=================== ATTR ===================")
    print_attrs(attrs)

    if args.show_rgb:
        show_rgb_video(data, args.rgb_pause)

    if args.save_rgb:
        save_rgb_images(data)
