import rospy
from std_srvs.srv import Trigger, TriggerRequest

from ll4ma_util import ros_util


if __name__ == "__main__":
    rospy.loginfo("Resetting environment...")
    ros_util.call_service(TriggerRequest(), "/isaacgym/reset_env", Trigger)
    rospy.loginfo("Environment reset")
