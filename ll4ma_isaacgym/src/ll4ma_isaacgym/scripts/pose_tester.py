from isaacgym import gymtorch, gymapi  # noqa: F401
import sys
import argparse
import rospy

from ll4ma_util import file_util

from ll4ma_isaacgym.core import SessionConfig
from ll4ma_isaacgym.scripts.behavior_runner_iiwa import BehaviorRunner


def main():
    rospy.init_node("run_iiwa_behaviors", anonymous=True)

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c",
        "--config",
        type=str,
        default="config/iiwa_pick_place_cleaner.yaml",
        help="Filename of YAML config for simulation (relative to current directory)",
    )
    parser.add_argument("--device", type=str, default="cpu")
    parser.add_argument("--rate", type=float, default=100.0)
    parser.add_argument("--unfake", action="store_false")
    parser.set_defaults(fake=True)
    args = parser.parse_args(rospy.myargv(argv=sys.argv)[1:])

    file_util.check_path_exists(args.config, "Config file")
    session_config = SessionConfig(config_filename=args.config)
    session_config.device = args.device

    runner = BehaviorRunner(session_config, args.rate, True, args.fake)

    tot_tests = 200
    completes = 0
    fails = 0
    for testi in range(tot_tests):
        runner.reset_env()

        runner.reset(False)

        try:
            runner.run_behavior(preview=False)
            completes += 1
        except Exception as e:
            import traceback

            traceback.print_exc()
            print(e)
            fails += 1

        rospy.loginfo("Behavior execution complete.")
    print("completed:", completes)
    print("fails:", fails)
    print("totals:", tot_tests)


if __name__ == "__main__":
    main()
