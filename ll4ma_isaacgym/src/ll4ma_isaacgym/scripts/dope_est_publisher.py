#!/usr/bin/env python
"""
This spins up an instance of Isaacgym and publishes that as the environment
state prediction. This is useful for if you want to run a motion plan without
using vision to get a state prediction. It will allow rviz to show where the
isaacgym objects are and you can align them to match. See behavior_runner_iiwa.py
for an example of a script that uses the environment state prediction to create
a plan and executes it on the real iiwa robot.
"""
from isaacgym import gymtorch, gymapi  # noqa: F401
import sys
import argparse
import rospy

from std_srvs.srv import TriggerResponse
from visualization_msgs.msg import Marker
from geometry_msgs.msg import PoseStamped, Pose

from ll4ma_util import file_util  # , ros_util

from ll4ma_isaacgym.core import Simulator, SessionConfig
from ll4ma_isaacgym.msg import IsaacGymState
from ll4ma_isaacgym.srv import EstState, EstStateResponse


class DopEstPublisher:
    """docstring for DopEstPublisher."""

    def __init__(self, config, complete_fake=False):
        self.config = config
        self.complete_fake = complete_fake

        self.simulator = Simulator(self.config)
        self.simulator.step()
        simstate = self.simulator.get_env_state(0)
        isaac_state = self.simulator._publish_ros_data(False)

        rospy.init_node("dopest_publisher")

        self.state_pub = rospy.Publisher("/iiwa/state_est/full", IsaacGymState, queue_size=10)

        self.srv_state = EstStateResponse()
        for oi in range(len(isaac_state.object_names)):
            self.srv_state.object_names.append(isaac_state.object_names[oi])
            self.srv_state.object_paths.append(isaac_state.object_paths[oi])
            if isaac_state.object_names[oi].data == "table":
                obj_name = isaac_state.object_names[oi].data
                pose = Pose()
                pose.position.x = simstate.object_states[obj_name][0]
                pose.position.y = simstate.object_states[obj_name][1]
                pose.position.z = simstate.object_states[obj_name][2]
                pose.orientation.x = simstate.object_states[obj_name][3]
                pose.orientation.y = simstate.object_states[obj_name][4]
                pose.orientation.z = simstate.object_states[obj_name][5]
                pose.orientation.w = simstate.object_states[obj_name][6]
                self.srv_state.object_poses.append(pose)
            else:
                self.srv_state.object_poses.append(None)

        self.object_markers = []
        self.rviz_pubs = []
        self.dope_subs = []

        def dope_pose_cb(msg_var):
            def pose_cb(msg):
                return self.pose_cb(msg, msg_var)

            return pose_cb

        objidx = 0
        for oi, (obj_name, obj_config) in enumerate(self.simulator.config.env.objects.items()):
            pub = rospy.Publisher("/iiwa/state_est/{}".format(obj_name), Marker, queue_size=10)
            self.rviz_pubs.append(pub)

            marker = Marker()
            marker.header.frame_id = "world"
            marker.header.stamp = rospy.Time.now()
            marker.id = oi
            marker.action = Marker.ADD
            if obj_config.object_type == "box":
                marker.type = Marker.CUBE
                marker.scale.x = obj_config.extents[0]
                marker.scale.y = obj_config.extents[1]
                marker.scale.z = obj_config.extents[2]
            elif obj_config.object_type == "urdf":
                marker.type = Marker.MESH_RESOURCE
                marker.scale.x = 1
                marker.scale.y = 1
                marker.scale.z = 1
                marker.type = 10
                marker.mesh_resource = (
                    obj_config.asset_root + "/" + obj_config.asset_filename.replace(".urdf", ".stl")
                )
                marker.mesh_use_embedded_materials = True

            if obj_config.set_color:
                marker.color.r = simstate.object_colors[obj_name].r
                marker.color.g = simstate.object_colors[obj_name].g
                marker.color.b = simstate.object_colors[obj_name].b
                marker.color.a = 0.75

            marker.pose.position.x = simstate.object_states[obj_name][0]
            marker.pose.position.y = simstate.object_states[obj_name][1]
            marker.pose.position.z = simstate.object_states[obj_name][2]
            marker.pose.orientation.x = simstate.object_states[obj_name][3]
            marker.pose.orientation.y = simstate.object_states[obj_name][4]
            marker.pose.orientation.z = simstate.object_states[obj_name][5]
            marker.pose.orientation.w = simstate.object_states[obj_name][6]

            self.object_markers.append(marker)
            pub.publish(marker)

            if obj_name == "table":
                continue
            elif obj_name == "cleaner":
                obj_name = "bleach"
            sub = rospy.Subscriber("/dope/pose_{}".format(obj_name), PoseStamped, dope_pose_cb(oi))
            objidx += 1
            self.dope_subs.append(sub)

        self.envstate_srv = rospy.Service("/iiwa/get_state", EstState, self.get_envstate)

        self.rate = rospy.Rate(100)  # 100hz

        self.state = IsaacGymState()

        self.action = simstate.joint_position.clone()

        self.update_state()

    def spin(self):
        while not rospy.is_shutdown():
            self.update_state()
            # publish the environment state
            self.state_pub.publish(self.state)
            for pi, pub in enumerate(self.rviz_pubs):
                pub.publish(self.object_markers[pi])
            self.rate.sleep()

    def handle_reset_env(self, req):
        self.reset_env()
        return TriggerResponse()

    def reset_env(self):
        rospy.loginfo("resetting...")
        self.simulator.reset()
        self.simulator.step()
        simstate = self.simulator.get_env_state(0)
        self.action = simstate.joint_position.clone()
        self.joint_cmd = None
        self.hand_action = None
        rospy.loginfo("reset")

    def get_envstate(self, req):
        self.update_state()
        print(self.srv_state.object_poses)
        return self.srv_state

    def update_state(self):
        if self.wait_for_state():
            pose = self.srv_state.object_poses[-1]
            # ref_T_dope = ros_util.pose_to_homogeneous(pose)
            # # Adam figured this out for bleach in Meshlab and rviz looking at the relative frames
            # dope_T_obj = np.array([[-1,  0,  0,  0],
            #                        [ 0,  0, -1,  0],
            #                        [ 0, -1,  0,  0],
            #                        [ 0,  0,  0,  1]])
            # ref_T_obj = ref_T_dope @ dope_T_obj
            # pose = ros_util.homogeneous_to_pose(ref_T_obj)
            self.srv_state.object_poses[-1] = pose
            for oi, obj_name in enumerate(self.simulator.config.env.objects):
                self.object_markers[oi].pose.position.x = self.srv_state.object_poses[oi].position.x
                self.object_markers[oi].pose.position.y = self.srv_state.object_poses[oi].position.y
                self.object_markers[oi].pose.position.z = self.srv_state.object_poses[oi].position.z
                self.object_markers[oi].pose.orientation.x = self.srv_state.object_poses[
                    oi
                ].orientation.x
                self.object_markers[oi].pose.orientation.y = self.srv_state.object_poses[
                    oi
                ].orientation.y
                self.object_markers[oi].pose.orientation.z = self.srv_state.object_poses[
                    oi
                ].orientation.z
                self.object_markers[oi].pose.orientation.w = self.srv_state.object_poses[
                    oi
                ].orientation.w

    def unfilled(self):
        return any([pose is None for pose in self.srv_state.object_poses])

    def wait_for_state(self, timeout=30):
        if self.unfilled():
            rospy.loginfo("Waiting for state...")
            start = rospy.get_time()
            while (
                not rospy.is_shutdown() and rospy.get_time() - start < timeout and self.unfilled()
            ):
                self.rate.sleep()
            if not self.unfilled():
                rospy.loginfo("State received")
            else:
                rospy.logerr("State unknown")
                return False
        return True

    def pose_cb(self, msg, pose_idx):
        self.srv_state.object_poses[pose_idx] = msg.pose


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c",
        "--config",
        type=str,
        default="config/iiwa_pick_place_cleaner.yaml",
        help="Filename of YAML config for simulation (relative to current directory)",
    )
    parser.add_argument("--fake", action="store_true")
    parser.add_argument("--device", type=str, default="cpu")
    parser.set_defaults(fake=False)
    args = parser.parse_args(rospy.myargv(argv=sys.argv)[1:])

    file_util.check_path_exists(args.config, "Config file")
    session_config = SessionConfig(config_filename=args.config)
    session_config.device = args.device
    session_config.n_envs = 1
    session_config.env.sim.headless = True
    publisher = DopEstPublisher(session_config, complete_fake=args.fake)

    publisher.spin()
