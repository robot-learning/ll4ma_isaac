#!/usr/bin/env python
import os
import argparse

from ll4ma_util import file_util, ui_util


if __name__ == "__main__":
    """
    This script re-indexes all demo files in one directory to be in
    sequential order. Makes assumption they're all files with extension
    '.pickle' and that you want to rename like demo_XXXX.pickle where
    the XXXX is a zero-padded unique index for the files in that dir.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--directory", type=str)
    args = parser.parse_args()

    file_util.check_path_exists(args.directory, "Data directory")

    filenames = file_util.list_dir(args.directory, ".pickle")

    tmp_dir = os.path.join(args.directory, "temp")
    file_util.safe_create_dir(tmp_dir)

    print("\nRe-indexing files...")
    for i, filename in enumerate(filenames):
        tmp_filename = os.path.join(tmp_dir, f"demo_{i+1:06d}.pickle")
        file_util.move(filename, tmp_filename)

    for filename in file_util.list_dir(tmp_dir, ".pickle"):
        save_filename = os.path.join(args.directory, os.path.basename(filename))
        file_util.move(filename, save_filename)

    file_util.remove(tmp_dir)

    ui_util.print_happy("\nFiles re-indexed successfully\n")
