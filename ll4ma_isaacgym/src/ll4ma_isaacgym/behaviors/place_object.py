import rospy
import numpy as np

from ll4ma_isaacgym.core import MoveToPoseConfig, OpenFingersConfig
from ll4ma_isaacgym.behaviors import Behavior
from ll4ma_isaacgym.candidate_generators import PlaceCandidateGenerator

import torch


class PlaceObject(Behavior):
    """
    Behavior to place an object.

    This behavior is hierarchical and has the steps of:
        1. Move through free space (with object already in hand) to a pose hovering
           slightly above the placement location
        2. Move (with object still in hand) a small straight path down to placement
           pose (using Cartesian path)
        3. Open the fingers to release the object

    It's assumed (without check) that the object is already grasped in the hand before
    this behavior executes.

    TODO want a more gentle finger opening behavior to release object, currently it's
    very abrupt and if object is light you can knock the object away if it gets caught
    on finger
    """

    def __init__(
        self,
        behavior_config,
        robot,
        env_config,
        sim,
        device="cuda",
        open_loop=False,
        rospy_log=True,
        name="",
    ):
        super().__init__(
            behavior_config, robot, env_config, sim, device, open_loop, rospy_log, name
        )

        self.init_obj_pose = None
        self.target_obj_pose = None

        self.candidate_generator = PlaceCandidateGenerator()
        self.selected_candidate = None

        behavior_configs = []
        if self.config.use_approach:
            behavior_configs.append(
                MoveToPoseConfig(
                    config_dict={
                        "name": "move_to_above",
                        "disable_collisions": [self.config.target_object],
                        "attach_objects": [self.config.target_object],
                        "end_effector_frame": robot.end_effector.get_link(),
                        "max_plan_attempts": self.config.max_plan_attempts,
                        "max_vel_factor": self.config.max_vel_factor,
                        "max_acc_factor": self.config.max_acc_factor,
                        "planning_time": self.config.planning_time,
                        "ignore_error": self.config.ignore_error,
                        "planner": self.config.planner,
                    }
                )
            )
        behavior_configs.append(
            MoveToPoseConfig(
                config_dict={
                    "name": "move_to_place",
                    "disable_collisions": [self.config.target_object],
                    "attach_objects": [self.config.target_object],
                    "end_effector_frame": robot.end_effector.get_link(),
                    "max_plan_attempts": self.config.max_plan_attempts,
                    "max_vel_factor": self.config.max_vel_factor,
                    "max_acc_factor": self.config.max_acc_factor,
                    "planning_time": self.config.planning_time,
                    "ignore_error": self.config.ignore_error,
                    "planner": self.config.planner,
                    # Only cartesian if doing straight-line approach:
                    "cartesian_path": self.config.use_approach,
                    "cartesian_planner": self.config.cartesian_planner,
                    "min_cartesian_pct": self.config.min_cartesian_pct,
                }
            )
        )
        if self.config.release:
            behavior_configs.append(
                OpenFingersConfig(config_dict={"name": "release", "behavior_type": "OpenFingers"})
            )

        self.init_behaviors(behavior_configs, robot, env_config, sim, device, open_loop, rospy_log)

    def set_place_pose(self, pose):
        """
        Sets the target pose for placing the object.

        Args:
            pose (list-like): Pose as a 7D vector (3D position and quaternion)
        """
        self.set_place_position(pose[:3])
        self.set_place_orientation(pose[3:])

    def set_place_position(self, pos):
        """
        Sets the target position for placing the object.

        Args:
            pos (list-like): Position as 3D vector
        """
        self.config.place_position = pos

    def set_place_orientation(self, quat):
        """
        Sets the target orientation for placing the object.

        Args:
            quat (list-like): Orientation as a quaternion (4D vector)
        """
        self.config.place_orientation = quat

    def get_behavior_params(self):
        if self.selected_candidate:
            bps = {}
            bps["target_object"] = self.config.target_object
            bps["init_object_pose"] = self.selected_candidate.init_obj_pose
            bps["target_object_pose"] = self.selected_candidate.target_obj_pose
            bps["above_ee_pose"] = self.selected_candidate.above_ee_pose
            bps["above_joints"] = self.selected_candidate.above_joints
            bps["place_ee_pose"] = self.selected_candidate.place_ee_pose
            bps["place_joints"] = self.selected_candidate.place_joints
            self._behavior_params = bps
        return super().get_behavior_params()

    def set_policy(self, state):
        target_obj = self.config.target_object
        self.init_obj_pose = state.object_states[target_obj].clone().cpu().numpy()[:7]

        target = self.init_obj_pose.copy()
        # target[:3] = self.config.place_position
        # target[0] += 0.05
        # target[1] -= 0.2
        # target[2] = self.config.place_position[2]

        if state.goal is not None:
            target = state.goal.clone().cpu().numpy()[:7]
        else:
            # TODO need to also account for if orientation randomization was enabled
            if self.config.place_position_ranges is None:
                # Only candidate will be the exact target set in config
                target = self.init_obj_pose.copy()
                target[:3] = self.config.place_position
                # target[1] -= 0.1
                # target[2] = self.config.place_position[2]
                if self.config.place_orientation is not None:
                    # Will keep init obj orientation if not specified
                    target[3:] = self.config.place_orientation
                else:
                    target = target[:3]
            else:
                target = None

        candidates = self.candidate_generator.get_candidates(
            self.init_obj_pose,
            state.ee_state.clone().cpu().numpy()[:7],  # ee_pose
            self.config,
            self.robot.end_effector.get_link(),
            target,
        )

        self._open_loop = True  # Force OL so we jointly plan movements to above and place

        # Try all candidates until we either find successful one or fail on all of them
        for i, candidate in enumerate(candidates):
            if candidate is None:
                continue  # Can happen for single candidate when filtering unreachable
            rospy.loginfo(f"Trying place candidate {i+1} of {len(candidates)}")
            self.set_above_ee_pose(np.array(candidate.above_ee_pose))
            self.set_place_ee_pose(np.array(candidate.place_ee_pose))
            if super().set_policy(state):
                self.selected_candidate = candidate
                self.target_obj_pose = candidate.target_obj_pose  # Needed for override_state
                return True  # Plans are set now on both sub-behaviors
            else:
                self.set_not_started()  # Reset since sub-behaviors may have reported fail
        self.set_failure()  # If we reached here, all candidates failed
        if len([candidate for candidate in candidates if candidate is not None]) == 0:
            rospy.logerr("No placement candidates!!!")
        else:
            rospy.logwarn("Placement candidates failed")
        import pdb

        pdb.set_trace()
        return False

    def override_state(self, state):
        retval = super().override_state(state)
        state.object_states[self.config.target_object][:7] = torch.tensor(self.target_obj_pose)
        return retval

    def set_above_ee_pose(self, pose):
        if "move_to_above" in self.behaviors:
            self.behaviors["move_to_above"].set_target_pose(pose)

    def set_place_ee_pose(self, pose):
        self.behaviors["move_to_place"].set_target_pose(pose)

    def adapt_action_for_objects(self):
        # This will ensure if we adapted the fingers in the close-fingers phase of
        # grasping the object that it still obeys that configuration while placing
        if (
            self.config.adapt_to_contact
            and self._current_behavior
            and self._current_behavior.name != "release"
        ):
            return [self.config.target_object]
        else:
            return []
