from ll4ma_opt.solvers.tensor_gausnewton import GaussNewtonMethodTensorized  # , ScipySolver
from ll4ma_opt.solvers import GaussNewtonMethod  # , PenaltyMethod, AugmentedLagranMethod
from ll4ma_opt.problems import IiwaKDLTraj
from ll4ma_opt.problems import IiwaIK

from ll4ma_util import math_util
# import moveit_interface.util as moveit_util

import torch


class IKOptimizer:
    """docstring for IKOptimizer."""

    def __init__(
        self, alpha=1, rho=0.5, FP_PRECISION=1e-5, num_joints=7, max_iterations=100, verbose=False
    ):
        self.alpha = alpha
        self.rho = rho
        self.FP_PRECISION = FP_PRECISION
        self.num_joints = num_joints
        self.max_iterations = max_iterations
        self.verbose = verbose

        self.problem = IiwaIK(
            desired_pose=None,
            error_type="pose",
            use_collision=False,
            collision_env=(
                1,
                0.1,
                (torch.tensor([0.3, 0.0, 0.99]), torch.tensor([0.3, 0.0, 0.99])),
            ),
            soft_limits=False,
        )
        # self.solver = PenaltyMethod(
        #     self.problem, 'gauss newton', {'alpha': self.alpha, 'rho': self.rho},
        #     FP_PRECISION=self.FP_PRECISION
        # )
        self.solver = GaussNewtonMethodTensorized(
            self.problem, self.alpha, self.rho, FP_PRECISION=self.FP_PRECISION
        )

    def get_ik(self, goal_ee):
        if goal_ee.shape[0] == 7:
            goal_ee = math_util.pose_to_homogeneous(goal_ee[:3], goal_ee[3:7])
        self.problem.set_desired_pose(goal_ee)
        initial_solution = self.problem.initial_solution
        result = self.solver.optimize(initial_solution, self.max_iterations)
        if self.verbose:
            print("time:", result.time)
            print("converged:", result.converged)
            print("iterates:", len(result.iterates))
            print("final cost:", result.cost)
        return torch.tensor(result.iterates[-1].squeeze())


class TrajectoryOptimizer:
    """docstring for TrajectoryOptimizer."""

    def __init__(
        self,
        alpha=1,
        rho=0.5,
        FP_PRECISION=1e-7,
        timesteps=10,
        num_joints=7,
        max_iterations=500,
        verbose=False,
        ee_link="reflex_palm_link",
    ):
        self.FP_PRECISION = FP_PRECISION
        self.timesteps = timesteps
        self.num_joints = num_joints
        self.min_iterations = 30
        self.max_iterations = max(self.min_iterations, max_iterations)
        self.verbose = verbose

        self.problem = IiwaKDLTraj(
            desired_pose=None,
            timesteps=self.timesteps,
            error_type="pose",
            use_collision=False,
            collision_env=(
                1,
                0.1,
                (torch.tensor([0.3, 0.0, 0.99]), torch.tensor([0.3, 0.0, 0.99])),
            ),
            soft_limits=False,
        )
        self.problem.error_type = "position"
        # self.solver = PenaltyMethod(
        #     problem, 'gauss newton', {'alpha': alpha, 'rho': rho},
        #     FP_PRECISION=self.FP_PRECISION
        # )

        self.solver = GaussNewtonMethodTensorized(
            self.problem, alpha, rho, FP_PRECISION=self.FP_PRECISION
        )
        # self.solver = ScipySolver(
        #     self.problem, alpha, rho, FP_PRECISION=self.FP_PRECISION
        # )

        self._idx = 0
        self._max_idx = 1000
        self._start_states = torch.zeros((self._max_idx, self.num_joints + 7), dtype=torch.float)
        self._trajectories = torch.zeros((self._max_idx, self.timesteps, 7), dtype=torch.float)
        self._infos = []
        self.init_noise = 0.01
        self.cost_threshold = 1.0

        self.use_gpu = False
        self.ee_link = ee_link

    def get_trajectory(
        self, start_joints, goal_ee, obj_state=None, interp=1, repeats=1, goal_ee_type="pose"
    ):
        repeats = max(repeats, 1)
        start_joints = start_joints  # .cpu()
        goal_ee = goal_ee  # .cpu()
        start_state = torch.cat((start_joints, goal_ee))
        mindist = None
        if self._idx > 0:
            self._start_states = self._start_states.to(start_state.device)
            dists = (self._start_states[: self._idx] - start_state.unsqueeze(0)).abs().sum(dim=-1)
            mindist = dists.argmin()
            # if self.verbose:
            #     print('\nmindist:', dists[mindist])
        best_traj = None
        best_info = None
        lowest_cost = float("inf")
        if self.use_gpu:
            self.solver.set_device(start_joints.device)
        for ri in range(repeats):
            trajectory, info = self._get_trajectory(
                start_joints,
                goal_ee,
                start_state,
                self.max_iterations,
                mindist,
                goal_ee_type=goal_ee_type,
            )
            if info["cost"] < lowest_cost:
                best_traj = trajectory
                best_info = info
                lowest_cost = info["cost"]
            if info["cost"] < self.cost_threshold:
                break
            if info["converged"] and info["iterates"] > self.min_iterations:
                break
        trajectory = best_traj
        info = best_info
        if self._idx < self._max_idx and (self._idx == 0 or dists[mindist] > 1.0):
            self._start_states[self._idx] = start_state
            self._trajectories[self._idx] = trajectory
            self._infos.append(info)
            self._idx += 1
        if interp > 1:
            trajectory = joint_interpolation(trajectory, interp)
        return trajectory, info

    def _get_trajectory(
        self, start_joints, goal_ee, start_state, max_iterations, mindist, goal_ee_type="pose"
    ):
        self.problem.set_start_joints(start_joints)
        if self.problem.error_type != goal_ee_type:
            self.problem.error_type = goal_ee_type
        if self.problem.error_type == "pose" and goal_ee.shape[0] == 7:
            goal_ee = math_util.pose_to_homogeneous(goal_ee[:3], goal_ee[3:7])
        # elif self.problem.error_type == 'position':
        #     ik_resp, success = moveit_util.get_ik(
        #         [goal_ee.cpu().numpy()], self.ee_link, 5
        #     )
        #     goal_ee = torch.tensor(
        #         ik_resp.solutions[0].position,
        #         dtype=start_joints.dtype,
        #         device=start_joints.device
        #     )
        self.problem.set_desired_pose(goal_ee)
        initial_solution = self.problem.initial_solution
        # initial_solution = torch.zeros((self.timesteps, self.num_joints))
        initial_solution = (
            torch.randn((self.timesteps, 7), device=start_joints.device) * self.init_noise
        )
        initial_solution[0, :] = start_joints
        if self._idx > 0:
            # initial_solution[1:,:] = self._trajectories[mindist,1:,:]
            initial_solution[-1, :] = self._trajectories[mindist, -1, :]
        # else:
        #     initial_solution = torch.zeros((self.timesteps, self.num_joints))
        #     initial_solution += start_joints.unsqueeze(0)
        # initial_solution[1:-1,:] += torch.randn((self.timesteps-2, 7))*self.init_noise
        initial_solution = initial_solution.reshape(*self.problem.initial_solution.shape)
        if not self.use_gpu:
            initial_solution = initial_solution.cpu()
        if isinstance(self.solver, GaussNewtonMethod):
            initial_solution = initial_solution.numpy()
        result = self.solver.optimize(initial_solution, max_iterations)
        if self.verbose:
            print("time:", result.time)
            print("converged:", result.converged)
            print("iterates:", len(result.iterates))
            print("final cost:", result.cost)
        trajectory = result.iterates[-1].squeeze().reshape(self.timesteps, self.num_joints)
        if not torch.is_tensor(trajectory):
            trajectory = torch.tensor(trajectory, device=start_joints.device)
        info = {
            "time": result.time,
            "converged": result.converged,
            "cost": result.cost,
            "iterates": len(result.iterates),
            "iterations": result.iterates,
            "hesstime": self.solver._hessTime,
        }
        return trajectory, info

    def error_type():
        doc = "The error_type property."

        def fget(self):
            return self.problem.error_type

        def fset(self, error_type):
            self.problem.error_type = error_type

        return locals()

    error_type = property(**error_type())

    def rho():
        doc = "The rho property."

        def fget(self):
            return self.solver.rho

        def fset(self, value):
            self.solver.rho = value

        return locals()

    rho = property(**rho())

    def alpha():
        doc = "The alpha property."

        def fget(self):
            return self.solver.alpha

        def fset(self, value):
            self.solver.alpha = value

        return locals()

    alpha = property(**alpha())


def joint_interpolation(joints, extras=2):
    interps = joints.repeat_interleave(extras, dim=0)
    diff = (joints[1:] - joints[:-1]) / extras
    for ei in range(1, extras):
        interps[ei:-extras:extras, :] += diff * ei
    return interps
