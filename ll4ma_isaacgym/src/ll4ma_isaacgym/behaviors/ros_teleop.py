import numpy as np
import rospy
from sensor_msgs.msg import JointState

from ll4ma_isaacgym.behaviors import Behavior


class ROSTeleop(Behavior):
    """
    Teleop behavior that directly takes command from a ROS topic.
    """

    def __init__(self, behavior_config, robot, *args):
        super().__init__(behavior_config, robot, *args)

        self.joint_cmd = None
        rospy.Subscriber(behavior_config.joint_cmd_topic, JointState, self.joint_cmd_cb)

    def get_action(self, state):
        action = state.prev_action
        # Update action if we've received a command from an external teleop node
        if self.joint_cmd is not None:
            # TODO this is not very robust, should do by joint name
            joint_pos = np.array(self.joint_cmd.position)[: state.n_arm_joints]
            action.set_arm_joint_position(joint_pos)
        return action

    def joint_cmd_cb(self, msg):
        self.joint_cmd = msg
