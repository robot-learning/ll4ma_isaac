import numpy as np
import torch
import rospy
import random

from ll4ma_isaacgym.core.config import PLANNERS
from ll4ma_isaacgym.behaviors import Behavior, behavior_util
from ll4ma_util import ros_util, torch_util, math_util
import moveit_interface.util as moveit_util
from geometry_msgs.msg import PoseStamped


class MoveToPose(Behavior):
    """
    Behavior to move robot end-effector to a desired pose.

    A motion-planned trajectory (joint space or Cartesian) is generated that will
    move the end-effector from its current pose to the desired pose.

    This supports setting a single target pose as well as multiple candidate target
    poses so that if a plan cannot be generated for a particular candidate, it will
    continue to randomly sample candidates until a plan for one of the is found or
    no plan can be found for any of the candidates.
    """

    TRAJ_OPT = None

    def __init__(
        self,
        behavior_config,
        robot,
        env_config,
        sim,
        device="cuda",
        open_loop=False,
        rospy_log=True,
        name="",
    ):
        super().__init__(
            behavior_config, robot, env_config, sim, device, open_loop, rospy_log, name
        )

        rospy.wait_for_service(moveit_util.GET_PLAN_SRV, timeout=30)

        self._plan = None
        self._trajectory = None
        self._target_pose_candidates = []
        if self.config.pose is not None:
            self.set_target_pose(self.config.pose)
        else:
            self.target_pose = None
        self.offset_from_current = None  # Saved on self to be logged in behavior params

        if self.rospy_log:
            self.beh_pub = rospy.Publisher(
                "/behaviors/target_pose/" + self.name, PoseStamped, queue_size=10
            )
            self.beh_msg = PoseStamped()
            self.beh_idx = 0

    def get_action(self, state):
        """
        Returns the next action from the motion-planned trajectory.

        Args:
            state (EnvironmentState): Current state from simulator
        Returns:
            action (Action): Action to be applied next in the simulator

        TODO populate joint velocity and set on action. The action interface currently
        only supports position commands so that's all we're commanding here.
        """
        if self.is_complete():
            return None
        if self.is_not_started() and self._plan is None:
            if self.rospy_log:
                rospy.loginfo(f"Running behavior: {self.name.upper()}")
            self.set_policy(state)

        action = state.prev_action
        if self._plan is not None:
            if len(self._trajectory.points) > 0:
                # action.set_arm_joint_position(np.array())
                action[: self.sim.robot.arm.num_joints()] = torch.tensor(
                    self._trajectory.points.pop(0).positions, device=action.device
                )
            if len(self._trajectory.points) == 0:
                self.set_success()
                if not self.config.ignore_error:
                    target_pos = self.target_pose[:3].unsqueeze(0)
                    actual_pos = state.ee_state[:3].unsqueeze(0)
                    target_rot = self.target_pose[3:].unsqueeze(0)
                    actual_rot = state.ee_state[3:7].unsqueeze(0)
                    pos_error = torch_util.position_error(
                        target_pos, actual_pos, True, flatten=True
                    )
                    rot_error = torch_util.quaternion_error(
                        target_rot, actual_rot, True, flatten=True
                    )
                    if torch.any(pos_error > 1e-3) or torch.any(rot_error > 1e-3):
                        self.set_failure()
        return action

    def set_target_pose(self, pose):
        """
        Sets target end-effector pose that motion planner will generate plan for.

        Args:
            pose (list-like): Target pose as 7D vector (3D position and quaternion)

        Input pose can be a list, numpy array, or torch tensor.
        """
        if isinstance(pose, list):
            self.target_pose = torch.tensor(pose)
        elif isinstance(pose, np.ndarray):
            self.target_pose = torch.tensor(pose)
        elif torch.is_tensor(pose):
            self.target_pose = pose
        else:
            raise ValueError(f"Unknown data type for setting target pose: {type(pose)}")

        if len(self.target_pose) != 7:
            raise ValueError(f"Expected target pose to be length 7 but got {len(self.target_pose)}")

        if self.rospy_log:
            # pose = PoseStamped()
            self.beh_msg.header.frame_id = "world"
            pose = self.beh_msg.pose
            pose.position.x = self.target_pose[0]
            pose.position.y = self.target_pose[1]
            pose.position.z = self.target_pose[2]
            pose.orientation.x = self.target_pose[3]
            pose.orientation.y = self.target_pose[4]
            pose.orientation.z = self.target_pose[5]
            pose.orientation.w = self.target_pose[6]
            self.beh_idx += 1
            self.beh_pub.publish(self.beh_msg)

        self.target_pose = self.target_pose.to(self.device)

        self.filter_unreachable = True
        if self.filter_unreachable and not rospy.is_shutdown():
            # Don't try to plan to kinematically unreachable poses
            ik_resp, success = moveit_util.get_ik(
                [self.target_pose.cpu().numpy()], self.robot.end_effector.get_link(), 5
            )
            if not success or ik_resp is None:
                rospy.logerr("Failed to call GetIK service, check if it's running")
            elif not all(ik_resp.solution_found):
                rospy.logwarn(
                    "GetIK service found no solution for target pose:"
                    + str(self.target_pose.cpu().tolist())
                )

    def set_target_pose_candidates(self, poses):
        """
        Set candidates for target poses. If this is populated, poses will be randomly
        sampled from the list and set as the target pose until a motion plan can be
        found (or fail if no plan can be found for any pose).

        Args:
            poses (list): List of pose candidates. Each candidate can be of any type
                          accepted by the self.set_target_pose function (list-like).
        """
        self._target_pose_candidates = poses
        random.shuffle(self._target_pose_candidates)

    def set_policy(self, state):
        """
        Sets policy (in this case a motion-planned trajectory) given the current state.

        Used both as a helper function on this class as well as allowing parent
        behaviors to force setting the plan before an action is requested (e.g.
        generating open loop sequences of behaviors).
        """
        self._plan = None
        # self.policy_set = False
        if self.target_pose is None and (
            self.config.offset_from_current_ranges is not None
            or self.config.offset_from_current_choices is not None
            or any(self.config.offset_from_current)
        ):

            self.offset_from_current = self.config.offset_from_current
            if self.config.offset_from_current_ranges is not None:
                # Sample offset if ranges for any dims have been specified. Ranges should be list
                # of 3 lists (one for each x,y,z dim), each list having 2 floats giving upper
                # and lower bounds for uniform sampling
                for i, range_ in enumerate(self.config.offset_from_current_ranges):
                    if range_ is not None:
                        self.offset_from_current[i] = np.random.uniform(*range_)
            if self.config.offset_from_current_choices is not None:
                # Set random choice of discrete options if specified for any dim
                for i, choices in enumerate(self.config.offset_from_current_choices):
                    if choices is not None:
                        self.offset_from_current[i] = np.random.choice(choices)

            target_pose = state.ee_state[:7].clone()
            target_pose[:3] += torch.tensor(self.offset_from_current, device=state.ee_state.device)
            self.set_target_pose(target_pose)
            np.set_printoptions(precision=2)
            if self.rospy_log:
                rospy.loginfo(
                    f"Using offset {np.array(self.offset_from_current)} "
                    f"from current pose for {self.config.name.upper()}"
                )
        else:
            rospy.loginfo(f"{self.name} using target pose: {self.target_pose.tolist()}")

        pose_candidate = 0
        n_candidates = len(self._target_pose_candidates)

        if self.target_pose is None and not self._target_pose_candidates and self.rospy_log:
            rospy.logwarn(f"No candidate poses for MoveToPose skill '{self.name}'")

        while (
            not rospy.is_shutdown()
            and self._plan is None
            and (self.target_pose is not None or len(self._target_pose_candidates) > 0)
        ):
            if self.target_pose is None:
                # Try the next candidate pose
                self.set_target_pose(self._target_pose_candidates.pop(0))
                pose_candidate += 1
                if self.rospy_log:
                    rospy.loginfo(
                        f"Trying pose candidate {pose_candidate} of "
                        f"{n_candidates} for {self.config.name.upper()}"
                    )
            # else:
            #     rospy.loginfo('trying target pose')
            start = rospy.get_time()

            plan, success = self._get_plan(state)
            if plan is not None and success and len(plan.trajectory.joint_trajectory.points) > 1:
                self._plan = plan
                # Post-process to interpolate for the simulation timestep
                nominal_traj = self._plan.trajectory.joint_trajectory
                self._trajectory = ros_util.interpolate_joint_trajectory(nominal_traj, state.dt)
                if self._trajectory is None:
                    import pdb

                    pdb.set_trace()

                self.policy_set = True
                break
            else:
                if self.rospy_log:
                    if plan is None:
                        reason = "No plan"
                    elif not success:
                        reason = "Request not successful"
                    elif len(plan.trajectory.joint_trajectory.points) <= 1:
                        reason = "Trajectory too small"
                    else:
                        reason = "Not sure?"
                    if n_candidates > 0:
                        rospy.logwarn(
                            f"Plan not found for candidate {pose_candidate} of "
                            f"{n_candidates} for {self.config.name.upper()} after "
                            f"{rospy.get_time() - start:.2f} secs. Reason: {reason}"
                        )
                    else:
                        rospy.logwarn(
                            "Plan not found for target pose for "
                            f"{self.config.name.upper()} after "
                            f"{rospy.get_time() - start:.2f} secs. Reason: {reason}"
                        )
                self.target_pose = None
        if self._plan is None:
            self.set_failure()
            return False
        return True

    def override_state(self, state):
        """
        Overrides the state by setting the joint position and corresponding end-effector
        pose (as computed from FK) as the last point in the computed motion plan.
        """
        if self._trajectory is None:
            return False
        joint_pos = torch.tensor(
            self._trajectory.points[-1].positions, device=state.joint_position.device
        ).unsqueeze(-1)
        assert state.n_arm_joints == len(joint_pos)
        state.joint_position[: state.n_arm_joints] = joint_pos[:]
        state.ee_state[: state.n_arm_joints] = self.target_pose
        return True

    def get_ros_actions(self):
        return self._trajectory

    def get_behavior_params(self):
        if self.target_pose is not None:
            self._behavior_params["target_pose"] = self.target_pose.cpu().numpy().tolist()
        if self.offset_from_current is not None:
            self._behavior_params["offset_from_current"] = self.offset_from_current
        return super().get_behavior_params()

    def _get_trajectory(self, prev_action):
        """
        Returns the current trajectory that was generated from a motion plan.
        """
        traj = None
        labels = []
        if self._trajectory is not None:
            traj = [
                torch.cat((torch.tensor(pt.positions).to(prev_action.device), prev_action[-1:]))
                for pt in self._trajectory.points
            ]
            labels = [self.name for _ in traj]
        return traj, labels

    def _get_plan(self, state):
        plan = None
        success = False
        if self.config.cartesian_path:
            if self.config.cartesian_planner == "jacobian_pinv":
                plan, success = self._get_jacobian_pinv(state)
            elif self.config.cartesian_planner == "moveit":
                plan, success = self._get_moveit(state)
            elif self.config.cartesian_planner == "trajopt":
                plan, success = self._get_trajopt(state)
            else:
                rospy.logerr("Unable to determine planning settings")
                if self.config.cartesian_path and self.config.cartesian_planner not in PLANNERS:
                    rospy.logerr(f"Unknown Cartesian planner type: {self.config.cartesian_planner}")
        elif self.config.planner == "jacobian_pinv":
            plan, success = self._get_jacobian_pinv(state)
        elif self.config.planner == "moveit":
            plan, success = self._get_moveit(state)
        elif self.config.planner == "trajopt":
            plan, success = self._get_trajopt(state)
        else:
            rospy.logerr("Unable to determine planning settings")
            if self.config.planner not in PLANNERS:
                rospy.logerr(f"Unknown planner type: {self.config.planner}")
        return plan, success

    def _get_jacobian_pinv(self, state):
        start_pose = ros_util.array_to_pose(state.ee_state[:7].cpu().numpy())
        target_pose = ros_util.array_to_pose(self.target_pose.cpu().numpy())
        joint_state = ros_util.get_joint_state_msg(
            state.joint_position[: state.n_arm_joints].cpu().numpy().squeeze(),
            joint_names=state.joint_names[: state.n_arm_joints],
        )
        plan, success = moveit_util.get_jac_pinv_plan(
            joint_state,
            start_pose,
            target_pose,
            self.robot.end_effector.get_link(),
            velocity=self.config.cartesian_vel,
        )
        if plan is not None and not plan.success:
            plan = None
        return plan, success

    def _get_moveit(self, state):
        attached_objects = {}
        for obj_name in self.config.attach_objects:
            # Compute the object pose in the EE frame
            obj_pos_quat = state.object_states[obj_name].clone().cpu().numpy()[:7]
            ee_pos_quat = state.ee_state.clone().cpu().numpy()[:7]
            w_T_obj = math_util.pose_to_homogeneous(obj_pos_quat[:3], obj_pos_quat[3:])
            w_T_ee = math_util.pose_to_homogeneous(ee_pos_quat[:3], ee_pos_quat[3:])
            ee_T_w = math_util.homogeneous_inverse(w_T_ee)
            ee_T_obj = ee_T_w @ w_T_obj
            obj_pos_in_ee, obj_quat_in_ee = math_util.homogeneous_to_pose(ee_T_obj)
            attached_objects[obj_name] = np.concatenate([obj_pos_in_ee, obj_quat_in_ee])

        plan, success = behavior_util.get_plan(
            self.target_pose,
            state,
            self.robot.end_effector.get_link(),
            "world",
            self.config.disable_collisions,
            self.config.max_vel_factor,
            self.config.max_acc_factor,
            self.config.max_plan_attempts,
            self.config.planning_time,
            self.config.cartesian_path,
            self.config.min_cartesian_pct,
            attached_objects=attached_objects,
            touch_links=self.robot.end_effector.get_touch_links(),
        )
        return plan, success

    def _get_trajopt(self, state):
        if MoveToPose.TRAJ_OPT is None:
            from ll4ma_isaacgym.planners.trajopt import TrajectoryOptimizer

            MoveToPose.TRAJ_OPT = TrajectoryOptimizer(
                timesteps=10, num_joints=state.n_arm_joints, max_iterations=1000, verbose=True
            )
        start_joints = state.joint_position.flatten()[: state.n_arm_joints]
        plan = MoveToPose.TRAJ_OPT.get_trajectory(
            start_joints, self.target_pose, obj_state=state, interp=10
        )
        plan = ros_util.tensor2rosTraj(plan, state.dt, self.config.max_vel_factor)
        return plan, True
