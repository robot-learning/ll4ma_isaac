import os

from ll4ma_util import manip_util, ros_util

from ll4ma_isaacgym.core import PickPlaceObjectConfig
from ll4ma_isaacgym.behaviors import Behavior


class StackObjects(Behavior):
    """
    Behavior to build stacks of objects.

    This behavior comprises a sequence of pick-place behaviors, one
    for each object being stacked.

    TODO this behavior is not super robust yet, need some work to configure target
    stacking poses more in terms of strategy to get object on top of another object.
    It's mainly stacking up from centroid, will need to generalize to more arbitrary
    stacking behaviors (e.g. building structured towers)
    """

    def __init__(
        self,
        behavior_config,
        robot,
        env_config,
        sim,
        device="cuda",
        open_loop=False,
        rospy_log=True,
        name="",
    ):
        super().__init__(
            behavior_config, robot, env_config, sim, device, open_loop, rospy_log, name
        )

        behavior_configs = [
            PickPlaceObjectConfig(
                config_dict={
                    "name": f"pick_place_{obj_name}",
                    "target_object": obj_name,
                    "allow_top_grasps": self.config.allow_top_grasps,
                    "allow_side_grasps": self.config.allow_side_grasps,
                    "allow_bottom_grasps": self.config.allow_bottom_grasps,
                    "remove_aligned_short_bb_face": self.config.remove_aligned_short_bb_face,
                    "ignore_error": self.config.ignore_error,
                    "min_cartesian_pct": self.config.min_cartesian_pct,
                    "max_plan_attempts": self.config.max_plan_attempts,
                    "planner": self.config.planner,
                    "cartesian_planner": self.config.cartesian_planner,
                }
            )
            for obj_name in behavior_config.objects
        ]

        if self.config.base_obj_position is not None:
            behavior_configs[0].place_position = self.config.base_obj_position
        if self.config.base_obj_position_ranges is not None:
            behavior_configs[0].place_position_ranges = self.config.base_obj_position_ranges

        self.init_behaviors(behavior_configs, robot, env_config, sim, device, open_loop, rospy_log)

    def get_action(self, state):
        # If we're in open-loop we'll just jump this logic about checking if
        # subehaviors were successful and using current object states
        if self._open_loop:
            return super().get_action(state)

        if not self.is_in_progress():
            self.current_obj_idx = 0
            # Reposition the base object if a target pose (or pose range) was specified,
            # otherwise move onto next object and we'll start stacking up from the base obj
            if (
                not any(self.behavior_configs[0].place_position)
                and self.behavior_configs[0].place_position_ranges is None
            ):
                self.behaviors[f"pick_place_{self.config.objects[0]}"].set_success()

            self.set_in_progress()

        current_obj = self.config.objects[self.current_obj_idx]
        current_behavior = f"pick_place_{current_obj}"

        if self.behaviors[current_behavior].is_complete_success():
            # Transition to next object if there's more to stack
            self.robot.end_effector.reset()
            self.prev_obj = current_obj
            prev_obj_config = self.env_config.objects[self.prev_obj]
            prev_obj_pose = state.object_states[self.prev_obj].clone().cpu().numpy()[:7]
            prev_obj_height = self.get_object_height(prev_obj_config)

            self.current_obj_idx += 1
            if self.current_obj_idx < len(self.config.objects):
                current_obj = self.config.objects[self.current_obj_idx]
                obj_config = self.env_config.objects[current_obj]
                obj_height = self.get_object_height(obj_config)

                target_obj_pose = prev_obj_pose
                target_obj_pose[2] += (
                    (prev_obj_height + obj_height) / 2.0
                ) + self.config.stack_buffer

                self.behaviors[f"pick_place_{current_obj}"].set_place_pose(target_obj_pose)

        self.policy_set = True  # This will make sure it doesn't recompute an open-loop policy
        return super().get_action(state)

    def set_policy(self, state):
        base_obj = self.config.objects[0]
        base_pose = state.object_states[base_obj].clone().cpu().numpy()[:7]
        # TODO if you wanted to set where the base of the stack is, you'll need to account
        # for that here and set the base pose as the one specified in config, and then
        # don't delete the behavior associated with first object pick-place, i.e. add a
        # conditional statement here that checks if you want to set base pose
        self.delete_behavior(f"pick_place_{base_obj}")
        objects = self.config.objects[1:]

        current_height = self.get_object_height(self.env_config.objects[base_obj])

        for current_obj in objects:
            current_behavior = f"pick_place_{current_obj}"

            obj_config = self.env_config.objects[current_obj]
            obj_height = self.get_object_height(obj_config)

            target_obj_pose = base_pose
            target_obj_pose[2] += ((current_height + obj_height) / 2.0) + self.config.stack_buffer

            current_height += obj_height
            self.behaviors[current_behavior].set_place_pose(target_obj_pose)

        return super().set_policy(state)

    def get_object_height(self, config):
        """
        Determines the height of an object. Currently determines this from
        the bounding box for meshes (using URDF files) and primitive shapes.

        Args:
            config (ObjectConfig): Object configuration
        Returns:
            height (float): Height of the object in meters
        """
        if config.object_type == "urdf":
            urdf_path = os.path.join(config.asset_root, config.asset_filename)
            mesh_filename = ros_util.get_mesh_filename_from_urdf(urdf_path, collision=True)
            height = manip_util.get_bb_dims(mesh_filename)[2]
        else:
            height = manip_util.get_bb_dims(prim_type=config.object_type, prim_dims=config.extents)[
                2
            ]
        return height
