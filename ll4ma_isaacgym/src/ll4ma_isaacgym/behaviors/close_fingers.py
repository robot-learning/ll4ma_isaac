import rospy
import torch
from ll4ma_isaacgym.behaviors import Behavior


class CloseFingers(Behavior):
    """
    Simple behavior to close the fingers to grasp something.

    TODO would be good to add force monitoring or better state checking to
    ensure it's completed the close action, right now it just repeats the
    action for a set number of timesteps
    """

    def __init__(self, behavior_config, robot, env_config, sim, *args):
        super().__init__(behavior_config, robot, env_config, sim, *args)

        # TODO these are temporary until I get force monitoring working
        self._step = 0
        self.close_for_steps = self.robot.end_effector.config.close_for_steps

    def get_action(self, state):
        if self.is_complete():
            return None

        if self.is_not_started():
            if self.rospy_log:
                rospy.loginfo(f"Running behavior: {self.name.upper()}")
            self.set_in_progress()

        action = state.prev_action
        # action.set_ee_discrete('close')
        action[-1] = self.robot.end_effector.config.discretes.close

        # TODO temporary until force monitoring works
        self._step += 1
        if self._step > self.close_for_steps:
            self.set_success()

        return action

    def set_policy(self, state):
        return True

    def _get_trajectory(self, prev_action):
        trajectory = torch.stack([prev_action.clone()] * self.close_for_steps)
        trajectory[:, -1] = self.robot.end_effector.config.discretes.close
        return [traj for traj in trajectory], [self.name for _ in trajectory]

    def get_ros_actions(self):
        return "close"

    def override_state(self, state):
        """
        Overrides the state by setting the ee pose to be closed
        """
        if len(state.joint_position) == state.n_arm_joints:
            return True
        idxs = self.robot.end_effector.config.grip_finger_indices
        state.joint_position[idxs, 0] = torch.tensor(
            self.robot.end_effector.config.close_finger_joint_pos,
            device=state.joint_position.device,
        )
        # state.prev_action.set_ee_discrete('close')
        return True

    def adapt_action_for_objects(self):
        if self.config.adapt_to_contact:
            return [self.config.target_object]
        else:
            return []
