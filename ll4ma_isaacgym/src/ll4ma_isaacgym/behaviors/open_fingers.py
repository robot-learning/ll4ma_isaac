import rospy
import torch
from ll4ma_isaacgym.behaviors import Behavior


class OpenFingers(Behavior):
    """
    Simple behavior to open the fingers all the way.

    TODO should add better checks that the fingers actually opened,
    right now there's essentially no failure case
    """

    def __init__(self, behavior_config, robot, env_config, *args):
        super().__init__(behavior_config, robot, env_config, *args)

        self._step = 0
        self.open_for_steps = self.robot.end_effector.config.open_for_steps

    def get_action(self, state):
        if self.is_complete():
            return None

        if self.is_not_started():
            if self.rospy_log:
                rospy.loginfo(f"Running behavior: {self.name.upper()}")
            self.set_in_progress()

        action = state.prev_action.clone()
        # action.set_ee_discrete('open')
        action[-1] = self.robot.end_effector.config.discretes.open

        # TODO temporary until better checks on result
        self._step += 1
        if self._step > self.open_for_steps:
            self.set_success()

        return action

    def set_policy(self, state):
        return True

    def _get_trajectory(self, prev_action):
        trajectory = torch.stack([prev_action.clone()] * self.open_for_steps)
        trajectory[:, -1] = self.robot.end_effector.config.discretes.open
        return [traj for traj in trajectory], [self.name for _ in trajectory]

    def get_ros_actions(self):
        return "open"

    def override_state(self, state):
        """
        Overrides the state by setting the ee pose to be open
        """
        if len(state.joint_position) == state.n_arm_joints:
            return True
        idxs = self.robot.end_effector.config.grip_finger_indices
        state.joint_position[idxs, 0] = torch.tensor(
            self.robot.end_effector.config.open_finger_joint_pos, device=state.joint_position.device
        )
        # state.prev_action.set_ee_discrete('open')
        return True
