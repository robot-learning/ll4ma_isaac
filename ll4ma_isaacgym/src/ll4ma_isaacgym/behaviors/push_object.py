import random
import numpy as np

from ll4ma_isaacgym.core import MoveToPoseConfig
from ll4ma_isaacgym.behaviors import Behavior
from ll4ma_isaacgym.candidate_generators import PushCandidateGenerator


class PushObject(Behavior):
    """
    Behavior for pushing an object.

    This behavior is hierarchical and has the steps of:
        1. Move end-effector through free space to a pre-push pose (configuration space)
           aligned to direction the object will be pushed in
        2. Move end-effector to a target end pose constrained to a straight-line
           Cartesian path, such that moving from pre-push to target pose pushes
           the object in a more or less straight line.

    Each movement is a motion-planned trajectory.
    """

    def __init__(
        self,
        behavior_config,
        robot,
        env_config,
        sim,
        device="cuda",
        open_loop=False,
        rospy_log=True,
        name="",
    ):
        super().__init__(
            behavior_config, robot, env_config, sim, device, open_loop, rospy_log, name
        )

        self.candidate_generator = PushCandidateGenerator()

        if self.config.target_objects:
            # choosen_objects = self.config.target_objects
            self.target_object = random.choice(self.config.target_objects)
            target_objects = [
                o for o in env_config.objects.keys() if not env_config.objects[o].fix_base_link
            ]
        elif self.config.target_object:
            self.target_object = self.config.target_object
            target_objects = [
                o for o in env_config.objects.keys() if not env_config.objects[o].fix_base_link
            ]
        else:
            # Choose a random object
            target_objects = [
                o for o in env_config.objects.keys() if not env_config.objects[o].fix_base_link
            ]
            self.target_object = random.choice(target_objects)

        self.selected_candidate = None  # PushPoseCandidate, populated if valid one found

        behavior_configs = [
            MoveToPoseConfig(
                config_dict={
                    "name": "approach",
                    "end_effector_frame": robot.end_effector.get_link(),
                    "max_plan_attempts": self.config.max_plan_attempts,
                    "planning_time": self.config.planning_time,
                    "max_vel_factor": self.config.max_vel_factor,
                    "max_acc_factor": self.config.max_acc_factor,
                    "ignore_error": self.config.ignore_error,
                    "planner": self.config.planner,
                }
            ),
            MoveToPoseConfig(
                config_dict={
                    "name": "push",
                    "end_effector_frame": robot.end_effector.get_link(),
                    "max_plan_attempts": self.config.max_plan_attempts,
                    "planning_time": self.config.planning_time,
                    "max_vel_factor": self.config.max_vel_factor,
                    "max_acc_factor": self.config.max_acc_factor,
                    "ignore_error": self.config.ignore_error,
                    "planner": self.config.planner,
                    "cartesian_path": True,
                    "cartesian_planner": self.config.cartesian_planner,
                    "cartesian_vel": self.config.cartesian_vel,
                    "min_cartesian_pct": self.config.min_cartesian_pct,
                    "disable_collisions": [self.target_object],
                }
            ),
            MoveToPoseConfig(
                config_dict={
                    "name": "return",
                    "end_effector_frame": robot.end_effector.get_link(),
                    "max_plan_attempts": self.config.max_plan_attempts,
                    "planning_time": self.config.planning_time,
                    "max_vel_factor": self.config.max_vel_factor,
                    "max_acc_factor": self.config.max_acc_factor,
                    "ignore_error": self.config.ignore_error,
                    "disable_collisions": target_objects,
                }
            ),
        ]

        self.init_behaviors(behavior_configs, robot, env_config, sim, device, open_loop, rospy_log)

    def get_behavior_params(self):
        # Only logging if they were set
        if self.selected_candidate is not None:
            bps = {}
            bps["target_object"] = self.config.target_object
            bps["init_object_pose"] = self.selected_candidate.init_obj_pose
            bps["target_object_pose"] = self.selected_candidate.target_obj_pose
            bps["ee_prepush_pose"] = self.selected_candidate.prepush_pose
            bps["ee_end_pose"] = self.selected_candidate.end_pose
            bps["push_direction"] = self.selected_candidate.push_direction
            bps["push_distance"] = self.selected_candidate.push_distance
            bps["push_height_offset"] = self.selected_candidate.push_height_offset
            self._behavior_params = bps
        return super().get_behavior_params()

    def set_policy(self, state):
        """
        Plan is a two-phase sequence that first plans a trajectory to be at a pre-push
        pose close to object surface, and an end pose such that if it moves at a
        straight line from pre-push pose to end pose it will push the object more or
        less in a straight line.

        Note the plan to pre-push pose is in configuration space while the actual push
        motion is a constrained Cartesian path to move in a straight line.

        TODO push actions can be parameterized in multiple ways, so there should be options
        to set things directly or infer based on sufficient info. Can have start/end poses
        for object, or start pose and push dist, possibly others
        """
        self.init_obj_pose = state.object_states[self.target_object].clone().cpu().numpy()[:7]

        if self.config.target_position_ranges is None:
            # Only candidate will be the exact target that was set in the config
            target = self.init_obj_pose.copy()
            for i, pos_i in enumerate(self.config.target_position):
                if pos_i is not None:
                    target[i] = pos_i
        else:
            target = None

        candidates = self.candidate_generator.get_candidates(
            self.init_obj_pose, self.config, self.robot.end_effector.get_link(), target
        )

        self._open_loop = True  # Force open-loop so we jointly plan approach and push

        # Try all candidates until we either find successful one or fail on all of them
        for i, candidate in enumerate(candidates):
            self.set_approach_ee_pose(np.array(candidate.prepush_pose))
            self.set_end_ee_pose(np.array(candidate.end_pose))
            self.set_return_ee_pose(np.array(candidate.prepush_pose))
            if super().set_policy(state):
                self.selected_candidate = candidate
                return True  # Plans are set now on both sub-behaviors
            else:
                self.set_not_started()  # Reset since sub-behaviors may have reported fail
        self.set_failure()  # If we reached here, all candidates failed
        return False

    def set_approach_ee_pose(self, pose):
        self.behaviors["approach"].set_target_pose(pose)

    def set_end_ee_pose(self, pose):
        self.behaviors["push"].set_target_pose(pose)

    def set_return_ee_pose(self, pose):
        self.behaviors["return"].set_target_pose(pose)
