import rospy
import torch
from enum import Enum
from copy import deepcopy
from collections import OrderedDict

from ll4ma_util import func_util

from ll4ma_isaacgym.core.config import BEHAVIOR_MODULES  # for dynamic class creation


class BehaviorStatus(Enum):
    NOT_STARTED = 1
    IN_PROGRESS = 2
    SUCCESS = 3
    FAILURE = 4


class Behavior:
    """
    Base class for behaviors.

    This class does a lot of heavy lifting for hierarchical behaviors in terms
    of managing transitions between sub-behaviors and retrieving actions/status
    from the sub-behaviors.
    """

    def __init__(
        self,
        behavior_config,
        robot,
        env_config,
        sim,
        device="cuda",
        open_loop=False,
        rospy_log=True,
        name="",
    ):
        self.name = name
        self.config = behavior_config  # Type BehaviorConfig
        self.robot = robot
        self.env_config = env_config
        self.sim = sim
        self.device = device
        self._trajectory = None
        self._labels = None
        self._behavior_seq = None
        self.traj_idx = 0
        self.rospy_log = rospy_log

        self.init_behaviors(
            self.config.behaviors, self.robot, env_config, sim, device, open_loop, rospy_log
        )

        self._behavior_params = {"type": self.__class__.__name__}
        if self.n_behaviors > 0:
            self._behavior_params["behaviors"] = {}

        # This allows setting all sub-behaviors based on current state, e.g. you can request
        # all plans at beginning
        self._open_loop = open_loop

        self.policy_set = False

        self._current_behavior = None
        # Manage current behavior with index so it's easy to revert back to previous on failure
        self._current_behavior_idx = 0
        self._subbehavior_transition = False

        self._prev_act = None
        self._wait_after_behavior_idx = 0

        self.set_not_started()

    def get_action(self, state):
        """
        Returns the next action to apply in the simulator given the current
        state of the simulator.

        Args:
            state (EnvironmentState): Current state of the simulator
        Returns:
            action (Action): Next action to apply in the simulator
        """
        if not self.policy_set:
            self.set_policy(state)

        if self._open_loop:
            if self._trajectory is None:
                self.get_trajectory(state)
            if self._trajectory is None:
                self.set_failure()
                if self.rospy_log:
                    rospy.logwarn("GET_ACTION: Plan failed!")
                return None
            if self.traj_idx >= len(self._trajectory):
                self.set_success()
                self._subbehavior_transition = True  # Save data for last timestep
                return None
            action = self._trajectory[self.traj_idx].clone()
            # Check if we've switched behaviors for behavior interval data logging
            if self.traj_idx > 0:
                current_label = self._labels[self.traj_idx]
                prev_label = self._labels[self.traj_idx - 1]
                self._subbehavior_transition = current_label != prev_label
            # Still track the current behavior being executed even in open loop
            if (
                self.traj_idx < len(self._behavior_seq)
                and self._behavior_seq[self.traj_idx] in self.behaviors
            ):
                self._current_behavior = self.behaviors[self._behavior_seq[self.traj_idx]]
            else:
                self._current_behavior = None
            self.traj_idx += 1
            return action

        self._set_status()
        self._subbehavior_transition = False

        # Transition to next sub-behavior if current one is done
        if self._current_behavior is not None and self._current_behavior.is_complete():
            # Optional wait period after behavior finishes to give env time to settle
            if self._wait_after_behavior_idx < self.config.wait_after_behavior:
                self._current_behavior._subbehavior_transition = False
                self._wait_after_behavior_idx += 1
                return self._prev_act
            self._current_behavior._subbehavior_transition = False
            self._current_behavior = None
            self._current_behavior_idx += 1

        if self.is_complete():
            self._subbehavior_transition = True  # So we get final timestep
            return None

        # Initialize next behavior if none is active
        if self._current_behavior is None:
            self._subbehavior_transition = True  # So we get initial timestep
            self._current_behavior = list(self.behaviors.values())[self._current_behavior_idx]

        act = self._current_behavior.get_action(state)
        self._prev_act = act.clone() if act is not None else None

        return act.clone() if act is not None else None

    def set_policy(self, state):
        self.policy_set = True  # Will set to false if open loop and sub-behavior set_policy fails
        if self._open_loop and self.n_behaviors > 0:
            state = deepcopy(state)
            for behavior in self.behaviors.values():
                if behavior.set_policy(state):
                    # Need to set start points as plan endpoints
                    if not behavior.override_state(state):
                        # TODO: if override fails then something went wrong
                        # This shouldn't ever fail if set_policy succeeds
                        print(behavior.name, "failed override")
                        import pdb

                        pdb.set_trace()
                else:
                    self.policy_set = False
                    if self.rospy_log:
                        rospy.logwarn(f"Policy not set: {behavior.name.upper()}")
                    break
        return self.policy_set

    def override_state(self, state):
        """
        Base implementation is a no-op, some behaviors will override the state values for
        generating open loop behavior sequences (e.g. sequence of motion plans will set
        the start joint position of the second plan to be the end position of the first plan).
        """
        if self.n_behaviors > 0:
            for behavior in self.behaviors.values():
                if not behavior.override_state(state):
                    return False
        return True

    def get_trajectory(self, state):
        if not self.policy_set:
            self.set_policy(state)
        if self.policy_set and self._trajectory is None:
            self.traj_idx = 0
            trajectory, labels = self._get_trajectory(state.prev_action)
            if trajectory is not None:
                self._trajectory = torch.stack(trajectory)
                # Pre-pend behavior name to reflect hierarchy of behaviors
                self._labels = [f"{self.name}:{label}" for label in labels]
                # This is for tracking the high-level behavior being executed
                # so we know which behavior is being run even in open loop
                self._behavior_seq = [label.split(":")[0] for label in labels]
        return self._trajectory, self._labels

    def _get_trajectory(self, prev_action):
        trajectory = [prev_action]
        labels = []
        for behavior_name, behavior in self.behaviors.items():
            b_traj, b_labels = behavior._get_trajectory(trajectory[-1])
            if b_traj is None:
                return None, []
            trajectory += b_traj
            labels += b_labels
        return trajectory[1:], labels

    def get_ros_actions(self):
        """
        TODO this is kind of a hack to work around the fact get_trajectory is
        over-optimized now for Tensors, when we often want the JointTrajectory
        that exists on the motion-planned behaviors for visualization and
        commanding real robot.
        """
        if not self.policy_set:
            rospy.logerr("Must set policy before retrieving ROS actions")
            return None
        return {b.name: b.get_ros_actions() for b in self.behaviors.values()}

    def is_complete(self):
        return self._status in [BehaviorStatus.SUCCESS, BehaviorStatus.FAILURE]

    def is_complete_success(self):
        return self._status == BehaviorStatus.SUCCESS

    def is_complete_failure(self):
        return self._status == BehaviorStatus.FAILURE

    def is_in_progress(self):
        return self._status == BehaviorStatus.IN_PROGRESS

    def is_not_started(self):
        return self._status == BehaviorStatus.NOT_STARTED

    def is_subbehavior_transition(self):
        """
        Determines if a sub-behavior transition is occurring anywhere in the behavior
        hierarchy (i.e. not just a transition between this behavior's sub-behaviors,
        but also if there is any transition between a sub-behavior's sub-behaviors,
        recursively).

        This is useful for data collection when logging data only after a sub-behavior
        completes. This can be overridden to customize when to report sub-behavior
        transitions so data logging will occur as desired for each behavior.
        """
        return self._subbehavior_transition or any(
            [b.is_subbehavior_transition() for b in self.behaviors.values()]
        )

    def set_success(self):
        self._set_status(BehaviorStatus.SUCCESS)

    def set_failure(self):
        self._set_status(BehaviorStatus.FAILURE)

    def set_in_progress(self):
        self._set_status(BehaviorStatus.IN_PROGRESS)

    def set_not_started(self):
        for behavior in self.behaviors.values():
            behavior.set_not_started()
        self.policy_set = False
        self._trajectory = None
        self._behavior_seq = None
        self.traj_idx = 0
        self._set_status(BehaviorStatus.NOT_STARTED)

    def init_behaviors(
        self,
        behavior_configs,
        robot,
        env_config,
        sim,
        device="cuda",
        open_loop=False,
        rospy_log=True,
    ):
        """
        Initializes sub-behaviors for hierarchical behaviors.
        """
        self.behavior_configs = behavior_configs
        self.behaviors = OrderedDict()
        for behavior_config in behavior_configs:
            Class = self._get_behavior_class(behavior_config.behavior_type)
            behavior = Class(
                behavior_config,
                robot,
                env_config,
                sim,
                device,
                open_loop,
                rospy_log,
                behavior_config.name,
            )
            behavior.name = behavior_config.name
            self.behaviors[behavior_config.name] = behavior
        self.n_behaviors = len(self.behaviors)

    def get_behavior_string(self):
        """
        Returns a string representing the behavior. The string reflects the hierarchy
        of sub-behaviors by appending each sub-behavior name separated by a colon, e.g.
        'stack_objects:pick_object1:approach`.

        If in open-loop mode, it retrieves the string from the pre-populated labels
        since that's the only way to know what the hierarchy is for the current step.
        Otherwise, it can directly infer from the current sub-behavior being run.
        """
        if self._open_loop and self._labels is not None and self.traj_idx < len(self._labels):
            name = self._labels[self.traj_idx]
        else:
            name = self.name
            if self._current_behavior is not None:
                name += f":{self._current_behavior.get_behavior_string()}"
            # Don't want leading colon for top-level behavior null string name
            if name.startswith(":"):
                name = name[1:]
        return name

    def get_behavior_params(self):
        current = self._current_behavior
        if current is not None:
            if "behaviors" not in self._behavior_params:
                self._behavior_params["behaviors"] = {}
            self._behavior_params["behaviors"][current.name] = current.get_behavior_params()
        return self._behavior_params

    def delete_behavior(self, name):
        if name in self.behaviors:
            del self.behaviors[name]
            self.n_behaviors -= 1

    def adapt_action_for_objects(self):
        """
        Some behaviors will want the simulator to adapt an action online when it's
        executing the low-level actions, e.g. stopping fingers when they come in
        contact with an object to be grasped. This should return a list of object
        names that action should be adapted for.
        """
        if self.n_behaviors > 0 and self._current_behavior is not None:
            return self._current_behavior.adapt_action_for_objects()
        else:
            return []

    def _set_status(self, status=None):
        """
        Maintaining internal _status of type BehaviorStatus to track state machine, and then
        public status that appends behavior name and informs parent behaviors what's going on.

        TODO this gets kind of confusing, can probably make this much easier to follow
        """
        if status is not None:
            self._status = status
            self.status = f"{self.config.name}.{status.name}"

        elif self._current_behavior is not None:
            sub_behavior_fail = self._current_behavior._status == BehaviorStatus.FAILURE
            final_sub_behavior_success = (
                self._current_behavior._status == BehaviorStatus.SUCCESS
                and self._current_behavior_idx == self.n_behaviors - 1
            )
            if sub_behavior_fail or final_sub_behavior_success:
                # If a sub-behavior fails, we fail also
                self._status = self._current_behavior._status
                self.status = f"{self.config.name}.{self._status.name}"
            else:
                self._status = BehaviorStatus.IN_PROGRESS
                self.status = f"{self.config.name}.{self._current_behavior.status}"

    def _get_behavior_class(self, behavior_type):
        """
        Finds the behavior class associated with a behavior type for
        dynamic class creation.
        """
        BehaviorClass = func_util.get_class(behavior_type, BEHAVIOR_MODULES)
        if BehaviorClass is None:
            raise ValueError(f"Unknown behavior type: {behavior_type}")
        return BehaviorClass
