import os

from ll4ma_util import ros_util

from ll4ma_isaacgym.core import MoveToPoseConfig
from ll4ma_isaacgym.behaviors import Behavior
from ll4ma_isaacgym.candidate_generators import BBPoseCandidateGenerator


class Reach2Object(Behavior):
    """
    Behavior to reach to an object.

    This behavior is hierarchical and has the steps of:
        1. Reach through free space to be at an approach point offset from object
    """

    def __init__(
        self,
        behavior_config,
        robot,
        env_config,
        sim,
        device="cuda",
        open_loop=False,
        rospy_log=True,
        name="",
    ):
        super().__init__(
            behavior_config, robot, env_config, sim, device, open_loop, rospy_log, name
        )

        self.candidate_generator = BBPoseCandidateGenerator()

        behavior_configs = [
            MoveToPoseConfig(
                config_dict={
                    "name": "approach",
                    "end_effector_frame": robot.end_effector.get_link(),
                    "max_plan_attempts": self.config.max_plan_attempts,
                    "max_vel_factor": self.config.max_vel_factor,
                    "max_acc_factor": self.config.max_acc_factor,
                    "planning_time": self.config.planning_time,
                    "ignore_error": self.config.ignore_error,
                    "planner": self.config.planner,
                }
            )
        ]

        self.init_behaviors(behavior_configs, robot, env_config, sim, device, open_loop, rospy_log)

    def get_behavior_params(self):
        self._behavior_params["target_object"] = self.config.target_object
        return super().get_behavior_params()

    def set_policy(self, state):
        target_obj = self.config.target_object
        obj_config = self.env_config.objects[target_obj]
        self.init_obj_pose = state.object_states[target_obj].clone().cpu().numpy()[:7]

        if obj_config.object_type == "urdf":
            urdf_path = os.path.join(obj_config.asset_root, obj_config.asset_filename)
            mesh_filename = ros_util.get_mesh_filename_from_urdf(urdf_path, collision=True)
            prim_type = ""
            prim_dims = None
        else:
            mesh_filename = ""
            prim_type = obj_config.object_type
            prim_dims = obj_config.extents

        candidates = self.candidate_generator.get_candidates(
            self.init_obj_pose,
            self.robot.end_effector.get_link(),
            self.robot.config.end_effector.obj_to_ee_offset,
            mesh_filename,
            prim_type,
            prim_dims,
            not self.config.allow_bottom_grasps,
            not self.config.allow_top_grasps,
            not self.config.allow_side_grasps,
            self.config.remove_aligned_short_bb_face,
        )

        self.behaviors["approach"].set_target_pose_candidates(candidates)

        return super().set_policy(state)

    def override_state(self, state):
        super().override_state(state)
