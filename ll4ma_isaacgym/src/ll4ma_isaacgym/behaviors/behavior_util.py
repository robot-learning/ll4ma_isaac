import rospy

from sensor_msgs.msg import JointState
from moveit_msgs.msg import CollisionObject
from geometry_msgs.msg import PoseStamped, Pose, Point, Quaternion

import moveit_interface.util as moveit_util
from ll4ma_util import ros_util


def get_plan(
    target_pose,
    state,
    ee_link,
    frame="world",
    disable_collisions=[],
    max_vel_factor=0.1,
    max_acc_factor=0.1,
    max_plan_attempts=1,
    planning_time=1.0,
    cartesian_path=False,
    min_cartesian_pct=1.0,
    visualize_pose=False,
    attached_objects={},
    touch_links=[],
):
    """
    Helper function to request a plan from Isaac Gym state info.
    """
    if isinstance(target_pose, str):
        target = target_pose
    else:
        target = PoseStamped()
        target.header.frame_id = frame
        target.pose.position.x = target_pose[0]
        target.pose.position.y = target_pose[1]
        target.pose.position.z = target_pose[2]
        target.pose.orientation.x = target_pose[3]
        target.pose.orientation.y = target_pose[4]
        target.pose.orientation.z = target_pose[5]
        target.pose.orientation.w = target_pose[6]

        if visualize_pose:
            # Publish pose for debugging in rviz
            pub = rospy.Publisher("/target_pose", PoseStamped, queue_size=1)
            ros_util.wait_for_publisher(pub)
            pub.publish(target)

    joint_state = JointState()
    joint_state.position = state.joint_position.cpu().numpy().flatten().tolist()
    joint_state.velocity = state.joint_velocity.cpu().numpy().flatten().tolist()
    joint_state.effort = state.joint_torque.cpu().numpy().flatten().tolist()
    joint_state.name = state.joint_names

    objects = get_moveit_objects_from_state(
        state, disable_collisions, attached_objects, ee_link, touch_links
    )

    resp, success = moveit_util.get_plan(
        target,
        joint_state,
        ee_link,
        objects,
        max_vel_factor,
        max_acc_factor,
        max_plan_attempts,
        planning_time,
        cartesian_path,
        min_cartesian_pct,
    )
    return resp, success


def get_moveit_objects_from_state(
    state, disable_collisions=[], attached_objects={}, ee_link=None, touch_links=[]
):
    objects = {k: v.to_dict() for k, v in state.objects.items()}

    for obj_name in state.objects.keys():
        if obj_name in disable_collisions:
            objects[obj_name]["operation"] = CollisionObject.REMOVE
        else:
            objects[obj_name]["operation"] = CollisionObject.ADD
        obj_pose = state.object_states[obj_name].clone().cpu().numpy()[:7]
        objects[obj_name]["pose"] = Pose(Point(*obj_pose[:3]), Quaternion(*obj_pose[3:]))
        if obj_name in state.object_colors:
            objects[obj_name]["color"] = state.object_colors[obj_name]
        if obj_name in state.object_mesh_roots:
            objects[obj_name]["asset_root"] = state.object_mesh_roots[obj_name]
        if obj_name in state.object_mesh_filenames:
            objects[obj_name]["asset_filename"] = state.object_mesh_filenames[obj_name]
        if obj_name in attached_objects:
            objects[obj_name]["attached_pose"] = Pose(
                Point(*attached_objects[obj_name][:3]), Quaternion(*attached_objects[obj_name][3:])
            )
            objects[obj_name]["attach_to_link"] = ee_link
            objects[obj_name]["touch_links"] = touch_links

    return objects
