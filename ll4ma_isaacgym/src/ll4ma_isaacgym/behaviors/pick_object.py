import rospy
import os

from ll4ma_util import ros_util

from ll4ma_isaacgym.core import MoveToPoseConfig, CloseFingersConfig
from ll4ma_isaacgym.behaviors import Behavior
from ll4ma_isaacgym.candidate_generators import PickCandidateGenerator


class PickObject(Behavior):
    """
    Behavior to pick an object up.

    This behavior is hierarchical and has the steps of:
        1. Reach through free space to be at an approach point offset from object
        2. Close fingers to grasp the object
        3. Lift object up off supporting surface

    TODO Better success/failure conditions can be defined, e.g. making sure object
    was actually grasped. Right now it's kind of heuristically done on sub-behaviors

    TODO Probably better to have a proper GraspObject sub-behavior, right now it
    just very simply closes the fingers and hope you grab it.

    TODO Currently disabling collisions with target object on lift, that is a hack
    because we don't yet have attached collisions working in MoveIt
    """

    def __init__(
        self,
        behavior_config,
        robot,
        env_config,
        sim,
        device="cuda",
        open_loop=False,
        rospy_log=True,
        name="",
    ):
        super().__init__(
            behavior_config, robot, env_config, sim, device, open_loop, rospy_log, name
        )

        self.candidate_generator = PickCandidateGenerator()
        self.selected_candidate = None

        behavior_configs = []
        if not self.config.pregrasped:
            behavior_configs.append(
                MoveToPoseConfig(
                    config_dict={
                        "name": "approach",
                        "end_effector_frame": robot.end_effector.get_link(),
                        "max_plan_attempts": self.config.max_plan_attempts,
                        "max_vel_factor": self.config.max_vel_factor,
                        "max_acc_factor": self.config.max_acc_factor,
                        "planning_time": self.config.planning_time,
                        "ignore_error": self.config.ignore_error,
                        "planner": self.config.planner,
                    }
                )
            )
        behavior_configs.append(
            CloseFingersConfig(
                config_dict={
                    "name": "grasp",
                    "behavior_type": "CloseFingers",
                    "target_object": self.config.target_object,
                    "adapt_to_contact": self.config.adapt_to_contact,
                }
            )
        )
        behavior_configs.append(
            MoveToPoseConfig(
                config_dict={
                    "name": "lift",
                    "offset_from_current": self.config.lift_offset,
                    "offset_from_current_ranges": self.config.lift_offset_ranges,
                    "offset_from_current_choices": self.config.lift_offset_choices,
                    "disable_collisions": [self.config.target_object],
                    "end_effector_frame": robot.end_effector.get_link(),
                    "max_plan_attempts": self.config.max_plan_attempts,
                    "max_vel_factor": self.config.max_vel_factor,
                    "max_acc_factor": self.config.max_acc_factor,
                    "planning_time": self.config.planning_time,
                    "ignore_error": self.config.ignore_error,
                    "planner": self.config.planner,
                    "cartesian_path": True,
                    "cartesian_planner": self.config.cartesian_planner,
                    "min_cartesian_pct": self.config.min_cartesian_pct,
                }
            )
        )

        self.init_behaviors(behavior_configs, robot, env_config, sim, device, open_loop, rospy_log)

    def get_behavior_params(self):
        if self.selected_candidate is not None:
            bps = {}
            bps["target_object"] = self.config.target_object
            bps["target_object_pose"] = self.selected_candidate.target_obj_pose
            bps["init_object_pose"] = self.selected_candidate.init_obj_pose
            bps["ee_pick_pose"] = self.selected_candidate.pick_ee_pose
            bps["ee_lift_pose"] = self.selected_candidate.lift_ee_pose
            bps["pick_joints"] = self.selected_candidate.pick_joints
            bps["lift_joints"] = self.selected_candidate.lift_joints
            self._behavior_params = bps
        return super().get_behavior_params()

    def set_policy(self, state):
        target_obj = self.config.target_object
        obj_config = self.env_config.objects[target_obj]
        self.init_obj_pose = state.object_states[target_obj].clone().cpu().numpy()[:7]

        self._open_loop = True  # Force open loop to jointly plan for approach and lift motions

        if self.config.pregrasped:
            return super().set_policy(state)
        else:
            if obj_config.object_type == "urdf":
                urdf_path = os.path.join(obj_config.asset_root, obj_config.asset_filename)
                mesh_filename = ros_util.get_mesh_filename_from_urdf(urdf_path, collision=True)
                prim_type = ""
                prim_dims = None
            else:
                mesh_filename = ""
                prim_type = obj_config.object_type
                prim_dims = obj_config.extents

            candidates = self.candidate_generator.get_candidates(
                self.init_obj_pose,
                self.config,
                self.robot.end_effector.get_link(),
                self.robot.config.end_effector.obj_to_ee_offset,
                mesh_filename,
                prim_type,
                prim_dims,
                not self.config.allow_bottom_grasps,
                not self.config.allow_top_grasps,
                not self.config.allow_side_grasps,
                self.config.remove_aligned_short_bb_face,
            )

            for i, candidate in enumerate(candidates):
                rospy.logwarn(f"Trying pick candidate {i+1} of {len(candidates)}")
                self.set_pick_ee_pose(candidate.pick_ee_pose)
                self.set_lift_ee_pose(candidate.lift_ee_pose)
                if super().set_policy(state):
                    self.selected_candidate = candidate
                    return True
                else:
                    self.set_not_started()

            self.set_failure()
            return False

    def set_pick_ee_pose(self, pose):
        self.behaviors["approach"].set_target_pose(pose)

    def set_lift_ee_pose(self, pose):
        self.behaviors["lift"].set_target_pose(pose)

    def compute_target_object_pose(self):
        target_obj_pose = self.init_obj_pose.copy()
        target_obj_pose[:3] += self.behaviors["lift"].offset_from_current
        return target_obj_pose

    def override_state(self, state):
        return super().override_state(state)
        # target_obj_pose = self.compute_target_object_pose()
        # state.object_states[self.config.target_object][:7] = torch.tensor(target_obj_pose)
        # state.object_states[self.config.target_object][:7] = torch.tensor(self.target_obj_pose)

    def adapt_action_for_objects(self):
        if self.config.adapt_to_contact:
            return [self.config.target_object]
        else:
            return []
