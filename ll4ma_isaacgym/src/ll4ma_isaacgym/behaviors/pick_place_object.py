from ll4ma_isaacgym.core import PickObjectConfig, PlaceObjectConfig
from ll4ma_isaacgym.behaviors import Behavior


class PickPlaceObject(Behavior):
    """
    Behavior to pick a target object up and place in specified location.

    This behavior is hierarchical that combines the two steps of picking the
    object and then placing the object.
    """

    def __init__(
        self,
        behavior_config,
        robot,
        env_config,
        sim,
        device="cuda",
        open_loop=False,
        rospy_log=True,
        name="",
    ):
        super().__init__(
            behavior_config, robot, env_config, sim, device, open_loop, rospy_log, name
        )

        behavior_configs = [
            PickObjectConfig(
                config_dict={
                    "name": "pick",
                    "target_object": self.config.target_object,
                    "lift_offset": self.config.lift_offset,
                    "lift_offset_ranges": self.config.lift_offset_ranges,
                    "lift_offset_choices": self.config.lift_offset_choices,
                    "max_plan_attempts": self.config.max_plan_attempts,
                    "max_vel_factor": self.config.max_vel_factor,
                    "max_acc_factor": self.config.max_acc_factor,
                    "planning_time": self.config.planning_time,
                    "min_cartesian_pct": self.config.min_cartesian_pct,
                    "ignore_error": self.config.ignore_error,
                    "planner": self.config.planner,
                    "cartesian_planner": self.config.cartesian_planner,
                    "allow_top_grasps": self.config.allow_top_grasps,
                    "allow_bottom_grasps": self.config.allow_bottom_grasps,
                    "allow_side_grasps": self.config.allow_side_grasps,
                    "remove_aligned_short_bb_face": self.config.remove_aligned_short_bb_face,
                    "pregrasped": self.config.pregrasped,
                    "adapt_to_contact": self.config.adapt_to_contact,
                }
            ),
            PlaceObjectConfig(
                config_dict={
                    "name": "place",
                    "target_object": self.config.target_object,
                    "place_position": self.config.place_position,
                    "place_position_ranges": self.config.place_position_ranges,
                    "place_orientation": self.config.place_orientation,
                    "place_orientation_choices": self.config.place_orientation_choices,
                    "place_sample_axes": self.config.place_sample_axes,
                    "place_sample_angle_bounds": self.config.place_sample_angle_bounds,
                    "place_approach_height": self.config.place_approach_height,
                    "max_plan_attempts": self.config.max_plan_attempts,
                    "max_vel_factor": self.config.max_vel_factor,
                    "max_acc_factor": self.config.max_acc_factor,
                    "planning_time": self.config.planning_time,
                    "ignore_error": self.config.ignore_error,
                    "planner": self.config.planner,
                    "cartesian_planner": self.config.cartesian_planner,
                    "min_cartesian_pct": self.config.min_cartesian_pct,
                    "release": self.config.release,
                    "use_approach": self.config.use_approach,
                    "adapt_to_contact": self.config.adapt_to_contact,
                    "n_candidates": self.config.n_candidates,
                }
            ),
        ]

        self.init_behaviors(behavior_configs, robot, env_config, sim, device, open_loop, rospy_log)

    def set_place_pose(self, pose):
        """
        Sets the target pose for placing the object.

        Args:
            pose (list-like): Pose as a 7D vector (3D position and quaternion)
        """
        self.behaviors["place"].set_place_pose(pose)

    def set_place_position(self, pos):
        """
        Sets the target position for placing the object.

        Args:
            pos (list-like): Position as 3D vector
        """
        self.behaviors["place"].set_place_position(pos)

    def set_place_orientation(self, quat):
        """
        Sets the target orientation for placing the object.

        Args:
            quat (list-like): Orientation as a quaternion (4D vector)
        """
        self.behaviors["place"].set_place_orientation(quat)

    def get_behavior_params(self):
        self._behavior_params["target_object"] = self.config.target_object
        return super().get_behavior_params()
