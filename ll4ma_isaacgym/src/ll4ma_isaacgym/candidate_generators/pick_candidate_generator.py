import rospy
import numpy as np
from dataclasses import dataclass
from typing import List

from ll4ma_isaacgym.candidate_generators import CandidateGenerator, BBPoseCandidateGenerator

from moveit_interface import util as moveit_util


@dataclass
class PickCandidate:
    init_obj_pose: List[float] = None
    target_obj_pose: List[float] = None
    pick_ee_pose: List[float] = None
    pick_joints: List[float] = None
    lift_ee_pose: List[float] = None
    lift_joints: List[float] = None


class PickCandidateGenerator(CandidateGenerator):
    def __init__(self):
        self.bb_candidate_generator = BBPoseCandidateGenerator()

    def get_candidates(
        self,
        init_obj_pose,
        config,
        ee_link,
        offset=0.01,
        mesh_filename="",
        prim_type="",
        prim_dims=None,
        remove_bottom_poses=True,
        remove_top_poses=False,
        remove_side_poses=False,
        remove_aligned_short_bb_face=False,
        filter_unreachable=True,
    ):
        candidates = []
        pick_candidates = self.bb_candidate_generator.get_candidates(
            init_obj_pose,
            ee_link,
            offset,
            mesh_filename,
            prim_type,
            prim_dims,
            remove_bottom_poses,
            remove_top_poses,
            remove_side_poses,
            remove_aligned_short_bb_face,
            filter_unreachable=False,  # We'll filter together with lift poses later
        )
        pick_candidates = [c.get_pose() for c in pick_candidates]

        for pick_candidate in pick_candidates:
            # TODO you could generate a bunch of lift heights for each candidate,
            # currently only doing one
            lift_offset = config.lift_offset
            if config.lift_offset_ranges is not None:
                for i, range_ in enumerate(config.lift_offset_ranges):
                    if range_ is not None:
                        lift_offset[i] = np.random.uniform(*range_)
            if config.lift_offset_choices is not None:
                for i, choices in enumerate(config.lift_offset_choices):
                    if choices is not None:
                        lift_offset[i] = np.random.choice(choices)

            candidate = PickCandidate()
            candidate.pick_ee_pose = pick_candidate.tolist()
            candidate.lift_ee_pose = pick_candidate.copy()
            candidate.lift_ee_pose[:3] += lift_offset
            candidate.lift_ee_pose = candidate.lift_ee_pose.tolist()
            candidate.init_obj_pose = init_obj_pose.tolist()
            candidate.target_obj_pose = init_obj_pose.copy()
            candidate.target_obj_pose[:3] += lift_offset
            candidate.target_obj_pose = candidate.target_obj_pose.tolist()
            if filter_unreachable and not rospy.is_shutdown():
                # Don't try to plan to kinematically unreachable poses
                ik_resp, success = moveit_util.get_ik(
                    [candidate.pick_ee_pose, candidate.lift_ee_pose], ee_link, 5
                )
                if not success or ik_resp is None:
                    rospy.logerr("Failed to call GetIK service, check if it's running")
                if not success or ik_resp is None or not all(ik_resp.solution_found):
                    # rospy.logwarn(
                    #     "GetIK service found no solution for candidates:" +
                    #     str(candidate.tolist())
                    # )
                    candidate = None
                else:
                    # print('Successful solutions:')
                    # print(candidate.pick_ee_pose)
                    # print(candidate.lift_ee_pose)
                    candidate.pick_joints = list(ik_resp.solutions[0].position)
                    candidate.lift_joints = list(ik_resp.solutions[1].position)
                    candidates.append(candidate)
            else:
                candidates.append(candidate)

        return candidates
