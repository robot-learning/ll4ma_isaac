class CandidateGenerator:
    def get_candidates(self, *args, **kwargs):
        """
        Generates skill candidates.
        """
        raise NotImplementedError("Extending classes must implement this function")
