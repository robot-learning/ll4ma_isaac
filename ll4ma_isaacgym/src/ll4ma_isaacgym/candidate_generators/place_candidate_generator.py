from isaacgym import gymapi

import rospy
import numpy as np
import time
import random
from dataclasses import dataclass
from typing import List

from ll4ma_isaacgym.candidate_generators import CandidateGenerator

from ll4ma_util import math_util
from moveit_interface import util as moveit_util

from geometry_msgs.msg import PoseStamped


@dataclass
class PlaceCandidate:
    init_obj_pose: List[float] = None
    target_obj_pose: List[float] = None
    above_ee_pose: List[float] = None
    above_joints: List[float] = None
    place_ee_pose: List[float] = None
    place_joints: List[float] = None


class PlaceCandidateGenerator(CandidateGenerator):
    def __init__(self):
        self.pub_above = rospy.Publisher("/placecandidate/above", PoseStamped, queue_size=10)
        self.pub_placed = rospy.Publisher("/placecandidate/placed", PoseStamped, queue_size=10)

    def get_candidates(
        self,
        init_obj_pose,
        init_ee_pose,
        config,
        ee_link,
        target_obj_pose=None,
        filter_unreachable=True,
        timeout=5.0,
    ):
        candidates = []
        start = time.time()
        no_target = target_obj_pose is None
        if not no_target and len(target_obj_pose) == 7:
            candidates = [
                self._get_candidate(
                    init_obj_pose,
                    init_ee_pose,
                    config.place_approach_height,
                    ee_link,
                    target_obj_pose,
                    filter_unreachable,
                    timeout,
                )
            ]
        else:
            if not no_target:
                target_obj_pose = np.concatenate([target_obj_pose[:3], np.zeros(4)])
            while len(candidates) < config.n_candidates and time.time() - start < timeout:
                if no_target:
                    target_obj_pose = init_obj_pose.copy()
                    # Set target position (randomized if ranges are provided)
                    for i, pos_i in enumerate(config.place_position):
                        if pos_i is not None:
                            # Set target as defined if given, otherwise will default to init
                            target_obj_pose[i] = pos_i
                        if config.place_position_ranges[i] is not None:
                            # Randomize this dimension if range is provided
                            target_obj_pose[i] = np.random.uniform(*config.place_position_ranges[i])
                target_obj_pose[3:] = self._get_random_orientation(config)

                candidate = self._get_candidate(
                    init_obj_pose,
                    init_ee_pose,
                    config.place_approach_height,
                    ee_link,
                    target_obj_pose,
                    filter_unreachable,
                    timeout,
                )
                if candidate is not None:
                    # Can add more conditions here if you want to filter candidates further
                    candidates.append(candidate)
        if time.time() - start >= timeout and len(candidates) == 0:
            rospy.logwarn(f"No place skill candidates found after {timeout} secs.")

        return candidates

    def _get_random_orientation(self, config):
        ori = np.zeros(4)
        ori[-1] = 1

        # Set target orientation (specific target if specified, randomized if configured)
        if config.place_sample_axes is not None and config.place_sample_angle_bounds is not None:
            if len(config.place_sample_axes) != len(config.place_sample_angle_bounds):
                raise ValueError(
                    "Expected sample_axes and sample_angle_bounds to "
                    "have the same size but got "
                    f"{len(config.placesample_axes)} and "
                    f"{len(config.place_sample_angle_bounds)} respectively"
                )
            quat = ori.copy()
            for sample_axis, bounds in zip(
                config.place_sample_axes, config.place_sample_angle_bounds
            ):
                if len(sample_axis) != 3:
                    raise ValueError(
                        f"Expected sample_axis to have length 3 but got {len(sample_axis)}"
                    )
                if len(bounds) != 2:
                    raise ValueError(f"Expected bounds to have length 2 but got {len(bounds)}")
                axis = gymapi.Vec3(*sample_axis)
                angle = np.random.uniform(*bounds)
                rand_quat = gymapi.Quat.from_axis_angle(axis, angle)
                q2 = np.array([rand_quat.x, rand_quat.y, rand_quat.z, rand_quat.w])
                # Rotate by quat, then by q2:
                quat = math_util.quat_mul(q2, quat)
            ori = quat
        elif config.place_orientation_choices is not None:
            ori = np.array(random.choice(config.place_orientation_choices))
        elif config.place_orientation is not None:
            ori = np.array(config.place_orientation)
        return ori

    def _get_candidate(
        self,
        init_obj_pose,
        init_ee_pose,
        place_approach_height,
        ee_link,
        target_obj_pose,
        filter_unreachable=True,
        timeout=2.0,
    ):
        # Variables here reflect Hollerbach's TF notation [ref-frame]_T_[child-frame]
        w_T_target_obj = math_util.pose_to_homogeneous(target_obj_pose[:3], target_obj_pose[3:])
        w_T_ee = math_util.pose_to_homogeneous(init_ee_pose[:3], init_ee_pose[3:])
        w_T_obj = math_util.pose_to_homogeneous(init_obj_pose[:3], init_obj_pose[3:])
        obj_T_w = math_util.homogeneous_inverse(w_T_obj)
        obj_T_ee = obj_T_w @ w_T_ee
        w_T_target_ee = w_T_target_obj @ obj_T_ee
        p, q = math_util.homogeneous_to_pose(w_T_target_ee)
        place_ee_pose = np.concatenate([p, q])

        above_ee_pose = place_ee_pose.copy()
        # TODO want to add sample options and arbitrary x,y,z offset for this
        above_ee_pose[2] += place_approach_height

        pose = PoseStamped()
        pose.header.frame_id = "world"
        pose.pose.position.x = above_ee_pose[0]
        pose.pose.position.y = above_ee_pose[1]
        pose.pose.position.z = above_ee_pose[2]
        pose.pose.orientation.x = above_ee_pose[3]
        pose.pose.orientation.y = above_ee_pose[4]
        pose.pose.orientation.z = above_ee_pose[5]
        pose.pose.orientation.w = above_ee_pose[6]
        self.pub_above.publish(pose)

        pose = PoseStamped()
        pose.header.frame_id = "world"
        pose.pose.position.x = place_ee_pose[0]
        pose.pose.position.y = place_ee_pose[1]
        pose.pose.position.z = place_ee_pose[2]
        pose.pose.orientation.x = place_ee_pose[3]
        pose.pose.orientation.y = place_ee_pose[4]
        pose.pose.orientation.z = place_ee_pose[5]
        pose.pose.orientation.w = place_ee_pose[6]
        self.pub_placed.publish(pose)

        candidate = PlaceCandidate()
        candidate.above_ee_pose = above_ee_pose.tolist()
        candidate.place_ee_pose = place_ee_pose.tolist()
        candidate.init_obj_pose = init_obj_pose.tolist()
        candidate.target_obj_pose = target_obj_pose.tolist()

        if filter_unreachable and not rospy.is_shutdown():
            # Don't try to plan to kinematically unreachable poses
            ik_resp, success = moveit_util.get_ik([above_ee_pose, place_ee_pose], ee_link, 5)
            if not success or ik_resp is None or not all(ik_resp.solution_found):
                if not all(ik_resp.solution_found):
                    rospy.logwarn("GetIK service found no solution")
                    # print('Solutions:', ik_resp.solution_found)
                    # print('above_ee_pose:', above_ee_pose)
                    # print('place_ee_pose:', place_ee_pose)
                    # import pdb; pdb.set_trace()
                else:
                    rospy.logerr("Failed to call GetIK service, check if it's running")
                candidate = None
            else:
                candidate.above_joints = list(ik_resp.solutions[0].position)
                candidate.place_joints = list(ik_resp.solutions[1].position)
        return candidate
