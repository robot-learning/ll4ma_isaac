import rospy
from ll4ma_isaacgym.candidate_generators import CandidateGenerator
from ll4ma_util import manip_util
from moveit_interface import util as moveit_util


class BBPoseCandidateGenerator(CandidateGenerator):
    def get_candidates(
        self,
        obj_pose,
        ee_link,
        offset=0.01,
        mesh_filename="",
        prim_type="",
        prim_dims=None,
        remove_bottom_poses=True,
        remove_top_poses=False,
        remove_side_poses=False,
        remove_aligned_short_bb_face=False,
        filter_unreachable=True,
    ):
        """
        Generates axis-aligned pose candidates offset from the bounding box of an
        object. These are typically useful for approach poses for pre-grasp.
        """
        if mesh_filename:
            candidates = manip_util.get_bb_pose_candidates(
                obj_pose, mesh_filename=mesh_filename, offset=offset
            )
        elif prim_type and prim_dims is not None:
            candidates = manip_util.get_bb_pose_candidates(
                obj_pose, prim_type=prim_type, prim_dims=prim_dims, offset=offset
            )
        else:
            raise ValueError(
                "Must specify mesh_filename, or both prim_type and prim_dims"
                "for generating pose candidates"
            )
        # Post-process the poses to filter out unwanted ones
        if remove_bottom_poses:
            candidates = manip_util.remove_bottom_pose_candidates(candidates)
        if remove_top_poses:
            candidates = manip_util.remove_top_pose_candidates(candidates)
        if remove_side_poses:
            candidates = manip_util.remove_side_pose_candidates(candidates)
        if remove_aligned_short_bb_face:
            candidates = manip_util.remove_aligned_short_bb_face(candidates)
        # Remove any that are kinematically unreachabe
        if filter_unreachable and not rospy.is_shutdown():
            candidates = [c.get_pose() for c in candidates]
            ikresp, _ = moveit_util.get_ik(candidates, ee_link, 5)
            candidates = [c for i, c in enumerate(candidates) if ikresp.solution_found[i]]
        return candidates
