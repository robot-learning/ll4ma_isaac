import time
import rospy
import random
import numpy as np
from dataclasses import dataclass
from typing import List

from ll4ma_isaacgym.candidate_generators import CandidateGenerator

from ll4ma_util import math_util
from moveit_interface import util as moveit_util


@dataclass
class PushCandidate:
    prepush_pose: List[float] = None  # EE pose at start of push (3D pos, 4D quat)
    prepush_joints: List[float] = None  # Joint configuration from IK solving prepush_pose
    end_pose: List[float] = None  # EE pose at end of push (3D pos, 4D quat)
    end_joints: List[float] = None  # Joint configuration from IK solving end_pose
    init_obj_pose: List[float] = None  # Initial object pose (3D pos, 4D quat)
    target_obj_pose: List[float] = None  # Target object pose (3D pos, 4D quat)
    push_distance: float = None  # Distance object is pushed
    push_direction: List[float] = None  # Planar xy direction object is pushed (unit vector)
    push_height_offset: float = None  # Offset in z (vertical height) from object origin


class PushCandidateGenerator(CandidateGenerator):
    def get_candidates(
        self,
        init_obj_pose,
        config,
        ee_link,
        target_obj_pose=None,
        filter_unreachable=True,
        timeout=2.0,
    ):
        """
        Retrieves pose candidates for the push skill (prepush pose and terminal pose
        for end-effector).

        Args:
            init_obj_pose (ndarray): Initial pose of the object
            config (PushObjectConfig): Config for the push behavior
            ee_link (str): Name of the end-effector link
            target_obj_pose (ndarray): Optional target object pose, this is used if you already
                                       have a fixed desired object pose (i.e. not randomized)
            filter_unreachable (bool): Filter out kinematically unreachable poses if True
            timeout (float): Max time to try getting candidates (can timout if you're
                             trying to push something outside the reachable workspace)
        Returns:
            candidates (List[PushPoseCandidate]) List of push candidates
        """
        candidates = []
        start = time.time()

        if target_obj_pose is not None:
            candidates = [
                self._get_candidate(
                    init_obj_pose, config, ee_link, target_obj_pose, filter_unreachable, timeout
                )
            ]
        else:
            while len(candidates) < config.n_candidates and time.time() - start < timeout:
                target_obj_pose = init_obj_pose.copy()
                for i, pos_i in enumerate(config.target_position):
                    if pos_i is not None:
                        # Set target as defined if given, otherwise will default to init
                        target_obj_pose[i] = pos_i
                    if config.target_position_ranges[i] is not None:
                        # Randomize this dimension if range is provided
                        target_obj_pose[i] = np.random.uniform(*config.target_position_ranges[i])
                    # TODO can also set target orientations

                candidate = self._get_candidate(
                    init_obj_pose, config, ee_link, target_obj_pose, filter_unreachable, timeout
                )
                if candidate is not None:
                    # Can add more conditions here if you want to filter candidates further
                    if candidate.push_distance >= config.min_push_distance:
                        candidates.append(candidate)

        if time.time() - start >= timeout and len(candidates) == 0:
            rospy.logwarn(
                f"No push skill candidates found after {timeout} secs. "
                "The object is probably outside the reachable workspace "
                "for the push skill."
            )

        return candidates

    def _get_candidate(
        self, init_obj_pose, config, ee_link, target_obj_pose, filter_unreachable=True, timeout=2.0
    ):
        # TODO for now doing planar push assuming z-axis points up in world frame
        diff = target_obj_pose[:2] - init_obj_pose[:2]  # Planar in x-y
        diff = np.append(diff, 0.0)  # No change in z-direction
        push_distance = np.linalg.norm(diff)
        push_direction = diff / push_distance

        # TODO offset from object (away from centroid) should come from object
        # geometry and EE config (e.g. once you figure the push direction, use
        # object pose and geometry to find closest point on surface or on
        # bounding box, then take a small additional offset from that based on
        # robot EE config
        obj_offset = config.obj_offset_dist * -push_direction

        # Set push height offset based on behavior config
        if config.push_height_offset_range:
            push_height_offset = np.random.uniform(*config.push_height_offset_range)
        elif config.push_height_offset_choices:
            push_height_offset = random.choice(config.push_height_offset_choices)
        else:
            push_height_offset = config.push_height_offset
        obj_offset[2] = push_height_offset

        # Compute EE pose to align with push dist
        # TODO assumes a lot about robot EE frame, want to make dependent on EE config.
        # This aligns EE x-axis to push direction, EE-y axis to world z-axis.
        R = math_util.construct_rotation_matrix(
            x=push_direction, y=np.array([0, 0, 1]), normalize=False
        )
        q = math_util.rotation_to_quat(R)

        # Prepush pose is where EE should be before executing straight-line push
        prepush_pose = np.concatenate([init_obj_pose[:3] + obj_offset, q])
        # End pose is prepush pose, plus diff between target and initial obj poses,
        # plus the distance the EE is offset from the obj initially
        end_pose = prepush_pose.copy()
        end_pose[:2] += diff[:2] - obj_offset[:2]  # Minus since it was in neg push direction

        # Returning all this info since it will be logged in behavior params
        candidate = PushCandidate()
        candidate.prepush_pose = prepush_pose.tolist()
        candidate.end_pose = end_pose.tolist()
        candidate.init_obj_pose = init_obj_pose.tolist()
        candidate.target_obj_pose = target_obj_pose.tolist()
        candidate.push_distance = push_distance
        candidate.push_direction = push_direction.tolist()
        candidate.push_height_offset = push_height_offset

        if filter_unreachable and not rospy.is_shutdown():
            # Don't try to plan to kinematically unreachable poses
            ik_resp, success = moveit_util.get_ik([prepush_pose, end_pose], ee_link, 5)
            if not success or ik_resp is None:
                rospy.logerr("Failed to call GetIK service, check if it's running")
            if not success or ik_resp is None or not all(ik_resp.solution_found):
                candidate = None
            else:
                candidate.prepush_joints = list(ik_resp.solutions[0].position)
                candidate.end_joints = list(ik_resp.solutions[1].position)

        return candidate
