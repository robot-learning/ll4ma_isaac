from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup


setup_args = generate_distutils_setup(
    packages=[
        "ll4ma_isaacgym",
        "ll4ma_isaacgym.behaviors",
        "ll4ma_isaacgym.common",
        "ll4ma_isaacgym.oracles",
        "ll4ma_isaacgym.robots",
        "ll4ma_isaacgym.tasks",
        "ll4ma_isaacgym.visualization",
    ],
    package_dir={"": "src"},
)

setup(**setup_args)
