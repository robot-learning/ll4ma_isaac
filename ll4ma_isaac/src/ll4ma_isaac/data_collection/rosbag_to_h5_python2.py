#!/usr/bin/env python
from __future__ import print_function

import os
import sys
import h5py
import cv2
import argparse
import numpy as np
from tqdm import tqdm
from glob import glob

import rosbag

from ll4ma_isaac.util import ros_util
from ll4ma_isaac.environments.common import Status


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--rosbag_dir", type=str, required=True, help="Absolute path to directory of rosbags"
    )
    parser.add_argument(
        "--h5_dir",
        type=str,
        help="Absolute path where H5 files will be saved (defaults to rosbag_dir/h5)",
    )
    parser.add_argument(
        "--img_size", type=int, default=128, help="Desired height/width dimension of images"
    )
    args = parser.parse_args()

    if not os.path.isdir(args.rosbag_dir):
        print("\nPath does not exist: {}".format(args.rosbag_dir))
        sys.exit(1)

    bag_filenames = [f for f in glob(os.path.join(args.rosbag_dir, "*")) if f.endswith(".bag")]
    if not bag_filenames:
        print("\nNo bag files in directory: {}".format(args.rosbag_dir))
        sys.exit(1)
    bag_filenames = sorted(bag_filenames)

    h5_dir = args.h5_dir if args.h5_dir else os.path.join(args.rosbag_dir, "h5")
    if not os.path.exists(h5_dir):
        os.makedirs(h5_dir)

    print("\nConverting bag files to H5 files...")
    for bag_filename in tqdm(bag_filenames):
        try:
            h5_filename = os.path.basename(bag_filename).replace(".bag", ".h5")
            if h5_filename in os.listdir(h5_dir):
                continue
            h5_filename = os.path.join(h5_dir, h5_filename)

            h5_file = h5py.File(h5_filename, "w")
            msgs = list(rosbag.Bag(bag_filename).read_messages())
            n_msgs = len(msgs)
            test_msg = msgs[0][1]  # One msg to get sizes/names for pre-allocation

            # Find the task goal
            for _, msg, _ in msgs:
                if not msg.task_goal:
                    continue
                h5_file.attrs["task_goal"] = msg.task_goal
                break
            # Figure out if it was task success or failure
            h5_file.attrs["task_execution_result"] = "UNKNOWN"
            for _, msg, _ in reversed(msgs):
                if msg.status == Status.TASK_SUCCESS.name or msg.status == Status.TASK_FAILURE.name:
                    h5_file.attrs["task_execution_result"] = msg.status
                    break

            joint_names = test_msg.joint_state.name
            finger_idxs = [i for i, n in enumerate(joint_names) if "finger" in n]
            arm_idxs = [i for i, n in enumerate(joint_names) if "finger" not in n]
            finger_joint_names = np.array([n for n in joint_names if "finger" in n], dtype="S")
            arm_joint_names = np.array([n for n in joint_names if "finger" not in n], dtype="S")

            rgb = np.zeros((n_msgs, args.img_size, args.img_size, 3), dtype=np.uint8)
            depth = np.zeros((n_msgs, args.img_size, args.img_size), dtype=np.float32)
            instance_seg = np.zeros((n_msgs, args.img_size, args.img_size), dtype=np.uint8)
            semantic_seg = np.zeros((n_msgs, args.img_size, args.img_size), dtype=np.uint8)
            time = np.zeros((n_msgs, 1))
            dt = np.zeros((n_msgs, 1))
            joint_positions = np.zeros((n_msgs, len(arm_joint_names)))
            joint_velocities = np.zeros((n_msgs, len(arm_joint_names)))
            gripper_joint_positions = np.zeros((n_msgs, len(finger_joint_names)))
            gripper_joint_velocities = np.zeros((n_msgs, len(finger_joint_names)))
            tfs = {
                tf.child_frame_id: {
                    "position": np.zeros((n_msgs, 3)),
                    "orientation": np.zeros((n_msgs, 4)),
                }
                for tf in test_msg.tf.transforms
            }
            tf_parents = {tf.child_frame_id: tf.header.frame_id for tf in test_msg.tf.transforms}
            status = []
            semantic_labels = np.array(test_msg.semantic_labels, dtype="S")

            # TODO Ids are screwed up on save

            semantic_ids = np.array(test_msg.semantic_ids)
            instance_labels = np.array(test_msg.instance_labels, dtype="S")
            instance_ids = np.array(test_msg.instance_ids)
            objects = {
                obj.name: {
                    "prim_path": obj.prim_path,
                    "body_type": obj.body_type,
                    "position": np.array(
                        [obj.pose.position.x, obj.pose.position.y, obj.pose.position.z]
                    ),
                    "orientation": np.array(
                        [
                            obj.pose.orientation.x,
                            obj.pose.orientation.y,
                            obj.pose.orientation.z,
                            obj.pose.orientation.w,
                        ]
                    ),
                    "length": obj.length,
                    "width": obj.width,
                    "height": obj.height,
                    "mass": obj.mass,
                    "semantic_label": obj.semantic_label,
                    "units": obj.units,
                    "color": np.array([obj.color.r, obj.color.g, obj.color.b]),
                }
                for obj in test_msg.objects
            }
            ros_cam_info = {
                "frame_id": test_msg.ros_camera_info.header.frame_id,
                "height": test_msg.ros_camera_info.height,
                "width": test_msg.ros_camera_info.width,
                "distortion_model": test_msg.ros_camera_info.distortion_model,
                "D": np.array(test_msg.ros_camera_info.D),
                "K": np.array(test_msg.ros_camera_info.K),
                "R": np.array(test_msg.ros_camera_info.R),
                "P": np.array(test_msg.ros_camera_info.P),
                "binning_x": test_msg.ros_camera_info.binning_x,
                "binning_y": test_msg.ros_camera_info.binning_y,
            }
            isaac_cam_info = {
                "clipping_range_low": test_msg.isaac_camera_info.clipping_range_low,
                "clipping_range_high": test_msg.isaac_camera_info.clipping_range_high,
                "focal_length": test_msg.isaac_camera_info.focal_length,
                "fov": test_msg.isaac_camera_info.fov,
                "horizontal_aperture": test_msg.isaac_camera_info.horizontal_aperture,
                "pose": np.array(test_msg.isaac_camera_info.pose),
                "projection_matrix": np.array(test_msg.isaac_camera_info.projection_matrix),
                "resolution_height": test_msg.isaac_camera_info.resolution_height,
                "resolution_width": test_msg.isaac_camera_info.resolution_width,
            }

            for i, (topic, msg, t) in enumerate(msgs):
                # RGB
                rgb_i = ros_util.get_rgb_img(msg.rgb)
                rgb_i = cv2.resize(
                    rgb_i, (args.img_size, args.img_size), interpolation=cv2.INTER_LINEAR
                )
                rgb[i] = rgb_i
                # Depth
                depth_i = ros_util.get_img(msg.depth)
                depth_i = cv2.resize(
                    depth_i, (args.img_size, args.img_size), interpolation=cv2.INTER_LINEAR
                )
                depth[i] = depth_i
                # Segmentations
                instance_seg_i = ros_util.get_img(msg.instance_segmentation)
                instance_seg_i = cv2.resize(
                    instance_seg_i, (args.img_size, args.img_size), interpolation=cv2.INTER_NEAREST
                ).astype(np.uint8)
                instance_seg[i] = instance_seg_i
                semantic_seg_i = ros_util.get_img(msg.semantic_segmentation)
                semantic_seg_i = cv2.resize(
                    semantic_seg_i, (args.img_size, args.img_size), interpolation=cv2.INTER_NEAREST
                ).astype(np.uint8)
                semantic_seg[i] = semantic_seg_i
                # Joint state
                joint_positions[i] = np.take(msg.joint_state.position, arm_idxs)
                joint_velocities[i] = np.take(msg.joint_state.velocity, arm_idxs)
                gripper_joint_positions[i] = np.take(msg.joint_state.position, finger_idxs)
                gripper_joint_velocities[i] = np.take(msg.joint_state.velocity, finger_idxs)
                # TF
                for tf in msg.tf.transforms:
                    tfs[tf.child_frame_id]["position"][i] = np.array(
                        [
                            tf.transform.translation.x,
                            tf.transform.translation.y,
                            tf.transform.translation.z,
                        ]
                    )
                    tfs[tf.child_frame_id]["orientation"][i] = np.array(
                        [
                            tf.transform.rotation.x,
                            tf.transform.rotation.y,
                            tf.transform.rotation.z,
                            tf.transform.rotation.w,
                        ]
                    )
                # Remaining data
                time[i] = msg.sim_time
                dt[i] = msg.sim_dt
                status.append(msg.status)

            h5_file["rgb"] = rgb
            h5_file["depth"] = depth
            h5_file["semantic_segmentation"] = semantic_seg
            h5_file["semantic_segmentation"].attrs["ids"] = semantic_ids
            h5_file["semantic_segmentation"].attrs["labels"] = semantic_labels
            h5_file["instance_segmentation"] = instance_seg
            h5_file["instance_segmentation"].attrs["ids"] = instance_ids
            h5_file["instance_segmentation"].attrs["labels"] = instance_labels
            h5_file["joint_positions"] = joint_positions
            h5_file["joint_positions"].attrs["joint_names"] = arm_joint_names
            h5_file["joint_velocities"] = joint_velocities
            h5_file["joint_velocities"].attrs["joint_names"] = arm_joint_names
            h5_file["gripper_joint_positions"] = gripper_joint_positions
            h5_file["gripper_joint_positions"].attrs["joint_names"] = finger_joint_names
            h5_file["gripper_joint_velocities"] = gripper_joint_velocities
            h5_file["gripper_joint_velocities"].attrs["joint_names"] = finger_joint_names
            for obj_id, obj_data in tfs.items():
                h5_file.create_group("tf/{}".format(obj_id))
                h5_file["tf"][obj_id]["position"] = obj_data["position"]
                h5_file["tf"][obj_id]["orientation"] = obj_data["orientation"]
                h5_file["tf"][obj_id].attrs["parent_frame"] = tf_parents[obj_id]
            h5_file["time"] = time
            h5_file["dt"] = dt
            h5_file["status"] = np.array(status, dtype="S")
            for obj_id, obj_data in objects.items():
                h5_file.create_group("objects/{}".format(obj_id))
                for key, value in obj_data.items():
                    h5_file["objects"][obj_id].attrs[key] = value
            h5_file.create_group("camera_info_ros")
            for key, value in ros_cam_info.items():
                h5_file["camera_info_ros"].attrs[key] = value
            h5_file.create_group("camera_info_isaac")
            for key, value in isaac_cam_info.items():
                h5_file["camera_info_isaac"].attrs[key] = value

            h5_file.close()
        except Exception as e:
            # print("Could not process {}".format(bag_filename))
            print(e)
