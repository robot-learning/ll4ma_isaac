#!/usr/bin/env python
import os
import sys
import yaml
import h5py

import rospy
import rospkg

from ll4ma_isaac.srv import CreateScene, CreateSceneRequest
from ll4ma_isaac.msg import Object

rospack = rospkg.RosPack()


DEFAULT_CONFIG_PATH = os.path.join(rospack.get_path("ll4ma_isaac"), "config", "scenes")


def create_scene_from_config(config_path):
    with open(config_path, "r") as f:
        config = yaml.load(f, Loader=yaml.FullLoader)
    task_name = config["task_name"]
    objects = config["objects"]
    create_scene(task_name, objects)


def create_scene_from_h5(h5_filename):
    with h5py.File(h5_filename, "r") as h5_file:
        task_name = h5_file.attrs["task_name"]
        objects = {}
        # Converte from H5 AttributeManager
        for k1, v1 in h5_file["objects"].items():
            objects[k1] = {}
            for k2, v2 in v1.attrs.items():
                objects[k1][k2] = v2
    create_scene(task_name, objects)


def create_scene(task_name, objects):
    req = CreateSceneRequest()
    req.task_name = task_name
    for obj_id, obj_data in objects.items():
        obj = Object()
        obj.name = obj_id
        obj.body_type = obj_data["body_type"]
        obj.semantic_label = obj_data["semantic_label"]
        obj.length = obj_data["length"]
        obj.width = obj_data["width"]
        obj.height = obj_data["height"]
        obj.units = obj_data["units"]
        obj.mass = obj_data["mass"]
        obj.color.r = obj_data["color"][0]
        obj.color.g = obj_data["color"][1]
        obj.color.a = obj_data["color"][2]
        # Assuming full pose will always be given (allows for sum check on quat in creation)
        if "position" in obj_data and "orientation" in obj_data:
            obj.pose.position.x = obj_data["position"][0]
            obj.pose.position.y = obj_data["position"][1]
            obj.pose.position.z = obj_data["position"][2]
            obj.pose.orientation.x = obj_data["orientation"][0]
            obj.pose.orientation.y = obj_data["orientation"][1]
            obj.pose.orientation.z = obj_data["orientation"][2]
            obj.pose.orientation.w = obj_data["orientation"][3]
        req.objects.append(obj)
    _create_scene = rospy.ServiceProxy("/isaac/create_scene", CreateScene)
    try:
        _create_scene(req)
    except rospy.ServiceException as e:
        rospy.logerr(f"Service call to create scene failed: {e}")


if __name__ == "__main__":
    rospy.init_node("isaac_create_scene")

    config_dir = rospy.get_param("~config_dir", DEFAULT_CONFIG_PATH)
    config_filename = rospy.get_param("~config_filename", "")
    h5_filename = rospy.get_param("~h5_filename", "")
    if config_filename:
        config_path = os.path.join(config_dir, config_filename)
        if not os.path.exists(config_path):
            rospy.logerr(f"Scene configuration file does not exist: {config_path}")
            sys.exit(1)
        create_scene_from_config(config_path)
    elif h5_filename:
        if not os.path.exists(h5_filename):
            rospy.logerr(f"H5 file does not exist: {h5_filename}")
            sys.exit(1)
        create_scene_from_h5(h5_filename)
    else:
        rospy.logerr("Must specify one of config_filename or h5_filename")
