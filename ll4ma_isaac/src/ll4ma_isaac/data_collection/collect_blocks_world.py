#!/usr/bin/env python3
import rospy
from std_msgs.msg import String
from std_srvs.srv import Trigger, TriggerRequest
from ll4ma_rosbag_utils.srv import (
    TopicAction,
    TopicActionRequest,
    RosbagAction,
    RosbagActionRequest,
)

import os
import sys
from tqdm import tqdm
from time import time

from ll4ma_isaac.environments.common import Status


class BlocksWorldCollector:
    def __init__(self):
        status_topic = rospy.get_param("~status_topic")
        self.n_demos = rospy.get_param("~n_demos")
        self.save_dir = rospy.get_param("~save_dir")
        self.timeout = rospy.get_param("~timeout", 30)
        self.max_attempts = rospy.get_param("~max_attempts", 5)

        self.rate = rospy.Rate(100)
        rospy.Subscriber(status_topic, String, self._status_cb)

        rospy.loginfo("Waiting for services...")
        rospy.wait_for_service("/isaac/perform_task")
        rospy.wait_for_service("/isaac/reset_task")
        rospy.wait_for_service("/rosbag/register_topics")
        rospy.loginfo("Services are ready.")

        self._register_rosbag_topics(["/isaac/data_log"])

        self.sim_status = None

        os.makedirs(self.save_dir, exist_ok=True)

    def collect_data(self):
        n_bags = len([f for f in os.listdir(self.save_dir) if f.endswith(".bag")])
        if n_bags >= self.n_demos:
            rospy.logwarn(
                f"Requested {self.n_demos} demos and {n_bags} already recorded "
                "in {self.save_dir}. No need to record anymore."
            )
            return

        rospy.loginfo("Waiting for simulator to be ready...")
        if not self._wait_for_status(Status.INITIALIZED):
            rospy.logerr("Simulation was not initialized. Scene creation is not working.")
            return
        self._reset_task()
        if not self._wait_for_status(Status.READY):
            rospy.logerr("Simulation is not ready. Something is wrong.")
            return
        rospy.loginfo("Simulator is ready.")

        rospy.loginfo(f"Collecting {self.n_demos} demos...")

        for i in tqdm(range(n_bags + 1, self.n_demos + 1), desc="Data Collection"):
            recorded_demo = False
            attempts = 0
            rosbag_filename = f"isaac_blocks_world_{i:04d}"

            # TODO need to add function that will back out recorded data on failure, i.e.
            # stop recording and delete whatever bag file just was created. I think that
            # might already be handled in rosbag utils pkg.
            while not recorded_demo and attempts < self.max_attempts:
                # Wait until task scene is ready to be manipulated
                if not self._wait_for_status(Status.READY):
                    self._reset_task()
                    attempts += 1
                    continue
                self._start_rosbag_record(rosbag_filename)
                self._start_task()
                # Wait for task to actually start executing
                if not self._wait_for_status(Status.EXECUTING_TASK):
                    self._stop_rosbag_record()
                    self._delete_rosbag(f"{rosbag_filename}.bag")
                    self._reset_task()
                    attempts += 1
                    continue
                # Wait for task execution to finish (should be either success or failure)
                if not self._wait_for_different_status(Status.EXECUTING_TASK, timeout=40):
                    self._stop_rosbag_record()
                    self._delete_rosbag(f"{rosbag_filename}.bag")
                    self._reset_task()
                    attempts += 1
                    continue
                self._stop_rosbag_record()
                self._reset_task()
                recorded_demo = True
            if attempts > self.max_attempts:
                rospy.logerr("Exceeded max number of attempts. Something is probably wrong in sim.")
                sys.exit(1)
        saved_bag_files = [f for f in os.listdir(self.save_dir) if f.endswith(".bag")]
        rospy.loginfo(f"Collected {len(saved_bag_files)} demos successfully.")

    def _status_cb(self, msg):
        self.sim_status = Status[msg.data]

    def _wait_for_status(self, status, timeout=None):
        if timeout is None:
            timeout = self.timeout
        current_time = 0
        while self.sim_status != status and current_time <= timeout:
            start = time()
            self.rate.sleep()
            current_time += time() - start
        if current_time > timeout:
            rospy.logerr(f"Timed out ({timeout} seconds) waiting for {status.name}")
            return False
        return True

    def _wait_for_different_status(self, status, timeout=None):
        if timeout is None:
            timeout = self.timeout
        current_time = 0
        while self.sim_status == status and current_time <= timeout:
            start = time()
            self.rate.sleep()
            current_time += time() - start
        if current_time > timeout:
            rospy.logerr(f"Timed out ({timeout} seconds) waiting for change from {status.name}")
            return False
        return True

    def _start_task(self):
        start_task = rospy.ServiceProxy("/isaac/perform_task", Trigger)
        try:
            start_task(TriggerRequest())
        except rospy.ServiceException as e:
            rospy.logerr(f"Perform task service request failed: {e}")

    def _reset_task(self):
        reset_task = rospy.ServiceProxy("/isaac/reset_task", Trigger)
        try:
            reset_task(TriggerRequest())
        except rospy.ServiceException as e:
            rospy.logerr(f"Reset task service request failed: {e}")

    def _register_rosbag_topics(self, topics):
        register_topics = rospy.ServiceProxy("/rosbag/register_topics", TopicAction)
        try:
            register_topics(TopicActionRequest(topics))
        except rospy.ServiceException as e:
            rospy.logerr(f"Register rosbag topic request failed: {e}")

    def _start_rosbag_record(self, rosbag_filename):
        start_record = rospy.ServiceProxy("/rosbag/set_recording", RosbagAction)
        try:
            start_record(RosbagActionRequest(self.save_dir, rosbag_filename, True))
        except rospy.ServiceException as e:
            rospy.logerr(f"Start rosbag record request failed: {e}")

    def _stop_rosbag_record(self):
        stop_record = rospy.ServiceProxy("/rosbag/set_recording", RosbagAction)
        try:
            stop_record(RosbagActionRequest(set_recording=False))
        except rospy.ServiceException as e:
            rospy.logerr(f"Stop rosbag record request failed: {e}")

    def _delete_rosbag(self, filename):
        delete = rospy.ServiceProxy("/rosbag/delete_rosbag", RosbagAction)
        try:
            delete(RosbagActionRequest(path=self.save_dir, rosbag_filename=filename))
        except rospy.ServiceException as e:
            rospy.logerr(f"Delete rosbag request failed: {e}")


if __name__ == "__main__":
    rospy.init_node("collect_blocks_world.py")
    collector = BlocksWorldCollector()
    collector.collect_data()
