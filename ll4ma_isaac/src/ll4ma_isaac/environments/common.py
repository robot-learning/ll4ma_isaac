from enum import Enum


class Status(Enum):
    UNINITIALIZED = 1  # Robot is created but no objects in scene yet
    INITIALIZED = 2  # Both robot and scene objects have been created
    READY = 3  # Robot is at home position and objects in scene ready to be manipulated
    EXECUTING_TASK = 4  # Robot is currently executing the task
    TASK_SUCCESS = 5  # Task was completed successfully
    TASK_FAILURE = 6  # Task was NOT completed successfully
    RESETTING_TASK = 7  # Environment is being reset for next task (robot to home, reset objects)
    RESETTING_ROBOT = 8  # Robot is returning to home position
    ROBOT_HOME = 9  # Robot returned to home position
