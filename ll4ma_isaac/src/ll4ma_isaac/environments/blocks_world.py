import omni.usd
from omni.isaac.motion_planning import _motion_planning
from omni.isaac.dynamic_control import _dynamic_control
from omni.isaac import RosBridgeSchema
from pxr import Sdf

import gc
import numpy as np
import random
import concurrent.futures

import rospy
from sensor_msgs.msg import JointState, CameraInfo
from tf2_msgs.msg import TFMessage
from std_msgs.msg import String
from std_srvs.srv import Trigger, TriggerResponse
from ll4ma_isaac.msg import DataLog, Object
from ll4ma_isaac.srv import CreateScene, CreateSceneResponse, ResetTask, ResetTaskResponse


from ll4ma_isaac.behavior.world import World
from ll4ma_isaac.behavior.behavior_states import Domain
from ll4ma_isaac.behavior.behavior_helpers import go_home
from ll4ma_isaac.tasks import StackBlocks, PushBlocks, PushBlockLeftRight
from ll4ma_isaac.robots.franka import Franka, DEFAULT_JOINT_CONFIG
from ll4ma_isaac.util import isaac_util, ros_util
from ll4ma_isaac.util.synthetic_data_helper import SyntheticDataHelper
from ll4ma_isaac.environments.common import Status


DEFAULT_GT_SENSORS = ["rgb", "depth", "camera", "instanceSegmentation", "semanticSegmentation"]


class BlocksWorld:
    def __init__(self, editor, gt_sensors=DEFAULT_GT_SENSORS):
        self._editor = editor  # Reference to the Kit editor
        self._stage = omni.usd.get_context().get_stage()  # Reference to the current USD stage
        self._executor = concurrent.futures.ThreadPoolExecutor(max_workers=1)

        self._domain = None
        self._current_task_goal = ""
        self._first_step = True
        self._status = Status.UNINITIALIZED  # Will be INITIALIZED once objects added to scene
        self._goal_status = None  # Will track when goal is satisfied

        self._gt_sensors = gt_sensors
        self._synthetic_helper = SyntheticDataHelper(self._gt_sensors, self._stage, self._editor)
        self._objects = {}
        self._semantics = {"instance": {}, "semantic": {}}

        self._franka_path = "/environments/env/Franka/panda"

        # ROS
        rospy.init_node("blocks_world_isaac")
        rospy.Subscriber("/panda/joint_states", JointState, self._joint_state_cb)
        rospy.Subscriber("/panda/robot_command", JointState, self._robot_command_cb)
        rospy.Subscriber("/tf", TFMessage, self._tf_cb)
        rospy.Subscriber("/camera_info", CameraInfo, self._camera_info_cb)
        self._data_log_pub = rospy.Publisher("/isaac/data_log", DataLog, queue_size=1)
        self._status_pub = rospy.Publisher("/isaac/status", String, queue_size=1)

        self._publish_data = True  # Can be set through checkbox in omniverse extension GUI

        self._data_log = DataLog()
        self._joint_state = None
        self._tf = None
        self._camera_info = None

        rospy.Service("/isaac/perform_task", Trigger, self._perform_task_srv)
        rospy.Service("/isaac/reset_task", ResetTask, self._reset_task_srv)
        rospy.Service("/isaac/reset_robot", Trigger, self._reset_robot_srv)
        rospy.Service("/isaac/create_scene", CreateScene, self._create_scene_srv)

        self._task = StackBlocks()  # This can get changed in create scene srv

    def __del__(self):
        # Cleanup scenario objects when deleted, force garbage collection
        self._domain = None
        self._executor = None
        gc.collect()

    def step(self, dt, publish_data):
        if self._editor.is_playing():
            self._publish_data = publish_data
            if self._first_step:
                # IMPORTANT it's crucial to create the dynamic control interface and add the
                # ROS joint state bridge AFTER editor is playing. There's a bug that the
                # articulation handle gets messed up if you create it before and you'll get
                # bogus joint states.
                #   See this thread: https://forums.developer.nvidia.com/t/139930
                self._mp = _motion_planning.acquire_motion_planning_interface()
                self._dc = _dynamic_control.acquire_dynamic_control_interface()
                self._register_robot()
                # Add ROS joint state bridge
                js_path = "/ROS_JointState"
                ros_js = RosBridgeSchema.RosJointState.Define(self._stage, Sdf.Path(js_path))
                ros_js.CreateEnabledAttr(True)
                ros_js.CreateJointStatePubTopicAttr("/panda/joint_states")
                ros_js.CreateArticulationPrimRel()
                ros_js.CreateQueueSizeAttr(0)
                js_prim = self._stage.GetPrimAtPath(js_path)
                js_prim.GetRelationship("articulationPrim").SetTargets([self._franka_path])
                self._first_step = False

            # Seems dt=0 fairly often, I think it makes sense to only update if > 0
            if dt > 0 and self._joint_state and self._tf and self._camera_info:
                self._domain.update(dt)
                if self._task.goals and self._current_task_goal:
                    self._set_goal_status()
                if self._publish_data:
                    self._publish_status()
                    self._publish_log_data(dt)

    def create_assets(self, env_path="/environments/env", solid_path="/physics/scene/solid"):
        self._stage = omni.usd.get_context().get_stage()
        isaac_util.set_up_axis_z(self._stage)

        # Create ROS TF bridge that will publish robot/object poses to topic
        tf_path = "/ROS_PoseTree"
        ros_tf = RosBridgeSchema.RosPoseTree.Define(self._stage, Sdf.Path(tf_path))
        ros_tf.CreateEnabledAttr(True)
        ros_tf.CreatePoseTreePubTopicAttr("/tf")
        ros_tf.CreateQueueSizeAttr(0)
        self.tf_prim = self._stage.GetPrimAtPath(tf_path)

        # Create ROS camera bridge to expose RGB/depth to topics.
        camera_prim = RosBridgeSchema.RosCamera.Define(self._stage, Sdf.Path("/ROS_Camera"))
        camera_prim.CreateEnabledAttr(True)
        camera_prim.CreateCameraInfoPubTopicAttr("/camera_info")
        camera_prim.CreateRgbPubTopicAttr("/rgb")
        camera_prim.CreateRgbEnabledAttr(True)
        camera_prim.CreateDepthPubTopicAttr("/depth")
        camera_prim.CreateDepthEnabledAttr(True)
        camera_prim.CreateFrameIdAttr("/sim_camera")
        camera_prim.CreateQueueSizeAttr(10)

        isaac_util.create_franka(
            self._stage,
            self._semantics,
            env_path=env_path,
            solid_robot=solid_path,
            tf_prim=self.tf_prim,
        )
        isaac_util.create_background(self._stage)
        isaac_util.setup_physics(self._stage)

    def _register_robot(self, env_path="/environments/env", solid_path="/physics/scene/solid"):
        # Create world and robot object
        self.franka = Franka(
            self._stage,
            self._stage.GetPrimAtPath(self._franka_path),
            self._dc,
            self._mp,
            solid_path,
            DEFAULT_JOINT_CONFIG,
        )
        self.world = World(self._dc, self._mp, self.franka.rmp_handle)
        self.world.register_parent(self.franka.base, self.franka.prim, self.franka.base_link)
        self.world.register_object(env_path + "/DemoTable/simple_table/CollisionCube", "table", 0)
        self._domain = Domain(self.franka, self.world)
        self._domain.suppressor.table = self.world.get_object_from_name("table")

    def _run_task(self):
        self._task.run(self._domain)

    def _perform_task(self):
        if self._domain:
            self._status = Status.EXECUTING_TASK
            self._domain.stop = False
            random.shuffle(self._domain.obj_ids)  # TODO can have option to set manually
            self._domain.set_goal_xy()
            self._current_task_goal = self._task.get_goal(self._domain)
            print(f"\nTask GOAL: {self._current_task_goal}")
            # These calls are non-blocking so state machine runs asynchronously while
            # the editor steps (which also makes this scenario step)
            future = self._executor.submit(self._run_task)
            future.add_done_callback(self._on_task_complete)

            # Uncomment this line to debug if task doesn't run and you get no feedback,
            # futures stuff doesn't get reported unless you print it yourself:
            # print("FUTURE RESULT:\n", future.result())

    def _on_task_complete(self, future):
        self._status = self._task.check_goal_status(self._goal_status, self._current_task_goal)
        print(f"Task RESULT: {self._status.name}")
        self._current_task_goal = ""

    def _set_goal_status(self):
        object_poses = self._get_object_poses()
        self._goal_status = self._task.set_goal_status(
            self._current_task_goal, self._objects, object_poses
        )

    def _goal_satisfied(self, goal):
        object_poses = self._get_object_poses()
        return self._task.goal_satisified(goal, self._objects, object_poses)

    def _get_object_poses(self):
        poses = {}
        for obj_id in self._objects.keys():
            p, q = self._get_pose(obj_id)
            poses[obj_id] = {"position": p, "quaternion": q}
        return poses

    def _get_pose(self, obj_id):
        # Find TF associated with obj_id
        p = q = None
        for tf in self._tf.transforms:
            if tf.child_frame_id == obj_id:
                p = np.array(
                    [
                        tf.transform.translation.x,
                        tf.transform.translation.y,
                        tf.transform.translation.z,
                    ]
                )
                q = np.array(
                    [
                        tf.transform.rotation.x,
                        tf.transform.rotation.y,
                        tf.transform.rotation.z,
                        tf.transform.rotation.w,
                    ]
                )
        return p, q

    def _publish_status(self):
        self._status_pub.publish(String(self._status.name))

    def _publish_log_data(self, step):
        gt = self._synthetic_helper.get_groundtruth(self._gt_sensors)
        if not self._is_valid_ground_truth(gt):
            return

        self._data_log.sim_dt = step
        self._data_log.sim_time += step
        self._data_log.tf = self._tf
        self._data_log.joint_state = self._joint_state
        self._data_log.ros_camera_info = self._camera_info
        self._data_log.status = self._status.name
        self._data_log.task_goal = self._current_task_goal
        self._data_log.task_goals = self._task.goals
        self._data_log.task_name = self._task.get_name()
        if self._goal_status is not None:
            self._data_log.goal_status = self._goal_status.tolist()

        # Populate object data
        for obj_id, obj_data in self._objects.items():
            obj = Object()
            obj.name = obj_id
            obj.prim_path = obj_data["prim_path"]
            obj.body_type = obj_data["body_type"]
            obj.pose.position.x = obj_data["position"][0]
            obj.pose.position.y = obj_data["position"][1]
            obj.pose.position.z = obj_data["position"][2]
            obj.pose.orientation.x = obj_data["orientation"][0]
            obj.pose.orientation.y = obj_data["orientation"][1]
            obj.pose.orientation.z = obj_data["orientation"][2]
            obj.pose.orientation.w = obj_data["orientation"][3]
            obj.length = obj_data["length"]
            obj.width = obj_data["width"]
            obj.height = obj_data["height"]
            obj.mass = obj_data["mass"]
            obj.color.r = obj_data["color"][0]
            obj.color.g = obj_data["color"][1]
            obj.color.b = obj_data["color"][2]
            obj.color.a = 1
            obj.semantic_label = obj_data["semantic_label"]
            obj.units = obj_data["units"]
            self._data_log.objects.append(obj)

        # Ground truth sensor data
        if "camera" in gt:
            self._data_log.isaac_camera_info.pose = gt["camera"]["pose"].T.flatten().tolist()
            self._data_log.isaac_camera_info.fov = gt["camera"]["fov"]
            self._data_log.isaac_camera_info.focal_length = gt["camera"]["focal_length"]
            self._data_log.isaac_camera_info.horizontal_aperture = gt["camera"][
                "horizontal_aperture"
            ]
            proj_mat = gt["camera"]["view_projection_matrix"].flatten().tolist()
            self._data_log.isaac_camera_info.projection_matrix = proj_mat
            self._data_log.isaac_camera_info.resolution_width = gt["camera"]["resolution"]["width"]
            self._data_log.isaac_camera_info.resolution_height = gt["camera"]["resolution"][
                "height"
            ]
            self._data_log.isaac_camera_info.clipping_range_low = gt["camera"]["clipping_range"][0]
            self._data_log.isaac_camera_info.clipping_range_high = gt["camera"]["clipping_range"][1]
        if "rgb" in gt:
            self._data_log.rgb = ros_util.get_rgb_msg(gt["rgb"][:, :, :3])  # Get rid of A-channel
        if "depth" in gt:
            self._data_log.depth = ros_util.get_depth_msg(gt["depth"])
        if "instanceSegmentation" in gt:
            sdf_paths, k_instance_seg = gt["instanceSegmentation"]
            # Flattening k-channel array of masks down to single channel with k pixel values
            k_instance_seg = k_instance_seg.astype(np.uint8)
            instance_seg = np.zeros(k_instance_seg.shape[-2:])
            instance_labels = [p.pathString for p in sdf_paths]
            instance_ids = [self._semantics["instance"][label] for label in instance_labels]
            for i, instance_id in enumerate(instance_ids):
                instance_seg += instance_id * k_instance_seg[i]
            # Using depth message since the data format is the same
            self._data_log.instance_segmentation = ros_util.get_depth_msg(instance_seg)
            self._data_log.instance_labels = instance_labels
            self._data_log.instance_ids = instance_ids
        if "semanticSegmentation" in gt:
            labels, k_semantic_seg = gt["semanticSegmentation"]
            # Flattening k-channel array of masks down to single channel with k pixel values
            k_semantic_seg = k_semantic_seg.astype(np.uint8)
            semantic_seg = np.zeros(k_semantic_seg.shape[-2:])
            semantic_ids = [self._semantics["semantic"][label] for label in labels]
            for i, semantic_id in enumerate(semantic_ids):
                semantic_seg += semantic_id * k_semantic_seg[i]
            # Using depth message since the data format is the same
            self._data_log.semantic_segmentation = ros_util.get_depth_msg(semantic_seg)
            self._data_log.semantic_labels = labels
            self._data_log.semantic_ids = semantic_ids
        self._data_log.header.stamp = rospy.Time.now()
        self._data_log_pub.publish(self._data_log)

    def _is_valid_ground_truth(self, gt):
        return all([key in gt for key in self._gt_sensors])

    def _reset(self, reset_robot=True):
        self._status = Status.RESETTING_TASK
        if self._domain:
            self._domain.stop = True

        self._task.set_valid_object_poses(self._objects)

        if self._editor.is_playing() and self._domain:
            # Set object poses in the scene
            tfs = {}
            for k, v in self._objects.items():
                tf = _dynamic_control.Transform()
                tf.p = v["position"]
                tf.r = v["orientation"]
                tfs[k] = tf
            self._domain.world.reset(tfs)
            if reset_robot:
                go_home(self._domain.franka, wait=True)

        self._goal_status = np.zeros(len(self._task.goals), dtype=np.uint8)
        self._goal_status[self._task.goals.index("NOT_GOAL")] = 1

        # Adding a small wait since it takes a moment for objects to settle after reset
        rospy.sleep(1)
        self._status = Status.READY

    def _reset_robot(self):
        self._status = Status.RESETTING_ROBOT
        go_home(self._domain.franka, wait=True)
        self._status = Status.ROBOT_HOME

    def _get_next_init_pose(self):
        """
        This allows object initialization to follow a pattern for placing objects in the scene
        so they won't be in collision on initialization which breaks simulation. Not completely
        robust but for smaller objects (and not too many) should prevent most problems.
        """
        position = None
        try:
            position = self._init_positions.pop(0)
        except IndexError:
            rospy.logerr("Out of initialization positions")
        return position, [0, 0, 0, 1]

    def _reset_init_positions(self, x_low=30, x_high=70, y_low=-40, y_high=40, z=8, spacing=15):
        xs = list(range(x_low, x_high + 1, spacing))
        ys = list(range(y_low, y_high + 1, spacing))
        self._init_positions = []
        for x in xs:
            for y in ys:
                self._init_positions.append([x, y, z])

    def _joint_state_cb(self, msg):
        self._joint_state = msg

    def _robot_command_cb(self, msg):
        """
        Using this callback to manually send joint configs to the motion planning interface.
        The joint command topic offered by ROS bridge is ineffective once motion plan interface
        is running so this is workaround.

        Forum question: https://forums.developer.nvidia.com/t/154842

        TODO: I wanted to define my own message to have separate arm and gripper state,
        but there was a problem subscribing to it for some reason so hacking the gripper
        state in with the joint state.
        """
        if len(msg.position) != 7:  # TODO excluding gripper for now
            rospy.logerr(f"Expected joint state size 7 but got {len(msg.position)}")
            return
        self.franka.send_config(msg.position)

        # joint_positions = msg.position[:7]
        # gripper_positions = msg.position[7:]
        # self.franka.send_config(joint_positions)
        # self.franka.end_effector.gripper.set_finger_positions(gripper_positions)

    def _tf_cb(self, msg):
        self._tf = msg

    def _camera_info_cb(self, msg):
        self._camera_info = msg

    def _perform_task_srv(self, req):
        self._perform_task()
        return TriggerResponse(success=True)

    def _reset_task_srv(self, req):
        self._reset(req.reset_robot)
        return ResetTaskResponse(success=True)

    def _reset_robot_srv(self, req):
        self._reset_robot()
        return TriggerResponse(success=True)

    def _create_scene_srv(self, req):
        # TODO can make this a little nicer/robust
        if req.task_name == "stack_blocks":
            self._task = StackBlocks()
        elif req.task_name == "push_blocks":
            self._task = PushBlocks()
        elif req.task_name == "push_block_left_right":
            self._task = PushBlockLeftRight()
        else:
            rospy.logerr(f"Unknown task: {req.task_name}")
            return CreateSceneResponse(success=False)

        self._reset_init_positions()
        self._objects = {}
        for ros_obj in req.objects:
            obj_id = ros_obj.name
            position, orientation = self._get_next_init_pose()
            obj_data = {
                "prim_path": f"/environments/env/Objects/{obj_id}",
                "body_type": ros_obj.body_type,
                "position": position,
                "orientation": orientation,
                "length": ros_obj.length,
                "width": ros_obj.width,
                "height": ros_obj.height,
                "mass": ros_obj.mass,
                "color": ros_util.rgba_msg_to_rgb_tuple(ros_obj.color),
                "semantic_label": ros_obj.semantic_label,
                "units": ros_obj.units,
            }
            # Override pose if provided by ROS object
            q = ros_obj.pose.orientation
            if q.x + q.y + q.z + q.w > 0:
                p = ros_obj.pose.position
                obj_data["position"] = [p.x, p.y, p.z]
                obj_data["orientation"] = [q.x, q.y, q.z, q.w]
            isaac_util.create_rigid_body(obj_data, self._stage, self._semantics, self.tf_prim)
            self._objects[obj_id] = obj_data

        self._task.set_goals(self._objects)

        # Try to avoid DC handles not being created yet by waiting a bit. There are robust checks
        # with timeouts for finding them, but this can help. Need to figure out better way to wait
        # for the handle because this approach throws unnecessary errors in console.
        rospy.sleep(1)
        for obj_id, obj_data in self._objects.items():
            self._domain.register_object(obj_id, obj_data)

        self._status = Status.INITIALIZED
        # Track which goals have been satisfied, will activate a bit for a goal if it's
        # determined to be satisfied (based on whatever the goal condition is)
        self._goal_status = np.zeros(len(self._task.goals), dtype=np.uint8)
        self._goal_status[self._task.goals.index("NOT_GOAL")] = 1

        if req.reset_scene:
            self._reset()

        return CreateSceneResponse(success=True)
