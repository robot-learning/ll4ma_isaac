"""
Utility functions for working with ROS data
"""
import h5py
import numpy as np
import cv2

from ll4ma_util import ros_util

from ll4ma_isaac.environments.common import Status


def log_msgs_to_h5(msgs, h5_filename, img_size=128):
    """
    Converts list of data log msgs to an H5 file.
    """
    h5_file = h5py.File(h5_filename, "w")
    n_msgs = len(msgs)
    test_msg = msgs[0]  # One msg to get sizes/names for pre-allocation

    h5_file.attrs["task_goals"] = np.array(test_msg.task_goals, dtype="S")
    # Find the task goal
    for msg in msgs:
        if not msg.task_goal:
            continue
        h5_file.attrs["task_goal"] = msg.task_goal
        break
    # Figure out if it was task success or failure
    h5_file.attrs["task_execution_result"] = "UNKNOWN"
    for msg in reversed(msgs):
        if msg.status == Status.TASK_SUCCESS.name or msg.status == Status.TASK_FAILURE.name:
            h5_file.attrs["task_execution_result"] = msg.status
            break
    h5_file.attrs["task_name"] = test_msg.task_name

    joint_names = test_msg.joint_state.name
    finger_idxs = [i for i, n in enumerate(joint_names) if "finger" in n]
    arm_idxs = [i for i, n in enumerate(joint_names) if "finger" not in n]
    finger_joint_names = np.array([n for n in joint_names if "finger" in n], dtype="S")
    arm_joint_names = np.array([n for n in joint_names if "finger" not in n], dtype="S")

    rgb = np.zeros((n_msgs, img_size, img_size, 3), dtype=np.uint8)
    depth = np.zeros((n_msgs, img_size, img_size), dtype=np.float32)
    instance_seg = np.zeros((n_msgs, img_size, img_size), dtype=np.uint8)
    semantic_seg = np.zeros((n_msgs, img_size, img_size), dtype=np.uint8)
    time = np.zeros((n_msgs, 1))
    dt = np.zeros((n_msgs, 1))
    joint_positions = np.zeros((n_msgs, len(arm_joint_names)))
    joint_velocities = np.zeros((n_msgs, len(arm_joint_names)))
    gripper_joint_positions = np.zeros((n_msgs, len(finger_joint_names)))
    gripper_joint_velocities = np.zeros((n_msgs, len(finger_joint_names)))
    tfs = {
        tf.child_frame_id: {"position": np.zeros((n_msgs, 3)), "orientation": np.zeros((n_msgs, 4))}
        for tf in test_msg.tf.transforms
    }
    tf_parents = {tf.child_frame_id: tf.header.frame_id for tf in test_msg.tf.transforms}
    status = []
    goal_status = []
    semantic_labels = np.array(test_msg.semantic_labels, dtype="S")

    # TODO Ids are screwed up on save

    semantic_ids = np.array(test_msg.semantic_ids)
    instance_labels = np.array(test_msg.instance_labels, dtype="S")
    instance_ids = np.array(test_msg.instance_ids)
    objects = {
        obj.name: {
            "prim_path": obj.prim_path,
            "body_type": obj.body_type,
            "position": np.array([obj.pose.position.x, obj.pose.position.y, obj.pose.position.z]),
            "orientation": np.array(
                [
                    obj.pose.orientation.x,
                    obj.pose.orientation.y,
                    obj.pose.orientation.z,
                    obj.pose.orientation.w,
                ]
            ),
            "length": obj.length,
            "width": obj.width,
            "height": obj.height,
            "mass": obj.mass,
            "semantic_label": obj.semantic_label,
            "units": obj.units,
            "color": np.array([obj.color.r, obj.color.g, obj.color.b]),
        }
        for obj in test_msg.objects
    }
    ros_cam_info = {
        "frame_id": test_msg.ros_camera_info.header.frame_id,
        "height": test_msg.ros_camera_info.height,
        "width": test_msg.ros_camera_info.width,
        "distortion_model": test_msg.ros_camera_info.distortion_model,
        "D": np.array(test_msg.ros_camera_info.D),
        "K": np.array(test_msg.ros_camera_info.K),
        "R": np.array(test_msg.ros_camera_info.R),
        "P": np.array(test_msg.ros_camera_info.P),
        "binning_x": test_msg.ros_camera_info.binning_x,
        "binning_y": test_msg.ros_camera_info.binning_y,
    }
    isaac_cam_info = {
        "clipping_range_low": test_msg.isaac_camera_info.clipping_range_low,
        "clipping_range_high": test_msg.isaac_camera_info.clipping_range_high,
        "focal_length": test_msg.isaac_camera_info.focal_length,
        "fov": test_msg.isaac_camera_info.fov,
        "horizontal_aperture": test_msg.isaac_camera_info.horizontal_aperture,
        "pose": np.array(test_msg.isaac_camera_info.pose),
        "projection_matrix": np.array(test_msg.isaac_camera_info.projection_matrix),
        "resolution_height": test_msg.isaac_camera_info.resolution_height,
        "resolution_width": test_msg.isaac_camera_info.resolution_width,
    }

    for i, msg in enumerate(msgs):
        # RGB
        rgb_i = ros_util.msg_to_rgb(msg.rgb)
        rgb_i = cv2.resize(rgb_i, (img_size, img_size), interpolation=cv2.INTER_LINEAR)
        rgb[i] = rgb_i
        # Depth
        depth_i = ros_util.msg_to_depth(msg.depth)
        depth_i = cv2.resize(depth_i, (img_size, img_size), interpolation=cv2.INTER_LINEAR)
        depth[i] = depth_i
        # Segmentations
        instance_seg_i = ros_util.msg_to_img(msg.instance_segmentation)
        instance_seg_i = cv2.resize(
            instance_seg_i, (img_size, img_size), interpolation=cv2.INTER_NEAREST
        ).astype(np.uint8)
        instance_seg[i] = instance_seg_i
        semantic_seg_i = ros_util.msg_to_img(msg.semantic_segmentation)
        semantic_seg_i = cv2.resize(
            semantic_seg_i, (img_size, img_size), interpolation=cv2.INTER_NEAREST
        ).astype(np.uint8)
        semantic_seg[i] = semantic_seg_i
        # Joint state
        joint_positions[i] = np.take(msg.joint_state.position, arm_idxs)
        joint_velocities[i] = np.take(msg.joint_state.velocity, arm_idxs)
        gripper_joint_positions[i] = np.take(msg.joint_state.position, finger_idxs)
        gripper_joint_velocities[i] = np.take(msg.joint_state.velocity, finger_idxs)
        # TF
        for tf in msg.tf.transforms:
            tfs[tf.child_frame_id]["position"][i] = np.array(
                [tf.transform.translation.x, tf.transform.translation.y, tf.transform.translation.z]
            )
            tfs[tf.child_frame_id]["orientation"][i] = np.array(
                [
                    tf.transform.rotation.x,
                    tf.transform.rotation.y,
                    tf.transform.rotation.z,
                    tf.transform.rotation.w,
                ]
            )
        # Remaining data
        time[i] = msg.sim_time
        dt[i] = msg.sim_dt
        status.append(msg.status)
        goal_status.append(msg.goal_status)

    h5_file["rgb"] = rgb
    h5_file["depth"] = depth
    h5_file["semantic_segmentation"] = semantic_seg
    h5_file["semantic_segmentation"].attrs["ids"] = semantic_ids
    h5_file["semantic_segmentation"].attrs["labels"] = semantic_labels
    h5_file["instance_segmentation"] = instance_seg
    h5_file["instance_segmentation"].attrs["ids"] = instance_ids
    h5_file["instance_segmentation"].attrs["labels"] = instance_labels
    h5_file["joint_positions"] = joint_positions
    h5_file["joint_positions"].attrs["joint_names"] = arm_joint_names
    h5_file["joint_velocities"] = joint_velocities
    h5_file["joint_velocities"].attrs["joint_names"] = arm_joint_names
    h5_file["gripper_joint_positions"] = gripper_joint_positions
    h5_file["gripper_joint_positions"].attrs["joint_names"] = finger_joint_names
    h5_file["gripper_joint_velocities"] = gripper_joint_velocities
    h5_file["gripper_joint_velocities"].attrs["joint_names"] = finger_joint_names
    for obj_id, obj_data in tfs.items():
        h5_file.create_group(f"tf/{obj_id}")
        h5_file["tf"][obj_id]["position"] = obj_data["position"]
        h5_file["tf"][obj_id]["orientation"] = obj_data["orientation"]
        h5_file["tf"][obj_id].attrs["parent_frame"] = tf_parents[obj_id]
    h5_file["time"] = time
    h5_file["dt"] = dt
    h5_file["status"] = np.array(status, dtype="S")
    h5_file["goal_status"] = np.array(goal_status)
    for obj_id, obj_data in objects.items():
        h5_file.create_group(f"objects/{obj_id}")
        for key, value in obj_data.items():
            h5_file["objects"][obj_id].attrs[key] = value
    h5_file.create_group("camera_info_ros")
    for key, value in ros_cam_info.items():
        h5_file["camera_info_ros"].attrs[key] = value
    h5_file.create_group("camera_info_isaac")
    for key, value in isaac_cam_info.items():
        h5_file["camera_info_isaac"].attrs[key] = value

    h5_file.close()
