#!/usr/bin/env python
# Copyright (c) 2020, NVIDIA CORPORATION.  All rights reserved.
#
# NVIDIA CORPORATION and its licensors retain all intellectual property
# and proprietary rights in and to this software, related documentation
# and any modifications thereto.  Any use, reproduction, disclosure or
# distribution of this software and related documentation without an express
# license agreement from NVIDIA CORPORATION is strictly prohibited.


"""Helper class for obtaining groundtruth data from OmniKit.

Support provided for RGB, Depth, Bounding Box (2D Tight, 2D Loose, 3D),
segmentation (instance and semantic), and camera parameters.

    Typical usage example:

    kit = OmniKitHelper()   # Start omniverse kit
    sd_helper = SyntheticDataHelper()
    gt = sd_helper.get_groundtruth(('rgb', 'depth', 'bbox2d_tight'))

"""

import math
import carb
import omni.kit.editor
from pxr import UsdGeom, Semantics
import numpy as np


# List of sensors requiring RayTracedLighting mode
RT_ONLY_SENSORS = [
    "depth",
    "boundingBox2DTight",
    "boundingBox2DLoose",
    "instanceSegmentation",
    "semanticSegmentation",
]


def get_view_proj_mat(camera_tf_matrix, fov, aspect_ratio, z_near, z_far):
    view_matrix = np.linalg.inv(camera_tf_matrix)
    projection_mat = get_projection_matrix(fov, aspect_ratio, z_near, z_far)
    return np.dot(view_matrix, projection_mat)


def get_projection_matrix(fov, aspect_ratio, z_near, z_far):
    """Calculate the camera projection matrix."""
    a = -1.0 / math.tan(fov / 2)
    b = -a * aspect_ratio
    c = z_far / (z_far - z_near)
    d = z_near * z_far / (z_far - z_near)
    return np.array(
        [[a, 0.0, 0.0, 0.0], [0.0, b, 0.0, 0.0], [0.0, 0.0, c, 1.0], [0.0, 0.0, d, 0.0]]
    )


def project_points(view_projection_matrix, points):
    """Project points onto a 2D plane."""
    points_homo = np.pad(points, ((0, 0), (0, 1)), mode="constant", constant_values=1.0)
    tf_points = np.dot(points_homo, view_projection_matrix)
    tf_points = tf_points / (tf_points[..., -1:])
    tf_points[..., :2] = 0.5 * (tf_points[..., :2] + 1)
    return tf_points


class SyntheticDataHelper:
    def __init__(self, gt_sensors, stage, editor):
        self.app = omni.kit.app.get_app_interface()
        ext_manager = self.app.get_extension_manager()
        ext_manager.set_extension_enabled("omni.syntheticdata", True)

        # Must be imported after getting app interface:
        import omni.syntheticdata._syntheticdata as sd

        self.sd = sd
        self.sd_interface = self.sd.acquire_syntheticdata_interface()
        self.editor = editor
        self.carb_settings = carb.settings.acquire_settings_interface()

        self.stage = stage

        self.sensor_helpers = {
            "rgb": self.get_rgb,
            "depth": self.get_depth,
            "instanceSegmentation": self.get_instance_segmentation,
            "semanticSegmentation": self.get_semantic_segmentation,
            "boundingBox2DTight": self.get_bbox2d_tight,
            "boundingBox2DLoose": self.get_bbox2d_loose,
            "boundingBox3D": self.compute_3d_bounding_box_oobb,
            "camera": self.get_camera_params,
        }

        self.sensor_state = {s: False for s in list(self.sensor_helpers.keys())}

        self.gt_sensors = gt_sensors
        for sensor in self.gt_sensors:
            if sensor not in list(self.sensor_helpers.keys()):
                raise ValueError
            if not self.sensor_state[sensor]:
                self.carb_settings.set_bool(f"/syntheticdata/sensors/{sensor}Sensor", True)
                self.sensor_state[sensor] = True

    def get_rgb(self):
        """Get RGB groundtruth data

        Returns:
            numpy.ndarray(dtype=uint8, shape=(H, W, C))
        """
        data = self._get_sensor_data(self.sd.SensorType.Rgb, "uint32")
        image_data = np.frombuffer(data, dtype=np.uint8).reshape(*data.shape, -1)
        return image_data

    def get_depth(self):
        """Get Depth groundtruth data

        Returns:
            numpy.ndarray(dtype=float32, shape=(H, W))
        """
        depth = self._get_sensor_data(self.sd.SensorType.Depth, "float")
        return depth

    def _get_sensor_data(self, sensor, dtype):
        width = self.sd_interface.get_sensor_width(sensor)
        height = self.sd_interface.get_sensor_height(sensor)
        row_size = self.sd_interface.get_sensor_row_size(sensor)

        get_sensor = {
            "uint32": self.sd_interface.get_sensor_host_uint32_texture_array,
            "float": self.sd_interface.get_sensor_host_float_texture_array,
        }
        return get_sensor[dtype](sensor, width, height, row_size)

    def get_instance_segmentation(self):
        """Get instance segmentation data.
        Generate a list of N instance names and corresponding array of N
        binary instance masks.

        Returns:
            A tuple of a list of instance names, and a bool array with shape (N, H, W).
        """
        sensor = self.sd.SensorType.InstanceSegmentation
        instance_tex = self._get_sensor_data(sensor, "uint32")
        instance_mappings = self.get_instance_mappings()
        instances_list = [im[4] for im in instance_mappings]
        instance_names = [im[0] for im in instance_mappings]

        instance_masks = np.zeros((len(instance_mappings), *instance_tex.shape), dtype=np.bool)
        for i, instances in enumerate(instances_list):
            instance_masks[i] = np.isin(instance_tex, instances)
        return instance_names, instance_masks

    def get_semantic_segmentation(self):
        """Get semantic segmentation data.
        Generate a list of N semantic labels and corresponding array of N
        binary semantic masks.

        Returns:
            A tuple of a list of semantic labels, and a bool array with shape (N, H, W).
        """
        sensor = self.sd.SensorType.InstanceSegmentation
        instance_seg_texture = self._get_sensor_data(sensor, "uint32")
        instance_mappings = self.get_instance_mappings()

        semantic_mappings = {}
        for im in instance_mappings:
            semantic_mappings.setdefault(im[3], []).extend(im[4])

        semantic_labels = list(semantic_mappings.keys())
        semantic_masks = np.zeros(
            (len(semantic_labels), *instance_seg_texture.shape), dtype=np.bool
        )
        for i, (semantic_label, instances) in enumerate(semantic_mappings.items()):
            semantic_masks[i] = np.isin(instance_seg_texture, instances)
        return semantic_labels, semantic_masks

    def get_bbox2d_tight(self):
        """Get Bounding Box 2D with tight bounds.
        Bounding boxes will only bound visible portions of an object.

        Returns:
            numpy.ndarray(dtype=[('semanticLabel', 'O'), ('instanceId', '<u4'),
                                 ('semanticId', '<u4'), ('x_min', '<i4'), ('y_min', '<i4'),
                                 ('x_max', '<i4'), ('y_max', '<i4')]),
                          shape=N)
        """
        sensor = self.sd.SensorType.BoundingBox2DTight
        return self._get_bbox2d(sensor)

    def get_bbox2d_loose(self):
        """Get Bounding Box 2D with tight bounds.
        Bounding boxes will bound entire object, including occluded portions.

        Returns:
            numpy.ndarray(dtype=[('semanticLabel', 'O'), ('instanceId', '<u4'),
                                 ('semanticId', '<u4'), ('x_min', '<i4'), ('y_min', '<i4'),
                                 ('x_max', '<i4'), ('y_max', '<i4')]),
                          shape=N)
        """
        sensor = self.sd.SensorType.BoundingBox2DLoose
        return self._get_bbox2d(sensor)

    def get_camera_params(self):
        """Get active camera intrinsic and extrinsic parameters.

        Returns:
            A dict of the active camera's parameters.

            pose (numpy.ndarray): camera position in world coordinates,
            fov (float): horizontal field of view in radians
            focal_length (float)
            horizontal_aperture (float)
            view_projection_matrix (numpy.ndarray(dtype=float64, shape=(4, 4)))
            resolution (dict): resolution as a dict with 'width' and 'height'.
            clipping_range (tuple(float, float)): Near and Far clipping values.
        """
        prim = self.stage.GetPrimAtPath(self.editor.get_active_camera())
        prim_tf = UsdGeom.Xformable(prim).ComputeLocalToWorldTransform(0.0)
        focal_length = prim.GetAttribute("focalLength").Get()
        horiz_aperture = prim.GetAttribute("horizontalAperture").Get()
        fov = 2 * math.atan(horiz_aperture / (2 * focal_length))
        width = self.sd_interface.get_sensor_width(self.sd.SensorType.Rgb)
        height = self.sd_interface.get_sensor_height(self.sd.SensorType.Rgb)
        aspect_ratio = width / height
        near, far = prim.GetAttribute("clippingRange").Get()
        view_proj_mat = get_view_proj_mat(prim_tf, fov, aspect_ratio, near, far)

        return {
            "pose": np.array(prim_tf),
            "fov": fov,
            "focal_length": focal_length,
            "horizontal_aperture": horiz_aperture,
            "view_projection_matrix": view_proj_mat,
            "resolution": {"width": width, "height": height},
            "clipping_range": (near, far),
        }

    def compute_3d_bounding_box_oobb(self):
        """Compute the 3D Object Oriented Bounding Box.
        Uses the UsdGeom.Imageable module to compute local bounds
        and transform and transforms the points accordingly.

        Returns:
            numpy.ndarray(dtype=float64, shape=(N, 8, 3))
        """
        bounding_boxes = []
        for prim in self.stage.Traverse():
            if prim.HasAPI(Semantics.SemanticsAPI):
                imageable = UsdGeom.Imageable(prim)
                bounds = imageable.ComputeLocalBound(0.0, "default")
                box = bounds.GetBox()
                bb = np.array([box.GetCorner(i) for i in range(8)])
                bb = np.pad(bb, ((0, 0), (0, 1)), mode="constant", constant_values=1.0)
                bb = np.dot(bb, np.array(bounds.GetMatrix()))[:, :3]
                bounding_boxes.append(bb)
        return np.array(bounding_boxes)

    def _get_instance_mapping(self, cur_prim):
        instance_mappings = []
        descendant_instance_ids = []
        children = cur_prim.GetChildren()
        instance_id = self.sd_interface.get_instance_segmentation_id(str(cur_prim.GetPath()))
        # print("ID", instance_id)

        for child in children:
            child_descendant_instance_ids, child_instance_mappings = self._get_instance_mapping(
                child
            )
            descendant_instance_ids += child_descendant_instance_ids
            instance_mappings += child_instance_mappings
        if instance_id > 0:
            descendant_instance_ids += [instance_id]
        if cur_prim.HasAPI(Semantics.SemanticsAPI):
            semantic_label = cur_prim.GetAttribute("semantic:Semantics:params:semanticData").Get()
            semantic_id = self.sd_interface.get_semantic_segmentation_id_from_data(semantic_label)
            instance_mappings.append(
                (
                    cur_prim.GetPath(),
                    instance_id,
                    semantic_id,
                    semantic_label,
                    descendant_instance_ids,
                )
            )
        return descendant_instance_ids, instance_mappings

    def _reduce_bboxes(self, bboxes):
        """Reduce bounding boxes of leaf nodes to parents with a semantic label
        and add label to data.
        """
        instance_mappings = self.get_instance_mappings()
        reduced_bboxes = []
        for im in instance_mappings:
            if im[4]:  # if mapping has descendant instance ids
                mask = np.isin(bboxes["instanceId"], im[4])
                bbox_masked = bboxes[mask]
                if len(bbox_masked) > 0:
                    reduced_bboxes.append(
                        (
                            im[3],
                            im[1],
                            im[2],  # semanticLabel, instanceId, semanticId
                            np.min(bbox_masked["x_min"]),
                            np.min(bbox_masked["y_min"]),
                            np.max(bbox_masked["x_max"]),
                            np.max(bbox_masked["y_max"]),
                        )
                    )

        return np.array(reduced_bboxes, dtype=[("semanticLabel", "O")] + bboxes.dtype.descr)

    def get_instance_mappings(self):
        """Get mapping between prims with semantic labels and leaf instance IDs.
        Traverse through the scene graph and for each prim with a semantic label,
        store a list of the IDs of its leaf nodes.

        Returns:
            list of tuples mapping leaf instance IDs to a parent with a semantic label.
            Each tuple is represented by
            (<Path>, <Instance ID>, <Semantic ID>, <List of descendant instance IDs>,
             <semantic Label>). For example:

            [('/World/car', 0, 1, [0, 1, 3, 4], 'Vehicle'),
             ('/World/car/tail_lights', 0, 2, [2, 3], 'TailLights')]
        """
        _, self.instance_mappings = self._get_instance_mapping(self.stage.GetPseudoRoot())
        return self.instance_mappings

    def _get_bbox2d(self, sensor):
        size = self.sd_interface.get_sensor_size(sensor)
        bboxes = self.sd_interface.get_sensor_host_bounding_box_2d_buffer_array(sensor, size)
        bboxes = self._reduce_bboxes(bboxes)
        return bboxes

    def get_groundtruth(self, gt_sensors):
        """Get groundtruth from specified gt_sensors.
        Enable syntheticdata sensors if required, render a frame and
        collect groundtruth from the specified gt_sensors

        If a sensor requiring RayTracedLighting mode is specified, render
        an additional frame in RayTracedLighting mode.

        Args:
            gt_sensors (list): List of strings of sensor names. Valid sensors names: rgb, depth,
                instanceSegmentation, semanticSegmentation, boundingBox2DTight,
                boundingBox2DLoose, boundingBox3D, camera

        Returns:
            Dict of sensor outputs
        """
        if isinstance(gt_sensors, str):
            gt_sensors = (gt_sensors,)

        rt_sensors = []
        remaining_sensors = []
        for sensor in gt_sensors:
            if sensor not in self.sensor_helpers:
                raise ValueError(
                    f"Sensor {sensor} is not supported. Choose from "
                    f"the following: {list(self.sensor_helpers.keys())}"
                )
            if sensor in RT_ONLY_SENSORS:
                rt_sensors.append(sensor)
            else:
                remaining_sensors.append(sensor)

        gt = {}
        # Process non-RT-only sensors
        for sensor in remaining_sensors:
            gt[sensor] = self.sensor_helpers[sensor]()

        # If using a sensor incompatible with current render mode, change mode and re-render
        cur_render_mode = self.carb_settings.get_as_string("/rtx/rendermode")
        if cur_render_mode != "RayTracedLighting" and rt_sensors:
            self.carb_settings.set_string("/rtx/rendermode", "RayTracedLighting")
            self.app.update(0.0)

        # Populate gt dict based on selected sensors
        for sensor in rt_sensors:
            gt[sensor] = self.sensor_helpers[sensor]()

        # Set render mode back to user-specified mode
        self.carb_settings.set_string("/rtx/rendermode", cur_render_mode)
        return gt
