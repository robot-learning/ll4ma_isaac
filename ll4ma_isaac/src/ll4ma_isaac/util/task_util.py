import numpy as np

from ll4ma_isaac.util import math_util


def get_heuristic_top_grasp_pose(obj_quat, world_bias_axis=np.array([1, 0, 0])):
    """
    Find top grasp (i.e. axis pointing out from gripper aligned to negative world up-axis)
    biased to align with a world frame. By default biases to match world x-axis which is
    usually pointing forward from robot's POV perspective.
    """
    obj_R = math_util.rotation_matrix_from_quaternion(obj_quat)
    # Find the axis closest to the bias axis
    sims = np.dot(obj_R, world_bias_axis)
    align_axis_idx = np.argmax(np.abs(sims))
    ee_R = np.eye(3)
    # TODO hard-coding assuming aligning EE x-axis to world -z
    ee_R[:, 0] = np.array([0, 0, -1])
    # Align EE z axis to found axis (sign accounts for whether aligned
    # axis is pointing in same or opposite direction as bias axis)
    ee_R[:, 2] = np.sign(sims[align_axis_idx]) * obj_R[:, align_axis_idx]
    other_axis = np.cross(
        ee_R[:, 2], ee_R[:, 0]
    )  # Assign remaining axis as cross product of others
    ee_R[:, 1] = other_axis / np.linalg.norm(other_axis)
    ee_quat = math_util.quaternion_from_rotation_matrix(ee_R)
    return ee_quat


def get_heuristic_side_grasp_pose(obj_quat, world_bias_axis=np.array([1, 0, 0])):
    """
    Find side grasp
    (i.e. axis pointing out from gripper aligned to face towards a target object)
    TODO: implement
    """
    obj_R = math_util.rotation_matrix_from_quaternion(obj_quat)
    # Find the axis closest to the bias axis
    sims = np.dot(obj_R, world_bias_axis)
    align_axis_idx = np.argmax(np.abs(sims))
    ee_R = np.eye(3)
    # Making the y-axis of the end effector align to +z of world
    ee_R[:, 1] = np.array([0, 0, 1])

    #     ee_R[:,0] = -obj_R[:,0]
    ee_R[:, 0] = np.sign(sims[align_axis_idx]) * obj_R[:, align_axis_idx]
    # Assign the remaining axis
    other_axis = np.cross(ee_R[:, 0], ee_R[:, 1])
    ee_R[:, 2] = other_axis / np.linalg.norm(other_axis)
    ee_quat = math_util.quaternion_from_rotation_matrix(ee_R)
    return ee_quat
