import torch
import numpy as np
from numpy.linalg import norm
from scipy.spatial.transform import Rotation as R
import copy
import traceback
import math

# TODO silent import so it's useful also in gym
try:
    from pxr import Gf
except ModuleNotFoundError:
    pass


def normalize(v):
    if norm(v) == 0:
        traceback.print_stack()
    v /= norm(v)
    return v


def normalized(v):
    if v is None:
        return None
    return normalize(copy.deepcopy(v))


def proj_orth(v1, v2, normalize_res=False, eps=1e-5):
    v2_norm = norm(v2)
    if v2_norm < eps:
        return v1

    v2n = v2 / v2_norm
    v1 = v1 - np.dot(v1, v2n) * v2n
    if normalize_res:
        return normalized(v1)
    else:
        return v1


def axes2mat(axis_x, axis_z, dominant_axis="z"):
    if dominant_axis == "z":
        axis_x = proj_orth(axis_x, axis_z)
    elif dominant_axis == "x":
        axis_z = proj_orth(axis_z, axis_x)
    elif dominant_axis is None:
        pass
    else:
        raise RuntimeError("Unrecognized dominant_axis: %s" % dominant_axis)

    axis_x = axis_x / norm(axis_x)
    axis_z = axis_z / norm(axis_z)
    axis_y = np.cross(axis_z, axis_x)

    R = np.zeros((3, 3))
    R[0:3, 0] = axis_x
    R[0:3, 1] = axis_y
    R[0:3, 2] = axis_z

    return R


# Projects T to align with the provided direction vector v.
def proj_to_align(R, v):
    max_entry = max(
        enumerate([np.abs(np.dot(R[0:3, i], v)) for i in range(3)]), key=lambda entry: entry[1]
    )
    return axes2mat(R[0:3, (max_entry[0] + 1) % 3], v)


def as_np_matrix_t(input):
    result = np.identity(4)
    result[:3, 3] = Gf.Vec3f(input.p.x, input.p.y, input.p.z)
    result[:3, :3] = Gf.Matrix3f(
        Gf.Quatf(input.r.w, Gf.Vec3f(input.r.x, input.r.y, input.r.z))
    ).GetTranspose()
    return result


def lookAt(camera, target, up):

    F = (target - camera).GetNormalized()
    R = Gf.Cross(up, F).GetNormalized()
    U = Gf.Cross(F, R)

    q = Gf.Quatf()
    trace = R[0] + U[1] + F[2]
    if trace > 0.0:
        s = 0.5 / math.sqrt(trace + 1.0)
        q = Gf.Quatf(0.25 / s, Gf.Vec3f((U[2] - F[1]) * s, (F[0] - R[2]) * s, (R[1] - U[0]) * s))
    else:
        if R[0] > U[1] and R[0] > F[2]:
            s = 2.0 * math.sqrt(1.0 + R[0] - U[1] - F[2])
            q = Gf.Quatf(
                (U[2] - F[1]) / s, Gf.Vec3f(0.25 * s, (U[0] + R[1]) / s, (F[0] + R[2]) / s)
            )
        elif U[1] > F[2]:
            s = 2.0 * math.sqrt(1.0 + U[1] - R[0] - F[2])
            q = Gf.Quatf(
                (F[0] - R[2]) / s, Gf.Vec3f((U[0] + R[1]) / s, 0.25 * s, (F[1] + U[2]) / s)
            )
        else:
            s = 2.0 * math.sqrt(1.0 + F[2] - R[0] - U[1])
            q = Gf.Quatf(
                (R[1] - U[0]) / s, Gf.Vec3f((F[0] + R[2]) / s, (F[1] + U[2]) / s, 0.25 * s)
            )
    return q


def get_random_planar_quaternion(min_degrees=0, max_degrees=360, axis="z"):
    rotation = np.random.uniform(min_degrees, max_degrees)
    r = R.from_euler(axis, rotation, degrees=True)
    return r.as_quat()


def rotation_matrix_from_quaternion(q):
    r = R.from_quat(q)
    return r.as_matrix()


def quaternion_from_rotation_matrix(r):
    q = R.from_matrix(r)
    return q.as_quat()


# torch quaternion functions from NVIDIA:


def quat_mul(a, b):
    assert a.shape == b.shape
    shape = a.shape
    a = a.reshape(-1, 4)
    b = b.reshape(-1, 4)

    x1, y1, z1, w1 = a[:, 0], a[:, 1], a[:, 2], a[:, 3]
    x2, y2, z2, w2 = b[:, 0], b[:, 1], b[:, 2], b[:, 3]
    ww = (z1 + x1) * (x2 + y2)
    yy = (w1 - y1) * (w2 + z2)
    zz = (w1 + y1) * (w2 - z2)
    xx = ww + yy + zz
    qq = 0.5 * (xx + (z1 - x1) * (x2 - y2))
    w = qq - ww + (z1 - y1) * (y2 - z2)
    x = qq - xx + (x1 + w1) * (x2 + w2)
    y = qq - yy + (w1 - x1) * (y2 + z2)
    z = qq - zz + (z1 + y1) * (w2 - x2)

    quat = torch.stack([x, y, z, w], dim=-1).view(shape)

    return quat


def quat_conjugate(a):
    shape = a.shape
    a = a.reshape(-1, 4)
    return torch.cat((-a[:, :3], a[:, -1:]), dim=-1).view(shape)


def quaternion_error(desired, current, square=False, numpy=False, flatten=False):
    q_c = quat_conjugate(current)
    q_r = quat_mul(desired, q_c)
    error = q_r[:, 0:3] * torch.sign(q_r[:, 3]).unsqueeze(-1)
    if square:
        error = error**2
    if numpy:
        error = error.cpu().numpy()
    if flatten:
        error = error.flatten()
    return error


def position_error(desired, current, square=False, numpy=False, flatten=False):
    error = desired - current
    if square:
        error = error**2
    if numpy:
        error = error.cpu().numpy()
    if flatten:
        error = error.flatten()
    return error
