import time
from pxr import Usd, UsdGeom, Gf, Sdf, Semantics, PhysicsSchema, PhysxSchema, Vt


def add_semantic_label(prim, label, semantics):
    semantic_api = Semantics.SemanticsAPI.Apply(prim, "Semantics")
    semantic_api.CreateSemanticTypeAttr().Set("class")
    semantic_api.CreateSemanticDataAttr().Set(label)
    inst_id = len(semantics["instance"]) + 1
    semantics["instance"][prim.GetPath().pathString] = inst_id
    if label not in semantics["semantic"]:
        sem_id = len(semantics["semantic"]) + 1
        semantics["semantic"][label] = sem_id


def create_rigid_body(obj_properties, stage, semantics, tf_prim=None):
    mass = obj_properties["mass"]
    length = obj_properties["length"]
    width = obj_properties["width"]
    height = obj_properties["height"]
    position = obj_properties["position"]
    orientation = obj_properties["orientation"]
    color = obj_properties["color"]
    body_type = obj_properties["body_type"]
    prim_path = obj_properties["prim_path"]
    semantic_label = obj_properties["semantic_label"]

    p = Gf.Vec3f(position[0], position[1], position[2])
    orientation = Gf.Quatf(orientation[0], orientation[1], orientation[2], orientation[3])
    scale = Gf.Vec3f(length, width, height)

    # Create geometry and visual appearance
    if body_type == "cube":
        body_geom = UsdGeom.Cube.Define(stage, prim_path)
        # TODO seems you need to pause a moment after each geom definition as otherwise
        # it occasionally messes up and creates an object of the wrong scale. I suspect
        # it's somehow racing with the async stage creation and then the extent/size
        # doesn't get set correctly. This seems to mitigate the issue, but need more
        # robust way to make sure obj was created I think before operating on it
        time.sleep(0.5)
        # Setting size and extent to 1, scale then corresponds to cm
        body_geom.CreateExtentAttr().Set(Vt.Vec3fArray([1, 1, 1]))
        body_geom.CreateSizeAttr().Set(1)
    else:  # TODO add other shapes
        raise ValueError("Unknown body type: {}".format(body_type))
    body_prim = stage.GetPrimAtPath(prim_path)
    body_geom.AddTranslateOp().Set(p)
    body_geom.AddOrientOp().Set(orientation)
    body_geom.AddScaleOp().Set(scale)
    body_geom.CreateDisplayColorAttr().Set([color])

    # Set physics properties
    PhysicsSchema.CollisionAPI.Apply(body_prim)
    if mass > 0:
        massAPI = PhysicsSchema.MassAPI.Apply(body_prim)
        massAPI.CreateMassAttr(mass)
    physicsAPI = PhysicsSchema.PhysicsAPI.Apply(body_prim)
    physicsAPI.CreateBodyTypeAttr("rigid")

    # Add robot to TF tree
    if tf_prim:
        tf_prim.GetRelationship("targetPrims").AddTarget(prim_path)

    # Set semantic information
    add_semantic_label(body_prim, semantic_label, semantics)

    return body_geom


def set_up_axis_z(stage):
    rootLayer = stage.GetRootLayer()
    rootLayer.SetPermissionToEdit(True)
    with Usd.EditContext(stage, rootLayer):
        UsdGeom.SetStageUpAxis(stage, UsdGeom.Tokens.z)


def set_prim_translate(prim, location):
    """
    Specify position of a given prim, reuse any existing transform ops when possible
    """
    properties = prim.GetPropertyNames()
    if "xformOp:translate" in properties:
        translate_attr = prim.GetAttribute("xformOp:translate")
        translate_attr.Set(location)
    elif "xformOp:translation" in properties:
        translation_attr = prim.GetAttribute("xformOp:translate")
        translation_attr.Set(location)
    elif "xformOp:transform" in properties:
        transform_attr = prim.GetAttribute("xformOp:transform")
        matrix = prim.GetAttribute("xformOp:transform").Get()
        matrix.SetTranslateOnly(location)
        transform_attr.Set(matrix)
    else:
        xform = UsdGeom.Xformable(prim)
        xform_op = xform.AddXformOp(
            UsdGeom.XformOp.TypeTransform, UsdGeom.XformOp.PrecisionDouble, ""
        )
        xform_op.Set(Gf.Matrix4d().SetTranslate(location))


def create_franka(
    stage,
    semantics,
    env_path="/environments/env",
    franka_stage="omni:/Isaac/Samples/Leonardo/Stage/franka_block_stacking.usd",
    solid_robot="/physics/scene/solid",
    location=Gf.Vec3d(0, 0, 0),
    tf_prim=None,
):
    prim = stage.DefinePrim(env_path, "Xform")
    prim.GetReferences().AddReference(franka_stage)
    set_prim_translate(prim, location)
    franka_path = env_path + "/Franka/panda"
    franka_prim = stage.GetPrimAtPath(franka_path)
    # Add collision groups
    for p in Usd.PrimRange(franka_prim):
        collisionAPI = PhysicsSchema.CollisionAPI.Get(stage, p.GetPath())
        if collisionAPI:
            rel = collisionAPI.CreateCollisionGroupRel()
            rel.AddTarget(Sdf.Path(solid_robot))
    # Add semantic labels
    table_path = env_path + "/DemoTable/simple_table/DemoTable"
    table_prim = stage.GetPrimAtPath(table_path)
    add_semantic_label(table_prim, "table", semantics)
    for p in franka_prim.GetAllChildren():
        add_semantic_label(p, "robot", semantics)
    # Add to TF tree
    if tf_prim:
        tf_prim.GetRelationship("targetPrims").AddTarget(franka_path)
        tf_prim.GetRelationship("targetPrims").AddTarget(table_path)


def create_background(
    stage,
    background_stage="omni:/Isaac/Environments/Grid/gridroom_curved.usd",
    background_path="/background",
):
    if not stage.GetPrimAtPath(background_path):
        prim = stage.DefinePrim(background_path, "Xform")
        prim.GetReferences().AddReference(background_stage)
        # Move the stage down -104cm so that the floor is below the table wheels,
        # move in y axis to get light closer
        set_prim_translate(prim, Gf.Vec3d(0, -400, -104))


def setup_physics(stage):
    # Specify gravity
    meters_per_unit = UsdGeom.GetStageMetersPerUnit(stage)
    gravity_scale = 9.81 / meters_per_unit
    gravity = Gf.Vec3f(0.0, 0.0, -gravity_scale)
    scene = PhysicsSchema.PhysicsScene.Define(stage, "/physics/scene")
    scene.CreateGravityAttr().Set(gravity)

    PhysxSchema.PhysxSceneAPI.Apply(stage.GetPrimAtPath("/physics/scene"))
    physx_api = PhysxSchema.PhysxSceneAPI.Get(stage, "/physics/scene")
    physx_api.CreatePhysxSceneEnableCCDAttr(True)
    physx_api.CreatePhysxSceneEnableStabilizationAttr(True)
    physx_api.CreatePhysxSceneEnableGPUDynamicsAttr(False)
    physx_api.CreatePhysxSceneBroadphaseTypeAttr("MBP")
    physx_api.CreatePhysxSceneSolverTypeAttr("TGS")
