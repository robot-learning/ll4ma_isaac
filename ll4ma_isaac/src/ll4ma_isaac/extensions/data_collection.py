import omni.ext
import omni.usd
import omni.appwindow
from omni.kit.ui import Window, DockPreference, Button, CheckBox
import omni.kit.settings
import omni.kit.editor
import asyncio

from omni.physx import _physx

# TODO having to do these sys path appends for now. The problem is Carbonite uses its own
# python path irrespective of what you have setup locally, and there is only a plugin for
# picking up on pip installs and not conda or your local packages. There is a way to set
# the Carbonite path yourself, but I couldn't get it working yet.
import os
import sys

from ll4ma_isaac.environments.blocks_world import BlocksWorld

sys.path += os.environ["CONDA_PKG_PATH"].split(",")  # Should have been set in setup_conda_env.sh


EXTENSION_NAME = "Data Collection"


class DataCollection(omni.ext.IExt):
    def on_startup(self):
        """
        Initialize extension and UI elements
        """
        self._editor = omni.kit.editor.get_editor_interface()
        self._usd_context = omni.usd.get_context()
        self._stage = self._usd_context.get_stage()
        self._window = Window(
            EXTENSION_NAME,
            300,
            200,
            menu_path="LL4MA/" + EXTENSION_NAME,
            open=True,
            dock=DockPreference.LEFT_BOTTOM,
        )

        self._physxIFace = _physx.acquire_physx_interface()

        self._settings = omni.kit.settings.get_settings_interface()
        self._settings.set("/persistent/physics/updateToUsd", False)
        self._settings.set("/persistent/physics/useFastCache", True)
        self._settings.set("/persistent/physics/numThreads", 8)

        self._appwindow = omni.appwindow.get_default_app_window()
        self._sub_stage_event = (
            self._usd_context.get_stage_event_stream().create_subscription_to_pop(
                self._on_stage_event
            )
        )

        self._enable_btn = self._window.layout.add_child(Button("Enable Extension"))
        self._enable_btn.set_clicked_fn(self._setup_environment)
        self._publish_box = self._window.layout.add_child(CheckBox(text="Publish Data", value=True))

    def _setup_environment(self, _):
        load_stage = asyncio.ensure_future(omni.kit.asyncapi.new_stage())
        asyncio.ensure_future(self._on_create_assets(load_stage))

    async def _on_create_assets(self, task):
        """
        Load any assets required by the scenario and create objects
        """
        done, pending = await asyncio.wait({task})
        if task not in done:
            return

        self._stage = self._usd_context.get_stage()

        self._scenario = BlocksWorld(self._editor)

        self._editor.stop()
        self._physxIFace.release_physics_objects()

        self._settings.set("/rtx/reflections/halfRes", True)
        self._settings.set("/rtx/shadows/denoiser/quarterRes", True)
        self._settings.set("/rtx/translucency/reflectionCutoff", 0.1)

        self._scenario.create_assets()

        self._physxIFace.release_physics_objects()
        self._physxIFace.force_load_physics_from_usd()

        self._editor_event_subscription = self._editor.subscribe_to_update_events(
            self._on_editor_step
        )
        self._physxIFace.release_physics_objects()
        self._physxIFace.force_load_physics_from_usd()

        # TODO can parameterize camera resolution and position
        self._editor.set_camera_position("/OmniverseKit_Persp", 120, 0, 100, True)
        self._settings.set("/app/renderer/resolution/width", 512)
        self._settings.set("/app/renderer/resolution/height", 512)

        light_prim = self._stage.GetPrimAtPath("/World/defaultLight")
        if light_prim:
            light_prim.SetActive(False)

        self._editor.play()

    def _on_editor_step(self, step):
        """
        This function is called every timestep in the editor

        Arguments:
            step (float): elapsed time between steps
        """
        self._scenario.step(step, self._publish_box.value)

    def _on_stage_event(self, event):
        """
        This function is called when stage events occur.
        Enables UI elements when stage is opened.
        Prevents tasks from being started until all assets are loaded

        Arguments:
            event (int): event type
        """
        self.stage = self._usd_context.get_stage()
        if event.type == int(omni.usd.StageEventType.OPENED):
            self._editor.stop()

    def on_shutdown(self):
        self._editor.stop()
        self._scenario = None
        self._editor_event_subscription = None
        self._window.set_update_fn(None)


def get_extension():
    return DataCollection()
