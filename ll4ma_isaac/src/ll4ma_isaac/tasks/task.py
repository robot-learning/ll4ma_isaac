import numpy as np

from ll4ma_isaac.environments.common import Status


class Task:
    def __init__(self, pose_error_epsilon=0.02):
        self._pose_error_epsilon = pose_error_epsilon

        self.goals = []

    def run(self, *args):
        raise NotImplementedError()

    def set_goals(self, *args):
        raise NotImplementedError()

    def get_goal(self, *args):
        raise NotImplementedError()

    def goal_satisfied(self, *args):
        raise NotImplementedError()

    def set_goal_status(self, goal, objects, object_poses):
        """
        Sets goal status checking if specified goal has been satisfied as function of
        state of objects in the scene.
        """
        goal_status = np.zeros(len(self.goals), dtype=np.uint8)
        for goal in [g for g in self.goals if g != "NOT_GOAL"]:
            if self.goal_satisfied(goal, objects, object_poses):
                goal_status[self.goals.index(goal)] = 1
        # If no goals satisifed just set the catch-all not-goal bit
        if np.sum(goal_status) == 0:
            goal_status[self.goals.index("NOT_GOAL")] = 1
        return goal_status

    def check_goal_status(self, goal_status, goal):
        """
        Determines task success failure based on whether target goal was achieved.

        Args:
            goal_status (ndarray): One-hot vector of goals, 1 indicates goal achieved
        """
        if goal_status[self.goals.index(goal)] == 1 and np.sum(goal_status) == 1:
            status = Status.TASK_SUCCESS
        else:
            status = Status.TASK_FAILURE
        return status

    def set_valid_object_poses(self, objects):
        """
        Sets valid poses for objects in the scene based on task's definition of
        object pose validity.
        """
        # Generate new poses for the objects
        for obj_id, obj_properties in objects.items():
            # TODO ideally should do collision checking so you don't try to spawn the objects
            # inside each other. Could implement proper collision checking for the objects and
            # pairwise check every new one with those created so far. A simpler way could be
            # just compute spheres around each object and do an extremity check between them.
            position, orientation = self._get_valid_object_pose()
            obj_properties["position"] = position
            obj_properties["orientation"] = orientation

    def get_name(self):
        raise NotImplementedError()

    def _get_valid_object_pose(self, *args):
        raise NotImplementedError()

    def _approx_equal(self, v1, v2, eps=0.001):
        return abs(v1 - v2) < eps
