import rospy
import numpy as np
from random import shuffle

from ll4ma_isaac.tasks import Task
from ll4ma_isaac.behavior.state_machine import Behavior, NextStateTransition, run_state_machine
from ll4ma_isaac.behavior.behavior_states import PushBlocksSimple


class PushBlocks(Task):
    def __init__(self, push_dist=0.25, pose_error_epsilon=0.1):
        super().__init__(pose_error_epsilon)
        self.push_dist = push_dist
        self.push_obj_id = None
        self._y_options = []

    def run(self, domain):
        if self.push_obj_id is None:
            print("Object to push has not been set")
            return
        start_state = latest_state = Behavior()
        push_blocks = PushBlocksSimple(domain, self.push_obj_id, push_dist=self.push_dist)
        latest_state.terminal_transition = NextStateTransition(push_blocks)
        run_state_machine(start_state, domain.step_rate, [domain])

    def set_goals(self, objects):
        """
        Goals for this setting are having one block pushed forward from a line of blocks.
        """
        self.goals = []
        obj_ids = sorted(list(objects.keys()))
        for obj_id in obj_ids:
            self.goals.append(f"{obj_id}__IN-FRONT__")
        self.goals.append("NOT_GOAL")  # Catch-all type when no goal is satisfied

    def get_goal(self, domain):
        self.push_obj_id = np.random.choice(domain.obj_ids)
        return f"{self.push_obj_id}__IN-FRONT__"

    def goal_satisfied(self, goal, objects, object_poses):
        if "__IN-FRONT__" in goal:
            # Add condition that pushed block is within epsilon of its target position
            obj_id = goal.replace("__IN-FRONT__", "")
            p = object_poses[obj_id]["position"]
            if p is None:
                return False
            p_target = [p / 100.0 for p in objects[obj_id]["position"]]  # Convert to m
            p_target[0] += self.push_dist
            pushed_block_satisfied = np.linalg.norm(p_target - p) <= self._pose_error_epsilon

            # Add condition that other objects were not disturbed
            other_blocks_satisfied = True
            for other_id in [i for i in objects.keys() if i != obj_id]:
                p = object_poses[other_id]["position"]
                p_target = [p / 100.0 for p in objects[other_id]["position"]]  # Convert to m
                if np.linalg.norm(p_target - p) > self._pose_error_epsilon:
                    other_blocks_satisfied = False
                    break
            return pushed_block_satisfied and other_blocks_satisfied
        else:
            rospy.logerr(f"Type of goal not known: {goal}")
            return False

    def get_name(self):
        return "push_blocks"

    def _get_valid_object_pose(self, x=50, y_low=-12, y_high=12, z=10):
        """
        Setting blocks in a line on the table. For now assuming 3 blocks possible and they
        will differ only in y value across the line.
        """
        if not self._y_options:
            self._y_options = np.linspace(y_low, y_high, 3).tolist()
            shuffle(self._y_options)
        position = [x, self._y_options.pop(0), z]
        orientation = [0, 0, 0, 1]
        return position, orientation
