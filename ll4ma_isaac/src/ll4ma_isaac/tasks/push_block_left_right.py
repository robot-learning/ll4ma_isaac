import rospy
import numpy as np

from ll4ma_isaac.tasks import Task
from ll4ma_isaac.behavior.state_machine import Behavior, NextStateTransition, run_state_machine
from ll4ma_isaac.behavior.behavior_states import PushBlocksSimple


class PushBlockLeftRight(Task):
    def __init__(
        self, min_push_dist=0.25, max_push_dist=0.4, push_dist_threshold=0.2, pose_error_epsilon=0.1
    ):
        super().__init__(pose_error_epsilon)
        self.min_push_dist = min_push_dist
        self.max_push_dist = max_push_dist
        self.push_dist_threshold = push_dist_threshold
        self.push_obj_id = None

    def run(self, domain):
        if self.push_obj_id is None:
            print("Object to push has not been set")
            return
        start_state = latest_state = Behavior()
        push_block = PushBlocksSimple(
            domain, self.push_obj_id, push_dist=self.push_dist, push_direction=self.push_direction
        )
        latest_state.terminal_transition = NextStateTransition(push_block)
        run_state_machine(start_state, domain.step_rate, [domain])

    def set_goals(self, objects):
        """
        Goals for this setting are having one block pushed forward from a line of blocks.
        """
        obj_id = list(objects.keys())[0]  # Should only be one object for this task
        self.goals = [f"{obj_id}__LEFT__", f"{obj_id}__RIGHT__"]
        self.goals.append("NOT_GOAL")  # Catch-all type when no goal is satisfied

    def get_goal(self, domain):
        self.push_obj_id = domain.obj_ids[0]  # Should only be one object for this task
        lr_choice = np.random.choice(["LEFT", "RIGHT"])
        self.push_direction = [0, 1, 0] if lr_choice == "RIGHT" else [0, -1, 0]
        self.push_dist = np.random.uniform(self.min_push_dist, self.max_push_dist)
        return f"{self.push_obj_id}__{lr_choice}__"

    def goal_satisfied(self, goal, objects, object_poses):
        if "__LEFT__" in goal:
            obj_id = goal.replace("__LEFT__", "")
            p = object_poses[obj_id]["position"]
            if p is None:
                return False
            block_left_satisfied = p[1] < -self.push_dist_threshold
            return block_left_satisfied
        elif "__RIGHT__" in goal:
            obj_id = goal.replace("__RIGHT__", "")
            p = object_poses[obj_id]["position"]
            if p is None:
                return False
            block_right_satisfied = p[1] > self.push_dist_threshold
            return block_right_satisfied
        else:
            rospy.logerr(f"Type of goal not known: {goal}")
            return False

    def get_name(self):
        return "push_block_left_right"

    def _get_valid_object_pose(self, x=50, y=0, z=10):
        """
        Only one block, for now placed always in the same location.
        """
        position = [x, y, z]
        orientation = [0, 0, 0, 1]
        return position, orientation
