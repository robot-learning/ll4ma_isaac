import rospy
import numpy as np

from ll4ma_isaac.tasks import Task
from ll4ma_isaac.behavior.state_machine import Behavior, NextStateTransition, run_state_machine
from ll4ma_isaac.behavior.behavior_states import StackBlocksSimple
from ll4ma_isaac.util import math_util


class StackBlocks(Task):
    def __init__(self, pose_error_epsilon=0.02):
        super().__init__(pose_error_epsilon)

    def run(self, domain):
        start_state = latest_state = Behavior()
        stack_blocks = StackBlocksSimple(domain)
        latest_state.terminal_transition = NextStateTransition(stack_blocks)
        run_state_machine(start_state, domain.step_rate, [domain])

    def set_goals(self, objects):
        """
        Goals in this setting are for now just "ON" goals, one object on top of another.
        This sets all pairwise "ON" relations. Can expand as needed.
        """
        self.goals = []
        obj_ids = sorted(list(objects.keys()))
        for obj_id1 in obj_ids:
            for obj_id2 in [o for o in obj_ids if o != obj_id1]:
                self.goals.append(f"{obj_id1}__ON__{obj_id2}")
        self.goals.append("NOT_GOAL")  # Catch-all type when no goal is satisfied

    def get_goal(self, domain):
        return "__ON__".join(reversed(domain.obj_ids))

    def goal_satisfied(self, goal, objects, object_poses):
        if "__ON__" in goal:
            # TODO assuming only 2 for now
            obj_id1, obj_id2 = goal.split("__ON__")
            h1 = objects[obj_id1]["height"] / 100.0  # TODO assuming oriented up for now
            h2 = objects[obj_id2]["height"] / 100.0
            p1 = object_poses[obj_id1]["position"]
            p2 = object_poses[obj_id2]["position"]
            if p1 is None or p2 is None:
                return False
            # Do z separate since the sign determines which is on top of the other
            z_diff = p1[2] - p2[2]
            z_satisfied = self._approx_equal(z_diff, (h1 + h2) / 2.0)
            xy_error = np.linalg.norm(p1[:2] - p2[:2])
            xy_satisfied = xy_error < self._pose_error_epsilon
            return z_satisfied and xy_satisfied
        else:
            rospy.logerr(f"Type of goal not known: {goal}")
            return False

    def get_name(self):
        return "stack_blocks"

    def _get_valid_object_pose(self, x_low=30, x_high=70, y_low=-40, y_high=40, z=8):
        """
        Returns valid random pose to spawn object within some fixed region on the table. z is
        fixed just to ensure it won't spawn inside table, can make higher if objects are bigger.

        TODO: can also randomize z to capture toppling effects or if you want actual piles on
        on table, but for now working with singulated objects on table plane.
        """
        # z is hard-coded for now just to not spawn into table
        position = [np.random.uniform(x_low, x_high), np.random.uniform(y_low, y_high), z]
        # TODO only sampling up to cube symmetry, will be different when you expand shapes
        orientation = math_util.get_random_planar_quaternion(-45, 45, "z")
        return position, orientation
