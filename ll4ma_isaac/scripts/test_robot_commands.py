#!/usr/bin/env python
import rospy
import numpy as np
from time import time

from sensor_msgs.msg import JointState


if __name__ == "__main__":
    """
    Simple test script to make sure robot commands work. Waits for current joint state, sets
    that as init, and computes a sinusoidal joint motion for every joint around the init.
    Also appends gripper binary open/close state so gripper should open close at a fixed
    interval.
    """
    rospy.init_node("test_joint_commands")

    init_joint_state = None

    def joint_state_cb(msg):
        global init_joint_state
        init_joint_state = msg

    pub = rospy.Publisher("/panda/robot_command", JointState, queue_size=1)
    sub = rospy.Subscriber("/panda/joint_states", JointState, joint_state_cb)
    rate = rospy.Rate(10)

    rospy.loginfo("Waiting for joint state...")
    while init_joint_state is None and not rospy.is_shutdown():
        rate.sleep()
    rospy.loginfo("Joint state received")

    init_joints = np.array(init_joint_state.position[:7])
    max_joints = init_joints + 0.5
    min_joints = init_joints - 0.5

    robot_command = JointState()
    robot_command.name = init_joint_state.name
    robot_command.position = list(init_joint_state.position[:7]) + [0]  # Add gripper state

    rospy.loginfo("Publishing robot commands...")
    start = time()
    i = 0
    gripper_state = False
    while not rospy.is_shutdown():
        robot_command.position = (
            np.sin(time() - start) * (max_joints - min_joints) * 0.5 + init_joints
        )
        if i % 20 == 0:
            gripper_state = not gripper_state
        i += 1
        robot_command.position = list(robot_command.position) + [int(gripper_state)]
        pub.publish(robot_command)
        rate.sleep()
