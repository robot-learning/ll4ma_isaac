#!/usr/bin/env python3
import os
import sys
import h5py
import numpy as np
import cv2

import rospy
from cv_bridge import CvBridge
from sensor_msgs.msg import JointState, Image
from tf2_msgs.msg import TFMessage
from geometry_msgs.msg import TransformStamped
from visualization_msgs.msg import Marker, MarkerArray

from ll4ma_isaac.util import vis_util


def get_joint_state_msg(joint_pos):
    joint_state_msg = JointState()
    # TODO joint names not yet in data, should do that
    joint_state_msg.name = [f"panda_joint{j}" for j in range(1, 8)]
    joint_state_msg.name += [f"panda_finger_joint{j}" for j in [1, 2]]
    joint_state_msg.position = joint_pos
    return joint_state_msg


def get_rgb_msg(rgb_array):
    rgb_img = cv2.cvtColor(rgb_array, cv2.COLOR_RGB2BGR)
    rgb_msg = CvBridge().cv2_to_imgmsg(rgb_img)
    return rgb_msg


def get_depth_msg(depth_array):
    # depth_img = cv2.cvtColor(depth_array, cv2.COLOR_BGR2GRAY)
    depth_msg = CvBridge().cv2_to_imgmsg(depth_array)
    return depth_msg


def get_seg_msg(segmentation, seg_ids, colors):
    seg_img = vis_util.segmentation_to_rgb(segmentation, seg_ids, colors)
    seg_msg = get_rgb_msg(seg_img)
    return seg_msg


def get_tf_msg(tf, idx):
    tf_msg = TFMessage()
    for child_frame, data in tf.items():
        tf_stamped = TransformStamped()
        tf_stamped.header.frame_id = data["parent_frame"]
        tf_stamped.child_frame_id = child_frame
        tf_stamped.transform.translation.x = data["position"][idx][0]
        tf_stamped.transform.translation.y = data["position"][idx][1]
        tf_stamped.transform.translation.z = data["position"][idx][2]
        tf_stamped.transform.rotation.x = data["orientation"][idx][0]
        tf_stamped.transform.rotation.y = data["orientation"][idx][1]
        tf_stamped.transform.rotation.z = data["orientation"][idx][2]
        tf_stamped.transform.rotation.w = data["orientation"][idx][3]
        tf_msg.transforms.append(tf_stamped)
    return tf_msg


def get_marker_msg(objects, tf, idx):
    marker_msg = MarkerArray()
    for i, (obj_id, obj_data) in enumerate(objects.items()):
        marker = Marker()
        marker.id = i
        marker.type = Marker.CUBE
        marker.action = Marker.MODIFY
        marker.pose.position.x = tf[obj_id]["position"][idx][0]
        marker.pose.position.y = tf[obj_id]["position"][idx][1]
        marker.pose.position.z = tf[obj_id]["position"][idx][2]
        marker.pose.orientation.x = tf[obj_id]["orientation"][idx][0]
        marker.pose.orientation.y = tf[obj_id]["orientation"][idx][1]
        marker.pose.orientation.z = tf[obj_id]["orientation"][idx][2]
        marker.pose.orientation.w = tf[obj_id]["orientation"][idx][3]
        marker.header.frame_id = tf[obj_id]["parent_frame"]
        marker.scale.x = obj_data["length"]
        marker.scale.y = obj_data["width"]
        marker.scale.z = obj_data["height"]
        marker.color.a = 1.0
        marker.color.r = obj_data["color"][0]
        marker.color.g = obj_data["color"][1]
        marker.color.b = obj_data["color"][2]
        marker_msg.markers.append(marker)
    return marker_msg


def publish_msg(msg, publisher):
    msg.header.stamp = rospy.Time.now()
    publisher.publish(msg)


def publish_tf_msg(msg, publisher):
    for tf_stamped in msg.transforms:
        tf_stamped.header.stamp = rospy.Time.now()
        publisher.publish(msg)


def publish_marker_msg(msg, publisher):
    for marker in msg.markers:
        marker.header.stamp = rospy.Time.now()
        publisher.publish(msg)


def visualize_data(h5_filename):
    rospy.loginfo("Loading H5 data...")
    with h5py.File(h5_filename, "r") as h5_file:
        rgb = np.array(h5_file["rgb"])
        depth = np.array(h5_file["depth"])
        instance_seg = np.array(h5_file["instance_segmentation"])
        instance_seg_ids = [0] + list(h5_file["instance_segmentation"].attrs["ids"])
        semantic_seg = np.array(h5_file["semantic_segmentation"])
        semantic_seg_ids = [0] + list(h5_file["semantic_segmentation"].attrs["ids"])
        joint_pos = np.array(h5_file["joint_positions"])
        tf = {}
        for obj_id, obj_data in h5_file["tf"].items():
            tf[obj_id] = {
                "parent_frame": obj_data.attrs["parent_frame"],
                "position": np.array(obj_data["position"]),
                "orientation": np.array(obj_data["orientation"]),
            }
        objects = {}
        for obj_id in h5_file["objects"].keys():
            obj_data = h5_file["objects"][obj_id].attrs
            objects[obj_id] = {
                # Extent/position converted from cm to m
                "length": obj_data["length"] / 100.0,
                "width": obj_data["width"] / 100.0,
                "height": obj_data["height"] / 100.0,
                "color": np.array(obj_data["color"]),
            }
    rospy.loginfo("H5 data loaded.")

    rospy.loginfo("Creating ROS messages...")
    n_timesteps = min(len(rgb), len(joint_pos))  # ROS and gt sometimes are off by one

    colors = [
        "windows blue",
        "amber",
        "faded green",
        "dusty purple",
        "light red",
        "deep blue",
        "blue green",
        "pumpkin",
        "periwinkle blue",
        "lemon yellow",
        "mocha",
        "greeny yellow",
        "grey/blue",
        "dark fuchsia",
        "greyish teal",
        "eggplant purple",
        "strong blue",
    ]

    instance_seg_colors = vis_util.random_colors(len(instance_seg_ids), xkcd_colors=colors)
    semantic_seg_colors = vis_util.random_colors(len(semantic_seg_ids), xkcd_colors=colors)

    joint_state_msgs = []
    rgb_msgs = []
    depth_msgs = []
    instance_seg_msgs = []
    semantic_seg_msgs = []
    tf_msgs = []
    marker_msgs = []
    for i in range(n_timesteps):
        joint_state_msgs.append(get_joint_state_msg(joint_pos[i]))
        rgb_msgs.append(get_rgb_msg(rgb[i]))
        depth_msgs.append(get_depth_msg(depth[i]))
        instance_seg_msgs.append(
            get_seg_msg(instance_seg[i], instance_seg_ids, instance_seg_colors)
        )
        semantic_seg_msgs.append(
            get_seg_msg(semantic_seg[i], semantic_seg_ids, semantic_seg_colors)
        )
        tf_msgs.append(get_tf_msg(tf, i))
        marker_msgs.append(get_marker_msg(objects, tf, i))
    rospy.loginfo("ROS messages created.")

    # joint_state_pub = rospy.Publisher("/panda/joint_states", JointState, queue_size=1)
    rgb_pub = rospy.Publisher("/rgb", Image, queue_size=1)
    depth_pub = rospy.Publisher("/depth", Image, queue_size=1)
    instance_seg_pub = rospy.Publisher("/instance_segmentation", Image, queue_size=1)
    semantic_seg_pub = rospy.Publisher("/semantic_segmentation", Image, queue_size=1)
    tf_pub = rospy.Publisher("/tf", TFMessage, queue_size=1)
    marker_pub = rospy.Publisher("/markers", MarkerArray, queue_size=1)

    rospy.loginfo("Publishing messages...")
    rate = rospy.Rate(30)
    while not rospy.is_shutdown():
        for i in range(n_timesteps):
            # publish_msg(joint_state_msgs[i], joint_state_pub)
            publish_msg(rgb_msgs[i], rgb_pub)
            publish_msg(depth_msgs[i], depth_pub)
            publish_msg(instance_seg_msgs[i], instance_seg_pub)
            publish_msg(semantic_seg_msgs[i], semantic_seg_pub)
            publish_tf_msg(tf_msgs[i], tf_pub)
            publish_marker_msg(marker_msgs[i], marker_pub)
            rate.sleep()
        rospy.sleep(3)


if __name__ == "__main__":
    rospy.init_node("validate_multisensory_data")

    h5_filename = rospy.get_param("~h5")

    if not os.path.exists(h5_filename):
        print(f"\nFile does not exist: {h5_filename}\n")
        sys.exit(1)

    visualize_data(h5_filename)
