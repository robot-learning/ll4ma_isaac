from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup


setup_args = generate_distutils_setup(
    packages=[
        "ll4ma_isaac",
        "ll4ma_isaac.behavior",
        "ll4ma_isaac.environments",
        "ll4ma_isaac.extensions",
        "ll4ma_isaac.robots",
        "ll4ma_isaac.tasks",
        "ll4ma_isaac.util",
    ],
    package_dir={"": "src"},
)

setup(**setup_args)
