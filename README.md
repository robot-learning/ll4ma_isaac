# LL4MA Isaac

This repository houses lab-specific resources for using the NVIDIA Isaac simulator.

Right now this is mainly supported for Ubuntu 20.04 and ROS Noetic.

**NOTE:** There are two packages in this repo: `ll4ma_isaac` is for [Isaac Omniverse](https://developer.nvidia.com/isaac-sim) and `ll4ma_isaacgym` is for [Isaac Gym](https://developer.nvidia.com/isaac-gym). They are disjoint from each other so if e.g. you're only using Isaac Gym then you only need to use `ll4ma_isaacgym`.

If you have problems using this package, please raise an issue on the [issue tracker](https://bitbucket.org/robot-learning/ll4ma_isaac/issues?status=new&status=open).

---

## How to Contribute

If you want to develop a feature or fix a bug (see the [issue tracker](https://bitbucket.org/robot-learning/ll4ma_isaac/issues?status=new&status=open) for open issues), please follow these steps:

1. Create an issue if one does not yet exist. Please describe in as much detail as you can so everyone is clear what the issue is to address.
2. Create a branch for your issue with a meaningful name (e.g. `issue14-fix-reload-bug`)
3. Develop your code on your branch
4. When you're ready to merge to main:
    1. Check the [pipelines page](https://bitbucket.org/robot-learning/ll4ma_isaac/pipelines/results/page/1) to make sure your most recent commit on your branch passes the pipeline. If you see a green check mark next to it you're good to go. If it's a red exclamation mark, click it to see what it's complaining about. We auto-run a linter, so it's likely you need to fix some code style things. You can run `flake8 .` on your local machine from the root directory of this repository to see what the problems are and either manually fix them, or run `black .` to try to auto-fix them.
    2. Create a pull request for your branch, and assign a reviewer. Currently this is probably Adam, but use your judgement who you think should review it.
    3. Wait for reviewers to approve, then merge your branch to `main`.
    4. Verify that when you merged to `main`, the pipeline still passes for `main`.
    5. Mark your issue as resolved, and leave a comment on the issue to say what pull request number resolves the issue.

---

## Installation
  1. Install conda if you don't have it yet, you can use [this script](https://bitbucket.org/robot-learning/ll4ma_util/src/main/conda/install_conda.sh) if needed.
  2. Create a conda environment. You can directly use [this environment config](https://bitbucket.org/robot-learning/ll4ma_util/src/main/conda/ll4ma_conda_env.yaml) and do `conda env create -f ll4ma_conda_env.yaml`. You may want to copy that config to your own package and modify it as you need to. If using it stock, you may have to seperately do `mamba install -c robostack ros-noetic-trac-ik-kinematics-plugin` for the moveit interface.
  3. Clone this package and some internal dependencies to a [`catkin` workspace](http://wiki.ros.org/catkin/workspaces). If it's a new workspace do these steps:
  ```bash
  conda activate ll4ma
  mkdir -p ~/catkin_ws/src
  cd ~/catkin_ws/src
  git clone git@bitbucket.org:robot-learning/ll4ma_isaac.git
  git clone git@bitbucket.org:robot-learning/ll4ma_robots_description.git
  git clone git@bitbucket.org:robot-learning/ll4ma_moveit.git
  git clone git@bitbucket.org:robot-learning/ll4ma_util.git
  cd ~/catkin_ws
  catkin init
  catkin build
  ```
  4. Download the code from [here](https://developer.nvidia.com/isaac-gym). You will need to create an NVIDIA developer account and request permission to download, but the request is typically approved quickly.
  5. Expand the compressed file you downloaded somewhere of your choosing (e.g. `~/source_code/isaacgym`), henceforth referred to as `ISAACGYM_ROOT`
  6. Install the code:
  ```bash
  cd $ISAACGYM_ROOT/python
  pip install -e .
  ```
  7. You will probably need to add something like this to your `.bashrc`, though make sure to adjust it for your conda environment location (".miniconda3") and environment name ("ll4ma"):
  ```bash
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/.miniconda3/envs/ll4ma/lib
  export LIBRARY_PATH=$LIBRARY_PATH:$HOME/.miniconda3/envs/ll4ma/lib
  ```
  8. Read NVIDIA's documentation by opening `$ISAACGYM_ROOT/docs/index.html` in a browser.

---

## Usage

You can check if everything worked by trying to run one of the data collection scripts. Open 2 different terminals. In the first terminal, type the command
```bash
roslaunch moveit_interface iiwa_reflex_moveit_interface_service.launch
```
You should see some text come up in the terminal and a message at the end saying `MoveIt interface services are available`. In the other terminal:
```bash
roscd ll4ma_isaacgym/src/ll4ma_isaacgym/scripts
python run_isaacgym.py
```
Hopefully, you see an Isaac Gym GUI pop up and the robot attempts to pick up a block from the table. You can add the `-h` flag to see more options that can be passed to the script. 


### Data Collection

If you want to collect data, you need to pass in the `--data_root` option with an absolute path to where you want the data saved, for example (note this example also changes number of envs and number of instances collected):
```bash
python run_isaacgym.py --data_root /absolute/path/to/data
```
You can specify the number of instances you want to collect and also change the number of envs active which can expedite data collection:
```bash
python run_isaacgym.py --data_root /absolute/path/to/data --n_envs 16 --n_demos 32
```
Here are some flags you can add to further configure the data collection session:

|Option                           | Description |
|---------------------------------|-------------|
| `--log_only_success`            | Log only successful task executions and discard failures. |
| `--log_only_behavior_intervals` | Log only timesteps before and after behaviors execute (recursively, so before and after every sub-behavior) |
| `--log_interval`                | Number of timesteps to skip between timesteps when data is saved (effectively downsamples data by that factor, e.g. `--log_interval 10` will only log every tenth step in the simulator) |

Currently the `pick_object` task is the most stable, though we're working on more interesting ones. The whole environment for that task is configured with the `config/iiwa_pick_object.yaml` file which is the default argument to the run script. You can change that config with the `--config` flag if in that same directory, and you can also change the directory containing the config with the `--config_dir` flag.

---

## Isaac Omniverse Setup

If you're only using Isaac Gym, right now you don't need to worry about this section. Current instructions for setting up Isaac on your local machine are on our wiki [here](https://robot-learning.cs.utah.edu/isaacsim_installation). Note these were written for an older version of Omniverse so it's possible some things have changed in the newer versions.

---